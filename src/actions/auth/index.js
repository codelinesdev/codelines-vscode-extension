import {
  SET_DID_FINISH_REAUTH,
} from './types';

export const setDidFinishReauth = didFinishReauth => ({
  type: SET_DID_FINISH_REAUTH,
  didFinishReauth,
});
