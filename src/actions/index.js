import * as bookMarks from './bookMarks';
import * as marker from './marker';
import * as editor from './editor';
import * as codeBook from './codeBook';
import * as user from './user';
import * as router from './router';
import * as workspace from './workspace';
import * as auth from './auth';

import * as userTypes from './user/types';
import * as bookMarksTypes from './bookMarks/types';
import * as editorTypes from './editor/types';
import * as routerTypes from './router/types';
import * as codeBookTypes from './codeBook/types';
import * as workspaceTypes from './workspace/types';
import * as authTypes from './auth/types';

export default {
  ...bookMarks,
  ...marker,
  ...editor,
  ...codeBook,
  ...user,
  ...router,
  ...workspace,
  ...auth,
};

export const types = {
  user: { ...userTypes },
  bookMarks: { ...bookMarksTypes },
  editor: { ...editorTypes },
  router: { ...routerTypes },
  codeBook: { ...codeBookTypes },
  workspace: { ...workspaceTypes },
  auth: { ...authTypes },
};
