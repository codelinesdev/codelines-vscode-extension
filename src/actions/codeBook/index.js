import {
  SET_ACTIVE_CODE_BOOK,
  SET_ACTIVE_CODE_BOOK_DOCUMENT,
  SET_CODE_BOOK_DOCUMENTS,
} from './types';

export const setActiveCodeBook = activeCodeBook => ({
  type: SET_ACTIVE_CODE_BOOK,
  activeCodeBook,
});

export const setActiveCodeBookDocument = activeDocument => ({
  type: SET_ACTIVE_CODE_BOOK_DOCUMENT,
  activeDocument,
});

export const setCodeBookDocuments = codeBookDocuments => ({
  type: SET_CODE_BOOK_DOCUMENTS,
  codeBookDocuments,
});
