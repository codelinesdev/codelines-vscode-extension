import {
  SET_EDITOR_REF,
  SET_EDITOR_STATE,
} from './types';

export const setEditorRef = editorRef => ({
  type: SET_EDITOR_REF,
  editorRef,
});

export const setEditorState = editorState => ({
  type: SET_EDITOR_STATE,
  editorState,
});
