import {
  SET_HOVERED_BOOK_MARK_ID,
  SET_FOCUSED_BOOK_MARK_ID,
} from './types';

export const setHoveredBookMarkId = hoveredBookMarkId => ({
  type: SET_HOVERED_BOOK_MARK_ID,
  hoveredBookMarkId,
});

export const setFocusedBookMarkId = focusedBookMarkId => ({
  type: SET_FOCUSED_BOOK_MARK_ID,
  focusedBookMarkId,
});
