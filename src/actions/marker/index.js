import { SET_IS_MARKER_READY } from '@/actions/types';

export const setIsMarkerReady = isMarkerReady => ({
  type: SET_IS_MARKER_READY,
  isMarkerReady,
});
