import {
  SET_WORKSPACE_NAME,
  SET_WORKSPACE_CODE_BOOKS,
} from './types';

export const setWorkspaceName = name => ({
  type: SET_WORKSPACE_NAME,
  name,
});

export const setWorkspaceCodeBooks = codeBooks => ({
  type: SET_WORKSPACE_CODE_BOOKS,
  codeBooks,
});
