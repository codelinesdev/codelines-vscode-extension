import {
  Uri,
  workspace,
  WorkspaceEdit,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as rimraf from 'rimraf';
import * as crypto from 'crypto';

import { generateName } from '../utils/codeBookName';
import CodeBook from './CodeBook';
// import CodeBookDocument from './CodeBookDocument';

interface CodeBooksDict { [codeBookId: string]: CodeBook }
interface CodeBookNames { name: string; id: string }

interface Settings {
  version: string;
  projectId: string;
  lastOpenedCodeBook: string;
  codeBooks: CodeBooksDict;
}

export default class CodelinesSettings {
  private static instance: CodelinesSettings;

  private currentVersion = '1.0';

  public readonly codelinesFolderPath: string;

  private settingsFilePath: string;

  private codelinesIgnoreFilePath: string;

  private codeBooksFolderPath: string;

  private codeBookDocumentsFolderPath: string;

  // @ts-ignore
  private settings: Settings;

  private get codeBooksDict(): CodeBooksDict {
    return this.settings.codeBooks;
  }

  public get codeBooks(): CodeBook[] {
    const arr: CodeBook[] = [];
    Object.keys(this.codeBooksDict).forEach((cbId) => {
      const cb = this.codeBooksDict[cbId];
      arr.push(cb);
    });

    return arr;
  }

  public get lastOpenedCodeBook(): string {
    return this.settings.lastOpenedCodeBook;
  }

  public set lastOpenedCodeBook(id: string) {
    this.settings.lastOpenedCodeBook = id;
    this.saveSettings();
  }

  public get projectId(): string {
    return this.settings.projectId;
  }

  public set projectId(id: string) {
    this.settings.projectId = id;
    this.saveSettings();
  }

  private constructor() {
    const folders = workspace.workspaceFolders;
    if (folders && folders[0]) {
      const rootFolder = folders[0];
      const workspaceRootPath = rootFolder.uri.fsPath;
      this.codelinesFolderPath = path.join(workspaceRootPath, '.codelines');
      this.codeBookDocumentsFolderPath = path.join(workspaceRootPath, '.codelines', 'documents');
      this.settingsFilePath = path.join(workspaceRootPath, '.codelines', 'settings.json');
      this.codelinesIgnoreFilePath = path.join(workspaceRootPath, '.codelinesignore');

      //  - .codelines/
      //    - settings.json
      //    - codeBooks/
      //      - <id1>/
      //        - marks.json
      //        - documents/
      //          - <docId1>/
      //            - content.json
      //      - <id2>/
      //      - <id3>/
      this.codeBooksFolderPath = path.join(this.codelinesFolderPath, 'codeBooks');

      const initialSettings = {
        codeBooks: [
          {
            id: '',
            name: '',
            rootPath: '',
            marksPath: '',
            documents: [
              {
                name: '',
                contentPath: '',
              },
            ],
          },
        ],
        lastOpenedCodeBook: '<codeBookId>',
      };

      this.tryToInitializeCodelinesFolder();
      this.loadSettings();
    } else {
      throw new Error('No workspace root folder. Cannot initialize CodelineSettings without a workspace.');
    }
  }

  public static getInstance() {
    if (!CodelinesSettings.instance) {
      this.instance = new CodelinesSettings();
    }
    return this.instance;
  }

  // public async createCodeBookDocument(documentName: string) {
  //   const document: CodeBookDocument = {
  //     name: documentName,
  //     contentPath: `${documentName}/content.json`,
  //     // contentPath: `${this.codeBookDocumentsFolderPath}/${documentName}/content.json`,
  //   };

  //   if (this.isExistingDocument(document)) {
  //     throw new Error(`Document '${document.contentPath}' already exists.`);
  //   }

  //   const uri = Uri.file(document.contentPath);
  //   const we = new WorkspaceEdit();
  //   we.createFile(uri);
  //   await workspace.applyEdit(we);

  //   // Empty file isn't valid JSON
  //   const emptyEditorContent = '{"blocks":[{"data":{},"depth":0,"entityRanges":[],"inlineStyleRanges":[],"key":"66rqv","text":"","type":"unstyled"}],"entityMap":{}}';
  //   const fullContentPath = path.join(this.codeBookDocumentsFolderPath, document.contentPath);
  //   console.log('Full content path', fullContentPath);

  //   const codeBookDocPath = path.join(this.codeBookDocumentsFolderPath, `${document.name}`);
  //   fs.mkdirSync(codeBookDocPath);
  //   fs.writeFileSync(fullContentPath, emptyEditorContent);

  //   this.settings.codeBookDocuments.push(document);
  //   this.saveSettings();

  //   return document;
  // }

  private getCodeBookForName(name: string): CodeBook | undefined {
    let codeBookWithName;
    Object.keys(this.codeBooksDict).forEach((id) => {
      const cb = this.codeBooksDict[id];
      if (cb.name === name) {
        codeBookWithName = cb;
      }
    });
    return codeBookWithName;
  }

  public getCodeBook(id: string) {
    return this.codeBooksDict[id];
  }

  public createCodeBook() {
    let name = generateName();

    // const existingCbNames: string[] = [];
    // Object.keys(this.codeBooksDict).forEach((cbId) => {
    //   const cb = this.codeBooksDict[cbId];
    //   existingCbNames.push(cb.name);
    // });

    // eslint-disable-next-line no-loop-func
    while (this.getCodeBookForName(name)) {
      name = generateName();
    }

    const relativeCodeBooksFolderPath = 'codeBooks'; // Relative to the .codelines folder
    const cb = new CodeBook(name, this.codelinesFolderPath, relativeCodeBooksFolderPath);
    this.codeBooksDict[cb.id] = cb;
    this.saveSettings();

    return cb;
  }

  public changeCodeBookName(id: string, name: string) {
    if (name === undefined || name === null) {
      throw new Error('Cannot set code book name to \'undefined\'');
    }

    const cb = this.getCodeBookForName(name);
    // 'cb !== id' because we don't want to throw when user is trying to set the same name
    if (cb && (cb.id !== id)) {
      throw new Error('Code book with this name already exists');
    }

    this.codeBooksDict[id].changeName(name);
    this.saveSettings();
  }

  public changeCodeBookDescription(id: string, description: string) {
    if (description === undefined || description === null) {
      throw new Error('Cannot set code book description to \'undefined\'');
    }

    this.codeBooksDict[id].changeDescription(description);
    this.saveSettings();
  }

  public deleteCodeBook(id: string) {
    const cbToDelete = this.codeBooksDict[id];
    // Remove the code book's root folder and all it's subfolders
    // TODO: Make async
    const absoluteCodeBookRootFolderPath = path.join(this.codelinesFolderPath, cbToDelete.rootPath);
    rimraf.sync(absoluteCodeBookRootFolderPath);

    // Remove code book from the settings object that is in the memory
    delete this.codeBooksDict[id];
    this.saveSettings();
  }

  public createCodeBookDocument(codeBookId: string, name: string) {
    this.codeBooksDict[codeBookId].createDocument(name, this.codelinesFolderPath);
    this.saveSettings();
  }

  public saveDocumentContent(codeBookId: string, documentName: string, content: any) {
    this.codeBooksDict[codeBookId].saveDocumentContent(documentName, content, this.codelinesFolderPath);
  }

  public getDocumentContent(codeBookdId: string, documentName: string): any {
    const doc = this.codeBooksDict[codeBookdId].documents[documentName];
    const absoluteContentPath = path.join(this.codelinesFolderPath, doc.contentPath);
    return JSON.parse(fs.readFileSync(absoluteContentPath, 'utf8'));
  }

  public deleteDocument(codeBookId: string) {
    // TODO
  }

  // public saveDocumentContent(document: CodeBookDocument, content: any) {
  //   if (!this.isExistingDocument(document)) {
  //     throw new Error(`Cannot save document content because document '${document.contentPath}' does not exist.`);
  //   }
  //   const stringified = JSON.stringify(content);
  //   const fullContentPath = path.join(this.codeBookDocumentsFolderPath, document.contentPath);
  //   fs.writeFileSync(fullContentPath, stringified);
  // }

  // public getContentForDocument(document: CodeBookDocument): any {
  //   if (!this.isExistingDocument(document)) {
  //     throw new Error(`Cannot get contents of document because document '${document.contentPath}' does not exist.`);
  //   }

  //   const fullContentPath = path.join(this.codeBookDocumentsFolderPath, document.contentPath);
  //   return JSON.parse(fs.readFileSync(fullContentPath, 'utf8'));
  // }

  // public deleteDocument(document: CodeBookDocument) {
  //   if (!this.isExistingDocument(document)) {
  //     throw new Error(`Cannot delete document because document '${document.contentPath}' does not exist.`);
  //   }

  //   this.settings.codeBookDocuments = this.settings.codeBookDocuments.filter(doc => doc.contentPath !== document.contentPath);
  //   this.saveSettings();

  //   rimraf.sync(`${this.codeBookDocumentsFolderPath}/${document.name}`);
  // }

  private tryToInitializeCodelinesFolder() {
    if (!fs.existsSync(this.codelinesFolderPath)) {
      fs.mkdirSync(this.codelinesFolderPath);
      // fs.mkdirSync(this.codeBookDocumentsFolderPath);

      fs.mkdirSync(this.codeBooksFolderPath);
    } else if (!fs.existsSync(this.codeBooksFolderPath)) {
      fs.mkdirSync(this.codeBooksFolderPath);
    } else if (!fs.existsSync(this.codeBookDocumentsFolderPath)) {
      fs.mkdirSync(this.codeBookDocumentsFolderPath);
    }

    this.tryToInitializeCodelinesIgnoreFile();
  }

  private tryToInitializeCodelinesIgnoreFile() {
    if (!fs.existsSync(this.codelinesIgnoreFilePath)) {
      const stringLines = [
        '# All files/folders here should be in glob pattern. See https://github.com/isaacs/node-glob#options',
        '**/node_modules/**',
        './node_modules/**',
        workspace.name ? `${workspace.name}.zip` : '',
      ];
      // eslint-disable-next-line
      const initialIgnoreString = stringLines.reduce((acc, line) => acc += `\n${line}`);

      // TODO: We should recognize a type of the project (Javascript, Python, etc)
      // and have some .codelinesignore templates

      fs.writeFileSync(this.codelinesIgnoreFilePath, initialIgnoreString);
    }
  }

  private loadSettings() {
    const initialSettings = {
      projectId: crypto.randomBytes(16).toString('base64'),
      codeBooks: {},
      lastOpenedCodeBook: '',
    };

    // If there isn't a settings file, create empty one
    if (fs.existsSync(this.settingsFilePath)) {
      this.settings = JSON.parse(fs.readFileSync(this.settingsFilePath, 'utf8'));

      // If user with a previous version of settings updated Codelines
      if (this.settings.version !== this.currentVersion) {
        // TODO: Convert user documents to the current folder structure
        // TODO: Backward compatibility
        this.settings = {
          ...this.settings,
          ...initialSettings,
          version: `${this.currentVersion}-update`,
        };
      }

      // Create proper instances from the JSON obj
      const codeBooks: CodeBooksDict = {};
      Object.keys(this.settings.codeBooks).forEach((cbId) => {
        const cbObj = this.settings.codeBooks[cbId];
        const codeBook = new CodeBook().deserialize(cbObj);
        codeBooks[codeBook.id] = codeBook;
      });
      this.settings.codeBooks = codeBooks;

      if (!this.settings.projectId) {
        this.projectId = crypto.randomBytes(16).toString('base64');
      }
    } else {
      this.settings = {
        ...initialSettings,
        version: this.currentVersion,
      };
    }
    this.saveSettings();

    // const uri = Uri.file(this.settingsFilePath);
    // const we = new WorkspaceEdit();
    // This ensures that if there already is a settings file it won't get overwritten
    // we.createFile(uri, { ignoreIfExists: true });
  }

  private saveSettings() {
    fs.writeFileSync(this.settingsFilePath, JSON.stringify(this.settings));
  }
}
