import * as path from 'path';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import CodeBookDocument from './CodeBookDocument';
import {
  Serializable,
  createFolder,
  createFile,
} from '../utils';

const firebaseSonfig = {
  apiKey: 'AIzaSyDwZn0QHift7NwByMpdUznEqaKH5VOg3bA',
  authDomain: 'codelines.firebaseapp.com',
  databaseURL: 'https://codelines.firebaseio.com',
  projectId: 'codelines',
  storageBucket: 'codelines-codebooks',
  messagingSenderId: '753392105954',
  appId: '1:753392105954:web:3ab94d77a3bd1e0d',
};

firebase.initializeApp(firebaseSonfig);
const db = firebase.firestore();

interface CodeBookDocsDict { [codeBookDocumentName: string]: CodeBookDocument }

export default class CodeBook implements Serializable<CodeBook> {
  public id: string;
  public name: string;
  public description = '';
  public documents: CodeBookDocsDict = {};
  public lastOpenedDocument = '';


  public rootPath: string; // A relative path to the .codelines folder
  public marksPath: string; // A relative path to the .codelines folder
  private documentsFolderPath: string; // A relative path to the .codelines folder

  public deserialize(obj: Record<string, any>): CodeBook {
    this.id = obj.id;
    this.name = obj.name;
    this.description = obj.description;

    // We must create proper class instances from the loaded `obj`
    const docs: CodeBookDocsDict = {};
    Object.keys((obj.documents as CodeBookDocsDict)).forEach((docName) => {
      const docObj = obj.documents[docName];
      const document = new CodeBookDocument().deserialize(docObj);
      docs[document.name] = document;
    });
    this.documents = docs;

    this.rootPath = obj.rootPath;
    this.marksPath = obj.marksPath;
    this.documentsFolderPath = obj.documentsFolderPath;
    return this;
  }

  // `relativeCodeBooksFolderPath`is relative to the .codelines folder
  // private constructor(id: string, name: string, rootPath: string);
  public constructor(name?: string, codelinesFolderPath?: string, relativeCodeBooksFolderPath?: string) {
    if (!name && !codelinesFolderPath && !relativeCodeBooksFolderPath) {
      // This is useful only when we are calling "new CodeBook().deserialize(jsonObj)"
      this.id = '';
      this.name = '';
      this.rootPath = '';
      this.marksPath = '';
      this.documentsFolderPath = '';
      return;
    }

    if (!name) {
      throw new Error('Specify `name` parameter.');
    } else if (!codelinesFolderPath) {
      throw new Error('Specify `codelinesFolderPath` parameter.');
    } else if (!relativeCodeBooksFolderPath) {
      throw new Error('Specify `relativeCodeBooksFolderPath` parameter.');
    }

    this.id = db.collection('codeBooks').doc().id;
    this.name = name;

    // We use a relative path because this value is saved in the settings.json
    // If the path would be absolute, it would break when the project is opened
    // on a different machine
    this.rootPath = path.join(relativeCodeBooksFolderPath, this.id);
    this.marksPath = path.join(this.rootPath, 'marks.json');
    this.documentsFolderPath = path.join(this.rootPath, 'documents');

    // Create a root folder for this code book
    const absoluteRootPath = path.join(codelinesFolderPath, relativeCodeBooksFolderPath, this.id);
    createFolder(absoluteRootPath);

    // Create marks.json file for this code book
    const absoluteMarksPath = path.join(absoluteRootPath, 'marks.json');
    createFile(absoluteMarksPath, '{}');

    // Create a root folder for this code book documents
    const absoluteDocumentsFolderPath = path.join(absoluteRootPath, 'documents');
    createFolder(absoluteDocumentsFolderPath);

    // Create default code book document - every code book must have at least one document
    this.createDocument('doc-1', codelinesFolderPath);
    this.lastOpenedDocument = 'doc-1';
  }

  public createDocument(name: string, codelinesFolderPath: string) {
    if (this.isExistingDocumentName(name)) {
      throw new Error(`Document with name '${name}' already exists.`);
    }

    const document = new CodeBookDocument(name, codelinesFolderPath, this.documentsFolderPath);
    this.documents[document.name] = document;
  }

  public changeName(name: string) {
    this.name = name;
  }

  public changeDescription(description: string) {
    this.description = description;
  }

  public saveDocumentContent(documentName: string, content: any, codelinesFolderPath: string) {
    if (!this.isExistingDocumentName(documentName)) {
      throw new Error(`Document with name '${documentName}' does not exist.`);
    }
    this.documents[documentName].saveContent(content, codelinesFolderPath);
  }

  private isExistingDocumentName(name: string) {
    return !!this.documents[name];
  }
}
