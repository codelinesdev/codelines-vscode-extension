import * as path from 'path';
import {
  Serializable,
  createFolder,
  createFile,
  writeToFile,
} from '../utils';

export default class CodeBookDocument implements Serializable<CodeBookDocument> {
  public name: string;
  private rootPath: string; // A relative path to the .codelines folder
  public contentPath: string; // A relative path to the .codelines folder
  private emptyEditorContent = '{"blocks":[{"data":{},"depth":0,"entityRanges":[],"inlineStyleRanges":[],"key":"66rqv","text":"","type":"unstyled"}],"entityMap":{}}';

  public deserialize(obj: Record<string, any>): CodeBookDocument {
    this.name = obj.name;
    this.rootPath = obj.rootPath;
    this.contentPath = obj.contentPath;
    this.emptyEditorContent = obj.emptyEditorContent;
    return this;
  }

  // `relativeDocumentsFolderPath`is relative to the .codelines folder
  public constructor(name?: string, codelinesFolderPath?: string, relativeDocumentsFolderPath?: string) {
    if (!name && !codelinesFolderPath && !relativeDocumentsFolderPath) {
      // This is useful only when we are calling "new CodeBookDocument().deserialize(jsonObj)"
      this.name = '';
      this.rootPath = '';
      this.contentPath = '';
      return;
    }

    if (!name) {
      throw new Error('Specify `name` parameter.');
    } else if (!codelinesFolderPath) {
      throw new Error('Specify `codelinesFolderPath` parameter.');
    } else if (!relativeDocumentsFolderPath) {
      throw new Error('Specify `relativeDocumentsFolderPath` parameter.');
    }

    this.name = name;

    // We use a relative path because this value is saved in the settings.json
    // If the path would be absolute, it would break when the project is opened
    // on a different machine
    this.rootPath = path.join(relativeDocumentsFolderPath, this.name);
    this.contentPath = path.join(this.rootPath, 'content.json');

    // Create a root folder for this document
    const absoluteRootPath = path.join(codelinesFolderPath, relativeDocumentsFolderPath, this.name);
    createFolder(absoluteRootPath);

    // Create content.json (e.i.: editor content) file for this document
    const absoluteContentPath = path.join(absoluteRootPath, 'content.json');
    createFile(absoluteContentPath, this.emptyEditorContent);
  }

  public saveContent(content: any, codelinesFolderPath: string) {
    if (!content) {
      throw new Error('Cannot save undefined content');
    }

    const absoluteContentPath = path.join(codelinesFolderPath, this.contentPath);

    const stringified = JSON.stringify(content);
    writeToFile(absoluteContentPath, stringified);
  }
}
