// eslint-disable-next-line import/no-unresolved
import * as vscode from 'vscode';
import {
  Mark,
} from 'marker';
import { getLines } from './utils';

export enum MarkDecorationState {
  Normal,
  Highlighted
}

export class CodeMarkDecoration {
  public mark: Mark;

  private singleLineDecoration: vscode.TextEditorDecorationType;

  private wholeLineDecoration: vscode.TextEditorDecorationType;

  private static createNormalDecoration(isWholeLine: boolean) {
    return vscode.window.createTextEditorDecorationType({
      isWholeLine,
      backgroundColor: 'rgba(27, 173, 132, 0.1)',
      overviewRulerColor: 'rgba(27, 173, 132, 0.45)',
      rangeBehavior: vscode.DecorationRangeBehavior.ClosedOpen,
    });
  }

  private static createHighlightedDecoration(isWholeLine: boolean) {
    return vscode.window.createTextEditorDecorationType({
      isWholeLine,
      backgroundColor: 'rgba(27, 173, 132, 0.8)',
      overviewRulerColor: 'rgba(27, 173, 132, 1.0)',
      rangeBehavior: vscode.DecorationRangeBehavior.ClosedOpen,
    });
  }

  public constructor(mark: Mark, state: MarkDecorationState) {
    this.mark = mark;
    if (state === MarkDecorationState.Normal) {
      this.singleLineDecoration = CodeMarkDecoration.createNormalDecoration(false);
      this.wholeLineDecoration = CodeMarkDecoration.createNormalDecoration(true);
    } else {
      this.singleLineDecoration = CodeMarkDecoration.createHighlightedDecoration(false);
      this.wholeLineDecoration = CodeMarkDecoration.createHighlightedDecoration(true);
    }
  }

  public dispose() {
    this.singleLineDecoration.dispose();
    this.wholeLineDecoration.dispose();
  }

  public addToEditor(editor: vscode.TextEditor) {
    const { singleLines, wholeLines } = getLines(editor.document, this.mark);
    if (wholeLines.length === 0) {
      editor.setDecorations(this.singleLineDecoration, singleLines);
    } else {
      editor.setDecorations(this.wholeLineDecoration, wholeLines.concat(singleLines));
    }
  }
}
