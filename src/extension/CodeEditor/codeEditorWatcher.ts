// eslint-disable-next-line import/no-unresolved
import * as vscode from 'vscode';

import {
  CodeBookMarker,
  Mark,
  MarkChange,
  MarkChangeType,
  MarkType,
} from 'marker';
import {
  CodeMarkDecoration,
  MarkDecorationState,
} from './codeMarkDecoration';
import {
  getRelativePath, getEditorsForPath, getBlockFromSelection, openEditorForPath, getFirstBlockFromMark, getRangeFromBlock,
} from './utils';
import { MarkerMap } from '../markerMap';

export class CodeEditorWatcher {
  private decorations: { [id: string]: CodeMarkDecoration } = {};

  private selections: vscode.Selection[] = [];

  private lastActiveTextEditor: vscode.TextEditor | undefined;

  private disposables: vscode.Disposable[];

  private markerMap: MarkerMap;
  private marker: CodeBookMarker | undefined;
  private codeBookId: string | undefined;

  // onDid change for every marker -> outside event
  // set one marker that it will get decoration info from

  public setDecorationMarker(codeBookId: string | undefined) {
    this.removeAllDecorations();
    this.codeBookId = codeBookId;
    if (codeBookId) {
      this.marker = this.markerMap[codeBookId];
      this.loadMarks();
    } else {
      this.marker = undefined;
    }
  }

  public async revealMark(mark: Mark, column: vscode.ViewColumn | undefined) {
    let editor = getEditorsForPath(mark.filePath).shift();
    if (!editor) {
      editor = await openEditorForPath(mark.filePath, column);
    }
    if (!editor) {
      return;
    }
    this.loadEditorMarks(editor);
    const block = getFirstBlockFromMark(mark);
    const range = getRangeFromBlock(editor.document, block);
    editor.revealRange(range, vscode.TextEditorRevealType.InCenter);
    this.addDecoration(mark, MarkDecorationState.Highlighted);
    await setTimeout(() => {
      this.rewriteDecoration(mark, MarkDecorationState.Normal);
    }, 1000);
  }

  public constructor(markerMap: MarkerMap) {
    this.markerMap = markerMap;

    this.disposables = [
      vscode.workspace.onDidChangeTextDocument(e => this.handleChangeTextDocument(e)),
      vscode.window.onDidChangeActiveTextEditor(e => this.handleChangeActiveTextEditor(e)),
      vscode.window.onDidChangeTextEditorSelection(e => this.handleChangeTextEditorSelection(e)),
      vscode.window.onDidChangeVisibleTextEditors(e => this.handleChangeVisibleTextEditors(e)),
    ];
  }

  public dispose() {
    this.removeAllDecorations();
    this.disposables.forEach(d => d.dispose());
  }

  public loadMarks() {
    vscode.window.visibleTextEditors.forEach(e => this.loadEditorMarks(e));
  }

  private handleChangeVisibleTextEditors(event: vscode.TextEditor[]) {
    event.forEach(e => this.loadEditorMarks(e));
  }

  private handleChangeTextDocument(event: vscode.TextDocumentChangeEvent) {
    if (/\.git$/g.test(event.document.fileName)) {
      console.log('CONTAINS .GIT', event.document);
      return;
    }
    const edits = event.contentChanges.map(c => (
      { text: c.text, block: { offset: c.rangeOffset, length: c.rangeLength } }
    ));
    const relativePath = getRelativePath(event.document.fileName);
    if (!relativePath) {
      return;
    }

    const allCodeBookIds = Object.keys(this.markerMap);
    const notSelectedIds = allCodeBookIds.filter(id => id !== this.codeBookId);
    const notSelectedMarkers = notSelectedIds.map(id => this.markerMap[id]);
    if (this.marker) {
      const changes = this.marker.processEdits(relativePath, edits);
      const bookChanges = changes.filter(markChange => markChange.mark.type === MarkType.BookMark);
      const codeChanges = changes.filter(markChange => markChange.mark.type === MarkType.CodeMark);
      codeChanges.forEach(c => this.renderDecorationChange(c));
      if (bookChanges.length > 0) {
        this.onDidBookMarksChangedEmitter.fire(bookChanges);
      }
    }
    notSelectedMarkers.forEach(m => m.processEdits(relativePath, edits));
  }

  public onDidBookMarksChangedEvent(handler: (event: MarkChange[]) => any) {
    return this.onDidBookMarksChangedEmitter.event(handler);
  }

  private onDidBookMarksChangedEmitter = new vscode.EventEmitter<MarkChange[]>();

  private handleChangeActiveTextEditor(event: vscode.TextEditor | undefined) {
    if (event) {
      this.lastActiveTextEditor = event;
      this.loadEditorMarks(event);
    }
  }

  private handleChangeTextEditorSelection(event: vscode.TextEditorSelectionChangeEvent) {
    this.lastActiveTextEditor = event.textEditor;
    this.selections = [...event.selections];
  }

  public removeDecoration(mark: Mark) {
    const decoration = this.decorations[mark.id];
    if (decoration) {
      decoration.dispose();
      delete this.decorations[mark.id];
    }
  }

  public removeAllDecorations() {
    Object.keys(this.decorations).forEach((key) => {
      const decoration = this.decorations[key];
      decoration.dispose();
    });
    this.decorations = {};
  }

  public rewriteDecoration(mark: Mark, state: MarkDecorationState) {
    const decoration = this.decorations[mark.id];
    if (decoration) {
      decoration.dispose();
      this.addDecoration(mark, state);
    }
  }

  public addDecoration(mark: Mark, state: MarkDecorationState) {
    this.removeDecoration(mark);
    const decoration = new CodeMarkDecoration(mark, state);
    this.decorations[mark.id] = decoration;
    const editors = getEditorsForPath(mark.filePath);
    editors.forEach(e => decoration.addToEditor(e));
  }

  public loadEditorMarks(editor: vscode.TextEditor) {
    const relativePath = getRelativePath(editor.document.fileName);
    if (!relativePath) {
      return;
    }
    if (!this.marker) {
      return;
    }
    const marks = this.marker.getMarksForFile(relativePath);

    marks.forEach((m) => {
      let decoration = this.decorations[m.id];
      if (!decoration) {
        decoration = new CodeMarkDecoration(m, MarkDecorationState.Normal);
        this.decorations[m.id] = decoration;
      }
      decoration.addToEditor(editor);
    });
  }

  public getMarkings() {
    if (!this.lastActiveTextEditor) {
      return null;
    }
    const { document } = this.lastActiveTextEditor;

    const relativePath = getRelativePath(document.fileName);
    if (!relativePath) {
      return undefined;
    }
    const blocks = this.selections.map(s => getBlockFromSelection(document, s));
    return { filePath: relativePath, blocks };
  }

  public renderDecorationChange(change: MarkChange) {
    switch (change.changeType) {
      case MarkChangeType.Change:
        this.rewriteDecoration(change.mark, MarkDecorationState.Normal);
        break;
      case MarkChangeType.Create:
        this.addDecoration(change.mark, MarkDecorationState.Normal);
        break;
      case MarkChangeType.Delete:
        this.removeDecoration(change.mark);
        break;
      default:
        break;
    }
  }
}
