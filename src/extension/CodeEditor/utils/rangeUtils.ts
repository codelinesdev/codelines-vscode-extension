// eslint-disable-next-line import/no-unresolved
import * as vscode from 'vscode';
import { Block, Mark } from 'marker';

export function sliceRange(range: vscode.Range): [vscode.Range, vscode.Range | null, vscode.Range | null] {
  if (range.isSingleLine) {
    return [range, null, null];
  }
  const firstLine = new vscode.Range(new vscode.Position(range.start.line, range.start.character), new vscode.Position(range.start.line, Number.MAX_SAFE_INTEGER));
  let wholeLines = null;
  const endLine = new vscode.Range(new vscode.Position(range.end.line, 0), new vscode.Position(range.end.line, range.end.character));

  if (range.end.line - range.start.line > 1) {
    wholeLines = new vscode.Range(new vscode.Position(range.start.line + 1, 0), new vscode.Position(range.end.line - 1, Number.MAX_SAFE_INTEGER));
  }
  return [firstLine, wholeLines, endLine];
}

export function getRangeFromBlock(document: vscode.TextDocument, block: Block) {
  return new vscode.Range(document.positionAt(block.offset), document.positionAt(block.offset + block.length));
}

export function getRangesFromMark(document: vscode.TextDocument, mark: Mark) {
  return mark.blocks.map(block => getRangeFromBlock(document, block));
}