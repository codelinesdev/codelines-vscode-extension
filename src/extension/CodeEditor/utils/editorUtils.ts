// eslint-disable-next-line import/no-unresolved
import * as vscode from 'vscode';
import { Mark } from 'marker';
import { getAbsolutePath } from './pathUtils';
import { sliceRange, getRangesFromMark } from './rangeUtils';

export function getDocumentForPath(relativePath: string) {
  const absolutePath = getAbsolutePath(relativePath);
  if (!absolutePath) {
    return undefined;
  }
  return vscode.workspace.textDocuments.find(d => d.fileName === absolutePath);
}

export function getEditorsForPath(relativePath: string) {
  const absolutePath = getAbsolutePath(relativePath);
  if (!absolutePath) {
    return [];
  }
  return vscode.window.visibleTextEditors.filter(e => e.document.fileName === absolutePath);
}

export function getEditorColumnsCount() {
  return new Set(vscode.window.visibleTextEditors.map(e => e.viewColumn)).size;
}

export async function openEditorForPath(filePath: string, column: vscode.ViewColumn | undefined) {
  const absolutePath = getAbsolutePath(filePath);
  if (!absolutePath) {
    return undefined;
  }
  const document = await vscode.workspace.openTextDocument(absolutePath);
  if (getEditorColumnsCount() === 0) {
    const editor = vscode.window.showTextDocument(document, vscode.ViewColumn.Beside);
    await vscode.commands.executeCommand('workbench.action.moveActiveEditorGroupLeft');
    return editor;
  }
  if (column) {
    if (column === vscode.ViewColumn.One) {
      const editor = vscode.window.showTextDocument(document, vscode.ViewColumn.Beside);
      await vscode.commands.executeCommand('workbench.action.moveActiveEditorGroupLeft');
      return editor;
    }
    const editor = vscode.window.showTextDocument(document, vscode.ViewColumn.One);
    //await vscode.commands.executeCommand('workbench.action.moveActiveEditorGroupLeft');
    return editor;
  }
  const editor = vscode.window.showTextDocument(document, vscode.ViewColumn.Beside);
  await vscode.commands.executeCommand('workbench.action.moveActiveEditorGroupLeft');
  return editor;
}

export function getLines(document: vscode.TextDocument, mark: Mark) {
  const ranges = getRangesFromMark(document, mark);
  return ranges.reduce((acc, range) => {
    const [first, whole, last] = sliceRange(range);
    acc.singleLines.push(first);
    if (whole) {
      acc.wholeLines.push(whole);
    }
    if (last) {
      acc.singleLines.push(last);
    }
    return acc;
  }, { singleLines: [] as vscode.Range[], wholeLines: [] as vscode.Range[] });
}

export function getEditorsCount() {
  return vscode.window.visibleTextEditors.length;
}

export function getEditorLanguage(editor: vscode.TextEditor) {
  return editor.document.languageId;
}

export function getEditorLineCount(editor: vscode.TextEditor) {
  return editor.document.lineCount;
}

export function getEditorVersion(editor: vscode.TextEditor) {
  return editor.document.version;
}

export function getPathLanguage(filePath: string) {
  const editor = vscode.window.visibleTextEditors.find(e => e.document.fileName === filePath);
  return editor ? getEditorLanguage(editor) : undefined;
}

export function getPathLineCount(filePath: string) {
  const editor = vscode.window.visibleTextEditors.find(e => e.document.fileName === filePath);
  return editor ? getEditorLineCount(editor) : undefined;
}

export function getPathVersion(filePath: string) {
  const editor = vscode.window.visibleTextEditors.find(e => e.document.fileName === filePath);
  return editor ? getEditorVersion(editor) : undefined;
}

// TODO: editor enumerator
// export function getAllEditors() {
//   workbench.action.nextEditor
// }
