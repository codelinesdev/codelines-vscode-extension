// eslint-disable-next-line import/no-unresolved
import * as vscode from 'vscode';

export function getBlockFromSelection(document: vscode.TextDocument, selection: vscode.Selection) {
  const offset = document.offsetAt(selection.start);
  const endset = document.offsetAt(selection.end);
  return { offset, length: endset - offset };
}
