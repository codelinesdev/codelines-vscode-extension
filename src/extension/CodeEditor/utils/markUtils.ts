import { Mark, Block } from 'marker';
// eslint-disable-next-line import/no-unresolved
import { window } from 'vscode';
import { getRangesFromMark } from './rangeUtils';
import { getDocumentForPath } from './editorUtils';


export function getFirstPositionFromMark(mark: Mark) {
  return mark.blocks.reduce((acc, block) => (block.offset < acc
    ? block.offset
    : acc), Number.MAX_SAFE_INTEGER);
}

export function getFirstBlockFromMark(mark: Mark) {
  // eslint-disable-next-line no-nested-ternary
  return mark.blocks.reduce((acc, block) => (block.offset < acc.offset
    ? block
    : block.offset === acc.offset && block.length < acc.length
      ? block
      : acc), { offset: Number.MAX_SAFE_INTEGER, length: Number.MAX_SAFE_INTEGER });
}

export function getContentFromMark(mark: Mark) {
  const document = getDocumentForPath(mark.filePath);
  if (document) {
    return getRangesFromMark(document, mark).map(r => document.getText(r));
  }
  return undefined;
}

export function getMarkLineCount(mark: { blocks: Block[]; filePath: string }) {
  const editor = window.visibleTextEditors.find(e => e.document.fileName === mark.filePath);
  if (!editor) {
    return undefined;
  }
  return mark.blocks
    .map(b => editor.document.positionAt(b.offset + b.length).line - editor.document.positionAt(b.offset).line + 1)
    .reduce((prev, curr) => prev + curr, 0);
}

export function getMarkLength(mark: { blocks: Block[]; filePath: string }) {
  return mark.blocks.map(b => b.length).reduce((prev, curr) => prev + curr, 0);
}
