// eslint-disable-next-line import/no-unresolved
import * as vscode from 'vscode';
import * as path from 'path';

export function getRelativePath(absolutePath: string) {
  const folders = vscode.workspace.workspaceFolders;
  if (!folders || !vscode.workspace.name) {
    return undefined;
  }
  const workspaceRootPath = folders[0].uri.fsPath;
  return path.relative(workspaceRootPath, absolutePath);
}

export function getAbsolutePath(relativePath: string) {
  const folders = vscode.workspace.workspaceFolders;
  if (!folders || !vscode.workspace.name) {
    return undefined;
  }
  const workspaceRootPath = folders[0].uri.fsPath;
  return path.join(workspaceRootPath, relativePath);
}
