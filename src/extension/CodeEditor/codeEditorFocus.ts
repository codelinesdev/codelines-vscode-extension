import {
  EventEmitter,
  Disposable,
  languages,
  Position,
  window,
  TextDocument,
  TextEditorSelectionChangeEvent,
  HoverProvider,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import {
  CodeBookMarker,
  Mark,
  MarkType,
} from 'marker';
import { getRelativePath } from './utils';

export interface CodeMarkFocusEvent {
  marks: Mark[];
  position: Position;
  document: TextDocument;
}

export class CodeEditorFocus {
  private marker: CodeBookMarker;

  private onHoverInMarksEmitter = new EventEmitter<CodeMarkFocusEvent>();

  private onCaretInMarksEmitter = new EventEmitter<CodeMarkFocusEvent>();

  private disposables: Disposable[];

  public constructor(marker: CodeBookMarker) {
    this.marker = marker;
    languages.registerHoverProvider({ scheme: 'file' }, CodeEditorFocus.createHoverProvider(marker, this.onHoverInMarksEmitter));
    this.disposables = [
      window.onDidChangeTextEditorSelection(e => this.handleChangeTextEditorSelection(e)),
    ];
  }

  public dispose() {
    this.disposables.forEach(d => d.dispose());
  }

  private static createHoverProvider(marker: CodeBookMarker, emitter: EventEmitter<CodeMarkFocusEvent>): HoverProvider {
    return {
      provideHover(document: TextDocument, position: Position) {
        const marks = CodeEditorFocus.getCodeMarksOnPosition(document, position, marker);
        if (marks.length > 0) {
          emitter.fire({ marks, position, document });
        }
        return null;
      },
    };
  }

  private handleChangeTextEditorSelection(event: TextEditorSelectionChangeEvent) {
    const activeEditor = window.activeTextEditor;
    if (event.textEditor === activeEditor) {
      if (event.textEditor.selection.isEmpty) {
        const position = event.textEditor.selection.active;
        const { document } = event.textEditor;
        const marks = CodeEditorFocus.getCodeMarksOnPosition(document, position, this.marker);
        this.onCaretInMarksEmitter.fire({ marks, position, document });
      }
    }
  }

  public onHoverInMarksEvent(handler: (event: CodeMarkFocusEvent) => any) {
    this.onHoverInMarksEmitter.event(handler);
  }

  public onCaretInMarksEvent(handler: (event: CodeMarkFocusEvent) => any) {
    this.onCaretInMarksEmitter.event(handler);
  }

  private static getCodeMarksOnPosition(document: TextDocument, position: Position, marker: CodeBookMarker) {
    const relativePath = getRelativePath(document.fileName);
    if (!relativePath) {
      return [];
    }
    const offset = document.offsetAt(position);
    return marker.getMarksForOffset(relativePath, offset, MarkType.CodeMark);
  }
}
