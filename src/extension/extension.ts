import {
  ExtensionContext,
  commands,
  window,
  workspace,
  Terminal,
  languages,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import {
  MarkType,
  Mark,
  MarkChange,
  CodeBookMarker,
} from 'marker';

import { getTokens, setTokens } from './state/tokens';
import { downloadFromUrl, uploadToUrl } from './CodeBookSharing/urlStorage';

import { SidebarDataProvider } from './sidebarViewlet';
import { commandNames } from './codelinesCommands';
import { CodelinesPanel } from './CodelinesPanel';

import { CodeEditorWatcher } from './CodeEditor/codeEditorWatcher';
import { MarkDecorationState } from './CodeEditor/codeMarkDecoration';
//import { CodeEditorFocus } from './CodeEditor/codeEditorFocus';

// import { CodeBookSettings, CodeBookDocument } from './CodeBookSettings';
import CodelinesSettings from './CodelinesSettings';
import {
  MarkerMap,
  initializeAllCodeBookMarkers,
  initializeCodeBookMarker,
  closeAllCodeBookMarkers,
  closeCodeBookMarker,
} from './markerMap';
import {
 trackCloseWebview,
 trackOpenWebview,
 trackCloseCodeBook,
 trackCreateMark,
 trackDeleteMark,
 trackDeleteCodeBook,
 trackRevealCodeMark,
 trackLoadCodeBook,
 trackCreateCodeBook,
 trackSignOut,
 trackSignUp,
 trackSignIn,
 trackCreateCommnad,
 trackRunCommand,
} from './Analytics/mixpanel';

let workspaceName: string;
let workspaceRootPath: string;

let codelinesSettings: CodelinesSettings;
let codeEditorWatcher: CodeEditorWatcher;

const markerMap: MarkerMap = {};
let marker: CodeBookMarker;
let currentCodeBookId: string;

//let codeEditorFocus: CodeEditorFocus;
let codelinesPanel: CodelinesPanel;
let intervalTimeout: NodeJS.Timeout;
let focusedBookMark: Mark | null;
let hoveredBookMark: Mark | null;
let terminal: Terminal | null;

// async function initializeMarker(marksFilePath: string) {
//   try {
//     if (marker) {
//       await marker.close();
//     }
//     marker = new CodeBookMarker(marksFilePath);
//     return marker.initialize();
//   } catch (error) {
//     window.showErrorMessage('Cannot initialize marker');
//     throw new Error('Cannot initialize marker');
//   }
// }

function changeActiveCodeBook(codeBookId: string | undefined) {
  if (codeEditorWatcher) {
    codeEditorWatcher.setDecorationMarker(codeBookId);
  }
  if (codeBookId) {
    currentCodeBookId = codeBookId;
    if (codeEditorWatcher) {
      codeEditorWatcher.loadMarks();
    }
    marker = markerMap[codeBookId];
  }
}

function initializeSidebarButton(context: ExtensionContext) {
  const dp = new SidebarDataProvider(codelinesPanel);
  intervalTimeout = setInterval(() => {
    dp.refresh();
  }, 1000);
  context.subscriptions.push(
    window.registerTreeDataProvider('codelinesSidebarView', dp),
  );
}

// function loadCodeMarksForOpenedTextEditors() {
//   window.visibleTextEditors.forEach((e) => {
//     console.log('Loading marks for document...', e.document);
//     codeEditorWatcher.loadEditorMarks(e);
//   });
// }

function setCodeMarkDecorationsForBookMark(bookMark: Mark, decorationState: MarkDecorationState) {
  const referencedCodeMarkIds = bookMark.references.map(r => r.id);
  const codeMarks = referencedCodeMarkIds.map(id => marker.getMark(id));
  codeMarks.forEach((cm) => {
    if (cm) {
      codeEditorWatcher.rewriteDecoration(cm, decorationState);
    }
  });
}

// function removeCodeMarkDecorations

/////////
function handleBookEditorSaveContent(message: any) {
  const {
    editorContent,
    codeBookId,
    documentName,
  } = message.data;
  codelinesSettings.saveDocumentContent(codeBookId, documentName, editorContent);
}

function handleBookEditorCreateBookMark(message: any, context: ExtensionContext) {
  const {
    editorContent,
    bookMarkBlocks,
    bookEditorTextLength,
    codeBookId,
    documentName,
  } = message.data;
  console.log(`create book mark - ${codeBookId}/${documentName}`);

  const cm = codeEditorWatcher.getMarkings();

  if (cm) {
    const markChanges = marker.createBindedMarks(
      cm,
      { filePath: `${codeBookId}/${documentName}`, blocks: bookMarkBlocks },
    );

    //const codeMarkBlock = cm.blocks[0];
    //const codeMarkLength = codeMarkBlock ? codeMarkBlock.length : 0;

    const bookMarkBlock = bookMarkBlocks[0];
    const bookMarkLength = bookMarkBlock ? bookMarkBlock.length : 0;

    const codeMarkChanges = markChanges.filter(mc => mc.mark.type === MarkType.CodeMark);
    codeMarkChanges.forEach(c => codeEditorWatcher.renderDecorationChange(c));

    const bookMarkChanges = markChanges.filter(mc => mc.mark.type === MarkType.BookMark);
    codelinesPanel.bookEditorMessager.sendBookMarksDidChange(bookMarkChanges, editorContent);

    trackCreateMark({ bookMarkLength, bookEditorTextLength }, cm, context);
  }
}

// NEED TO SEND CODEBOOK ID TOO
// function handleBookEditorGetBookMarkForOffset(message: any) {
//   const {
//     offset,
//     editorContent,
//   } = message.data;
//   const bookMarks = marker.getMarksForOffset(workspaceName, offset, MarkType.BookMark);
//   codelinesPanel.bookEditorMessager.sendBookMarksForOffset(bookMarks, editorContent);
// }

function handleBookEditorSetFocusedBookMarkId(message: any) {
  const { bookMarkId } = message.data;
  let newFocusedBookMark: Mark | null = null;

  newFocusedBookMark = marker.getMark(bookMarkId);

  if (focusedBookMark && newFocusedBookMark) {
    if (focusedBookMark.id !== newFocusedBookMark.id) { // Was focused book mark before
      console.log('Setting new focused book mark', newFocusedBookMark);
      // De-higlight previous code marks
      const oldCodeMarkIds = focusedBookMark.references.map(r => r.id);
      const oldCodeMarks = oldCodeMarkIds.map(id => marker.getMark(id));
      oldCodeMarks.forEach((cm) => {
        if (cm) {
          codeEditorWatcher.rewriteDecoration(cm, MarkDecorationState.Normal);
        }
      });

      focusedBookMark = newFocusedBookMark;

      const referencedCodeMarkIds = focusedBookMark.references.map(r => r.id);
      const codeMarks = referencedCodeMarkIds.map(id => marker.getMark(id));
      codeMarks.forEach((cm) => {
        if (cm) {
          // codeEditorWatcher.blinkMark(cm);
          codeEditorWatcher.rewriteDecoration(cm, MarkDecorationState.Highlighted);
        }
      });
    }
  } else if (!focusedBookMark && newFocusedBookMark) { // No focused book mark before
    console.log('Setting new focused book mark', newFocusedBookMark);
    // // De-higlight previous code marks
    // const oldCodeMarkIds = focusedBookMark.references.map(r => r.id);
    // const oldCodeMarks = oldCodeMarkIds.map(id => marker.getMark(id));
    // oldCodeMarks.forEach(cm => {
    //   if (cm) {
    //     codeEditorWatcher.setDecorationStateForMark(cm, MarkDecorationState.Normal);
    //   }
    // });

    focusedBookMark = newFocusedBookMark;

    const referencedCodeMarkIds = focusedBookMark.references.map(r => r.id);
    const codeMarks = referencedCodeMarkIds.map(id => marker.getMark(id));
    codeMarks.forEach((cm) => {
      if (cm) {
        // codeEditorWatcher.blinkMark(cm);
        codeEditorWatcher.rewriteDecoration(cm, MarkDecorationState.Highlighted);
      }
    });
  } else if (focusedBookMark && !newFocusedBookMark) {
    // De-higlight previous code marks
    const oldCodeMarkIds = focusedBookMark.references.map(r => r.id);
    const oldCodeMarks = oldCodeMarkIds.map(id => marker.getMark(id));
    oldCodeMarks.forEach((cm) => {
      if (cm) {
        // if this is commented then blinkin reveal will work ok, but marks will not fade
        codeEditorWatcher.rewriteDecoration(cm, MarkDecorationState.Normal);
      }
    });
    focusedBookMark = null;
  } else {
    focusedBookMark = null;
  }
}

function handleBookEditorBookMarks(message: any) {
  const {
    bookMarks,
    codeBookId,
    documentName,
  } = message.data;

  const marks: any[] = [];
  Object.entries(bookMarks).forEach(([bookMarkId, bookMark]: [string, any]) => {
    const { blocks } = bookMark;
    marks.push({ id: bookMarkId, blocks });
  });

  const changes = marker.rewriteFileMarks(`${codeBookId}/${documentName}`, marks);

  // Update code marks in a code editor
  changes
    .filter(c => c.mark.type === MarkType.CodeMark)
    .forEach(c => codeEditorWatcher.renderDecorationChange(c));
}

function handleBookEditorDeleteBookMark(message: any, context: ExtensionContext) {
  const {
    bookMarkId,
    editorContent,
  } = message.data;


  const changes = marker.deleteMark(bookMarkId);
  changes
  .filter(c => c.mark.type === MarkType.CodeMark)
  .forEach(c => codeEditorWatcher.renderDecorationChange(c));

  const bookMarkChanges = changes.filter(c => c.mark.type === MarkType.BookMark);
  codelinesPanel.bookEditorMessager.sendBookMarksDidChange(bookMarkChanges, editorContent);

  trackDeleteMark(context);
}

async function handleBookEditorShowCodeMarkForBookMark(message: any, context: ExtensionContext) {
  const {
    bookMarkId,
  } = message.data;
  const bookMark = marker.getMark(bookMarkId);
  if (!bookMark) {
    return undefined;
  }
  const codeMarkId = bookMark.references[0];
  if (!codeMarkId) {
    return undefined;
  }
  const codeMark = marker.getMark(codeMarkId.id);
  if (!codeMark) {
    return undefined;
  }

  const webviewColumn = codelinesPanel.getColumn();
  trackRevealCodeMark(context);
  return codeEditorWatcher.revealMark(codeMark, webviewColumn);
}

async function handleBookEditorNewCodeBookDocument(message: any) {
  // TODO
  console.log('IMPLEMENT handleBookEditorNewCodeBookDocument');

  // const { newDocumentName } = message.data;
  // try {
  //   await codeBookSettings.createCodeBookDocument(newDocumentName);
  //   mixpanel.track('Create Code Book Document', {
  //     sessionId,
  //     projectId: codeBookSettings.projectId,
  //     timestamp: new Date().toTimeString(),
  //   });
  // } catch (error) {
  //   console.error(error);
  //   window.showErrorMessage(`Cannot create document '${newDocumentName}'. ${error.message}`);
  // }

  // const docs = codeBookSettings.codeBookDocuments;
  // codelinesPanel.bookEditorMessager.sendCodeBookDocumentsUpdated(docs);
}

function handleBookEditorLoadCodeBookDocument(message: any, context: ExtensionContext) {
  const {
    codeBookId,
    documentName,
  } = message.data;
  const editorContent = codelinesSettings.getDocumentContent(codeBookId, documentName);
  // String in the format '<codeBookID>/<documentName>' is used as a file name for the marker
  const marks = marker.getMarksForFile(`${codeBookId}/${documentName}`);
  codelinesPanel.bookEditorMessager.sendLoadedCodeBookDocument(editorContent, marks);
  trackLoadCodeBook(context);
}

function handleBookEditorDeleteCodeBookDocument(message: any, context: ExtensionContext) {
  const { codeBookDocument } = message.data;
  try {
    const markChanges = marker.deleteMarksFromFile(codeBookDocument.contentPath);

    // Only handle code mark changes because the whole document where book marks were will be deleted
    const codeMarkChanges = markChanges.filter(mc => mc.mark.type === MarkType.CodeMark);
    codeMarkChanges.forEach(c => codeEditorWatcher.renderDecorationChange(c));

    codelinesSettings.deleteDocument(codeBookDocument);
  } catch (error) {
    console.error('Error while trying to delete a code book document', error);
  }
  trackDeleteCodeBook(codelinesSettings.codeBooks.length, context);
}

async function handleShowWebviweCommand(context: ExtensionContext) {
  //changeActiveCodeBook(currentCodeBookId);
  await codelinesPanel.showPanel();
  trackOpenWebview(context);
}

function handleHideWebviewCommand(context: ExtensionContext) {
  changeActiveCodeBook(undefined);
  codelinesPanel.hidePanel();
  trackCloseWebview(context);
}

async function handleBookEditorRequestTokens(context: ExtensionContext) {
  const tokens = await getTokens(context);
  codelinesPanel.bookEditorMessager.sendTokens(tokens.idToken, tokens.refreshToken);
}

async function handleBookEditorUploadCodeBook(message: any) {
  const {
    url,
    codebookId,
  } = message.data;
  const result = await uploadToUrl(url, codebookId, workspaceRootPath, workspaceName);
  codelinesPanel.bookEditorMessager.sendUploadCodeBook(result ? 'OK' : undefined);
}


async function handleBookEditorDownloadCodeBook(message: any) {
  const {
    url,
    codebookId,
  } = message.data;
  const result = await downloadFromUrl(url, codebookId, workspaceRootPath);
  codelinesPanel.bookEditorMessager.sendDownloadCodeBook(result ? 'OK' : undefined);
  //OPEN CODEBOOK IN EDITOR
}


async function handleBookEditorSaveTokens(message: any, context: ExtensionContext) {
  const {
    idToken,
    refreshToken,
    userId,
  } = message.data;
  setTokens(idToken, refreshToken, context);
}

function handleBookEditorSetHoveredBookMarkId(message: any) {
  const { bookMarkId } = message.data;

  if (!bookMarkId) {
    if (hoveredBookMark) {
      if (focusedBookMark && focusedBookMark.id !== hoveredBookMark.id) {
        setCodeMarkDecorationsForBookMark(hoveredBookMark, MarkDecorationState.Normal);
        hoveredBookMark = null;
      } else if (!focusedBookMark) {
        setCodeMarkDecorationsForBookMark(hoveredBookMark, MarkDecorationState.Normal);
        hoveredBookMark = null;
      }
    }
  }

  const newHoveredBookMark = marker.getMark(bookMarkId);
  if (newHoveredBookMark) {
    if (hoveredBookMark && focusedBookMark && focusedBookMark.id !== hoveredBookMark.id) {
      setCodeMarkDecorationsForBookMark(hoveredBookMark, MarkDecorationState.Normal);
    }
    if (focusedBookMark && focusedBookMark.id !== newHoveredBookMark.id) {
      setCodeMarkDecorationsForBookMark(newHoveredBookMark, MarkDecorationState.Highlighted);
      hoveredBookMark = newHoveredBookMark;
    } else if (!focusedBookMark) {
      setCodeMarkDecorationsForBookMark(newHoveredBookMark, MarkDecorationState.Highlighted);
      hoveredBookMark = newHoveredBookMark;
    }
  }
}

function handleBookEditorRunCommand(message: any, context: ExtensionContext) {
  const { commandString } = message.data;
  if (!terminal) {
    terminal = window.createTerminal('Codelines');
  }
  // 'true' - don't focus the terminal
  terminal.show(true);
  terminal.sendText(commandString);
  trackRunCommand(commandString, context);
}

async function handleBookEditorCreateCodeBook(context: ExtensionContext) {
  const codeBook = codelinesSettings.createCodeBook();
  await initializeCodeBookMarker(codelinesSettings.codelinesFolderPath, codeBook, markerMap);
  codelinesPanel.bookEditorMessager.sendCodeBookCreated(codeBook);
  trackCreateCodeBook(codelinesSettings.codeBooks.length, context);
}

function handleBookEditorListSavedCodeBooks() {
  const savedCodeBooks = codelinesSettings.codeBooks;
  codelinesPanel.bookEditorMessager.sendSavedCodeBooks(savedCodeBooks);
}

function handleBookEditorGetWorkspaceName() {
  codelinesPanel.bookEditorMessager.sendWorkspaceName(workspaceName);
}

async function handleBookEditorLoadCodeBook(message: any) {
  const { id } = message.data;
  const codeBook = codelinesSettings.getCodeBook(id);

  //const marksFilePath = path.join(codelinesSettings.codelinesFolderPath, codeBook.marksPath);

  changeActiveCodeBook(codeBook.id);
  // codeEditorFocus = new CodeEditorFocus(marker);
  // codeEditorFocus.onHoverInMarksEvent(e => console.log('Hover in marks:', e.marks));
  // codeEditorFocus.onCaretInMarksEvent(e => console.log('Caret in marks:', e.marks));

  codelinesPanel.bookEditorMessager.sendLoadedCodeBook(codeBook);
}

function handleBookEditorChangeCodeBookName(message: any) {
  const {
    id,
    name,
    description,
  } = message.data;

  try {
    codelinesSettings.changeCodeBookName(id, name);
    codelinesSettings.changeCodeBookDescription(id, description);
    const workspaceCodeBooks = codelinesSettings.codeBooks;
    codelinesPanel.bookEditorMessager.sendDidChangeCodeBookNameOk(workspaceCodeBooks);
  } catch (error) {
    codelinesPanel.bookEditorMessager.sendDidChangeCodeBookNameError(error.message);
  }
}

async function handleBookEditorDeleteCodeBook(message: any) {
  const { id } = message.data;
  const codeBook = codelinesSettings.getCodeBook(id);
  await closeCodeBookMarker(codeBook, markerMap);
  codelinesSettings.deleteCodeBook(id);
  const workspaceCodeBooks = codelinesSettings.codeBooks;
  codelinesPanel.bookEditorMessager.sendDidDeleteCodeBook(workspaceCodeBooks);
}

function handleBookEditorMarksChange(changes: MarkChange[]) {
  codelinesPanel.bookEditorMessager.sendBookMarksDeletedWithCodeMarks(changes);
}

function handleBookEditorDidCloseCodeBook(message: any, context: ExtensionContext) {
  //const { id } = message.data;
  changeActiveCodeBook(undefined);
  trackCloseCodeBook(context);
}

function handleBookEditorTrackEvent(message: any, context: ExtensionContext) {
  const {
    name,
    description,
    trackingData,
  } = message.data;
  switch (name) {
    case 'COMMAND_CREATE':
      trackCreateCommnad(trackingData.command, trackingData.bookEditorTextLength, context);
      break;
    case 'AUTH_SIGN_IN':
      trackSignIn(trackingData.userId, context);
      break;
    case 'AUTH_SIGN_UP':
      trackSignUp(trackingData.userId, trackingData.email, trackingData.username, context);
      break;
    case 'AUTH_SIGN_OUT':
      trackSignOut(context);
      break;
    default:
      break;
  }
  console.log('handleBookEditorTrackEvent message.data', message.data);
}
/////////

export function handleDidCloseWebview(context: ExtensionContext) {
  changeActiveCodeBook(undefined);
  codeEditorWatcher.removeAllDecorations();
  //console.log('HIDEEE');
  trackCloseWebview(context);
}

export async function activate(context: ExtensionContext) {
  console.log('Extension Codelines Activated');
  languages.getLanguages().then(l => console.log('languages', l));
  const folders = workspace.workspaceFolders;
  if (!folders || !workspace.name) {
    // TODO: Handle more user friendly?
    const message = 'Cannot use Codelines without a workspace';
    window.showErrorMessage(message);
    throw new Error(message);
  }
  workspaceRootPath = folders[0].uri.fsPath;
  workspaceName = workspace.name;

  codelinesSettings = CodelinesSettings.getInstance();
  await initializeAllCodeBookMarkers(codelinesSettings, markerMap);
  codeEditorWatcher = new CodeEditorWatcher(markerMap);

  codelinesPanel = new CodelinesPanel(context.extensionPath);
  codelinesPanel.onDidCloseWebviewOneHandler(() => handleDidCloseWebview(context));

  context.subscriptions.push(
    codeEditorWatcher.onDidBookMarksChangedEvent((e: any) => handleBookEditorMarksChange(e)),

    codelinesPanel.bookEditorWatcher.onBookEditorSaveContent((e: any) => handleBookEditorSaveContent(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorCreateBookMark((e: any) => handleBookEditorCreateBookMark(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorSetFocusedBookMarkId((e: any) => handleBookEditorSetFocusedBookMarkId(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorBookMarks((e: any) => handleBookEditorBookMarks(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorDeleteBookMark((e: any) => handleBookEditorDeleteBookMark(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorShowCodeMarkForBookMark(async (e: any) => handleBookEditorShowCodeMarkForBookMark(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorCreateNewCodeBookDocument((e: any) => handleBookEditorNewCodeBookDocument(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorLoadCodeBookDocument((e: any) => handleBookEditorLoadCodeBookDocument(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorDeleteCodeBookDocument((e: any) => handleBookEditorDeleteCodeBookDocument(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorUploadCodeBook(async (e: any) => handleBookEditorUploadCodeBook(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorSaveTokens(async (e: any) => handleBookEditorSaveTokens(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorDownloadCodeBook(async (e: any) => handleBookEditorDownloadCodeBook(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorSetHoveredBookMarkId((e: any) => handleBookEditorSetHoveredBookMarkId(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorRunCommand((e: any) => handleBookEditorRunCommand(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorRequestTokens(async () => handleBookEditorRequestTokens(context)),

    codelinesPanel.bookEditorWatcher.onBookEditorCreateCodeBook(async () => handleBookEditorCreateCodeBook(context)),
    codelinesPanel.bookEditorWatcher.onBookEditoreListSavedCodeBooks(() => handleBookEditorListSavedCodeBooks()),
    codelinesPanel.bookEditorWatcher.onBookEditorGetWorkspaceName(() => handleBookEditorGetWorkspaceName()),
    codelinesPanel.bookEditorWatcher.onBookEditorLoadCodeBook((e: any) => handleBookEditorLoadCodeBook(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorChangeCodeBookName((e: any) => handleBookEditorChangeCodeBookName(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorDeleteCodeBook(async (e: any) => handleBookEditorDeleteCodeBook(e)),
    codelinesPanel.bookEditorWatcher.onBookEditorDidCloseCodeBook((e: any) => handleBookEditorDidCloseCodeBook(e, context)),
    codelinesPanel.bookEditorWatcher.onBookEditorTrackEvent((e: any) => handleBookEditorTrackEvent(e, context)),

    commands.registerCommand(commandNames.HIDE_WEBVIEW, () => handleHideWebviewCommand(context)),
    commands.registerCommand(commandNames.SHOW_WEBVIEW, async () => handleShowWebviweCommand(context)),
    {
      async dispose() {
        await closeAllCodeBookMarkers(codelinesSettings, markerMap);
        changeActiveCodeBook(undefined);
      },
    },
    codeEditorWatcher,
  );
  initializeSidebarButton(context);
}

export function deactivate() {
  console.log('Extension Dectivated');
  clearInterval(intervalTimeout);
}
