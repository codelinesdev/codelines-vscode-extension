import adjectives from './adjectives';
import nouns from './nouns';

function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

export function generateName() {
  const rndAdjIndex = Math.floor(Math.random() * adjectives.length);
  const rndNounIndex = Math.floor(Math.random() * nouns.length);

  const adj = capitalize(adjectives[rndAdjIndex]);
  const noun = capitalize(nouns[rndNounIndex]);

  return `${adj}${noun}`;
}
