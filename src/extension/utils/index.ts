import * as fs from 'fs';

export interface Serializable<T> {
  deserialize(obj: Record<string, any>): T;
}

export function createFolder(path: string) {
  if (fs.existsSync(path)) {
    throw new Error(`Folder '${path}' already exists.`);
  }

  fs.mkdirSync(path);
}

// TODO: createFile() and writeToFile() might be different in the future;
export function createFile(path: string, content: any) {
  if (fs.existsSync(path)) {
    throw new Error(`File '${path}' already exists.`);
  }

  fs.writeFileSync(path, content);
}

export function writeToFile(path: string, content: any) {
  if (!fs.existsSync(path)) {
    throw new Error(`File '${path}' doesn't exist.`);
  }
  fs.writeFileSync(path, content);
}
