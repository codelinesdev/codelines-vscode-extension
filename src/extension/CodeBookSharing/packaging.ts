import * as fs from 'fs';
import * as archiver from 'archiver';
import * as path from 'path';
import * as readline from 'readline';

// for given codebook id return path to it int he project, if not found, returns undefined
export function resolveCodeBookPath(codebookId: string, workspaceRootPath: string): string {
  return path.join(workspaceRootPath, '.codelines', 'codeBooks', codebookId);
}

export async function getAllCodebookIds(workspaceRootPath: string) {
  const codeBooksPath = path.join(workspaceRootPath, '.codelines', 'codeBooks');
  try {
    const codeBooks = fs.readdirSync(codeBooksPath);
    console.log(`found ${codeBooks.length} codebooks in this project`);
    return codeBooks;
  } catch (error) {
    console.error('error reading codebooks folder', error);
  }
  return [];
}

export async function getLinesFromCodelinesIgnore(workspaceRootPath: string) {
  // Returns all an array of all non-comment lines from .codelinesignore
  try {
    const fileStream = fs.createReadStream(`${workspaceRootPath}/.codelinesignore`);
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity,
    });
    // Note: we use the crlfDelay option to recognize all instances of CR LF
    // ('\r\n') in file as a single line break.

    const lines: string[] = [];
    rl.on('line', line => lines.push(line));

    await new Promise((resolve) => {
      rl.once('close', () => resolve());
    });
    return lines;
  } catch (error) {
    throw error;
  }
}

export async function zipCodeBook(codeBookId: string, workspaceRootPath: string, workspaceName: string) {
  const pathToZippedCodeBook = `${workspaceRootPath}/${workspaceName}.zip`;
  try {
    const out = fs.createWriteStream(pathToZippedCodeBook);
    const archive = archiver('zip');

    archive.on('error', (error) => {
      throw error;
    });

    const ignore = await getLinesFromCodelinesIgnore(workspaceRootPath);
    const allCodeBooksId = await getAllCodebookIds(workspaceRootPath);
    const otherCodeBookIds = allCodeBooksId.filter(c => c !== codeBookId);
    const otherCodeBookPaths = otherCodeBookIds.map(c => path.join('.codelines', 'codeBooks', c));
    // add temporal settings to ignore then switch them back
    const ignoreWithCodebooks = ignore.concat(otherCodeBookPaths);
    console.log('ignore', ignoreWithCodebooks);
    archive.pipe(out);
    archive.glob('{.codelines/,}**/*', {
      cwd: workspaceRootPath,
      ignore: ignoreWithCodebooks,
    }, {});

    const settingsPath = path.join(workspaceRootPath, '.codelines', 'settings.json');
    const tempSettingsPath = path.join(workspaceRootPath, '.codelines', '.temp.settings.json');
    fs.copyFileSync(settingsPath, tempSettingsPath);
    const settings = JSON.parse(fs.readFileSync(settingsPath, 'utf-8'));
    settings.lastOpenedCodeBookDocument = undefined;
    settings.codeBookDocuments = settings.codeBookDocuments.filter((d: any) => d.name === codeBookId);
    fs.writeFileSync(settingsPath, JSON.stringify(settings));

    await archive.finalize();

    fs.copyFile(tempSettingsPath, settingsPath, async () => {
      fs.unlink(tempSettingsPath, () => {});
    });

    //const codeBookId = randomBytes(6).toString('hex'); // 1B in hex is 2 characters = 12 characters

    // .toLowerCase()
    // .replace(/^\W+|\W+$/g, '') // Trim all non-word characters from both sides of the string
    // .replace(/\W/g, '-') // Replace non-word characters by hyphens
    // .replace(/(.)(?=-*\1), ''/); // Squash multiple '-' into a single hyphen

    return pathToZippedCodeBook;
  } catch (error) {
    console.error(`cannot archive file ${codeBookId}`, error);
  }
  return undefined;
}
