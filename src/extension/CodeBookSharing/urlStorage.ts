import * as fs from 'fs';
import axios from 'axios';
import * as request from 'request';
import { zipCodeBook, resolveCodeBookPath } from './packaging';

export async function downloadFromUrl(url: string, codebookId: string, workspaceRootPath: string) {
  try {
    // this should be somewhere outside?
    const filePath = resolveCodeBookPath(codebookId, workspaceRootPath);
    if (!filePath) {
      console.log('cannot download to this location');
      return false;
    }
    const response = await axios.get(url);
    const file = fs.createWriteStream(filePath);
    await response.data.pipe(file);
    return true;
  } catch (error) {
    console.error(`cannot download file from ${url} to ${codebookId}`, error);
  }
  return false;
}

export async function uploadToUrl(url: string, codebookId: string, workspaceRootPath: string, workspaceName: string) {
  console.log(`UPLOADING CODEBOOK ${codebookId}`);
  let zipPath: string | undefined;
  try {
    const filePath = await zipCodeBook(codebookId, workspaceRootPath, workspaceName);
    zipPath = filePath;
    console.log('upload file path', filePath);
    if (!filePath) {
      console.log('cannot upload from this location');
      return false;
    }

    return new Promise((resolve, reject) => {
      fs.createReadStream(filePath).pipe(request.put(url, undefined, (error) => {
        if (error) {
          reject(error);
        } else {
          console.log('uploaded');
          resolve(true);
        }
      }));
    });
  } catch (error) {
    console.error(`cannot upload file from ${codebookId} to ${url}`, error);
  } finally {
    if (zipPath) {
      fs.unlink(zipPath, () => {});
    }
  }
  return false;
}
