import {
  Webview,
  Disposable,
  EventEmitter,
  Event,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';

import { messageType } from './shared/messageManager';

export class BookEditorWatcher {
  private webview: Webview | undefined;
  private disposables: Disposable[] = [];

  // TODO: Remove 'any' and create types for each event
  // TODO: disposables

  // private readonly bookEditorReadyEmitter = new EventEmitter<any>();
  // public onBookEditorReady: Event<any>;

  private readonly bookEditorContentUpdatedEmitter = new EventEmitter<any>();
  public onBookEditorContentUpdated: Event<any>;

  private readonly bookEditorSaveContentEmitter = new EventEmitter<any>();
  public onBookEditorSaveContent: Event<any>;

  private readonly bookEditorCreateBookMarkEmitter = new EventEmitter<any>();
  public onBookEditorCreateBookMark: Event<any>;

  private readonly bookEditorGetBookMarkForOffsetEmitter = new EventEmitter<any>();
  public onBookEditorGetBookMarkForOffset: Event<any>;

  // private readonly bookEditorSetFocusedBookMarksEmmiter = new EventEmitter<any>();
  // public onBookEditorSetFocusedBookMarks: Event<any>;

  private readonly bookEditorSetFocusedBookMarkIdEmitter = new EventEmitter<any>();
  public onBookEditorSetFocusedBookMarkId: Event<any>;

  private readonly bookEditorBookMarksEmitter = new EventEmitter<any>();
  public onBookEditorBookMarks: Event<any>;

  private readonly bookEditorDeleteBookMarkEmitter = new EventEmitter<any>();
  public onBookEditorDeleteBookMark: Event<any>;

  private readonly bookEditorShowCodeMarkForBookMarkEmitter = new EventEmitter<any>();
  public onBookEditorShowCodeMarkForBookMark: Event<any>;

  private readonly bookEditorCreateNewCodeBookDocumentEmitter = new EventEmitter<any>();
  public onBookEditorCreateNewCodeBookDocument: Event<any>

  private readonly bookEditorLoadCodeBookDocumentEmitter = new EventEmitter<any>();
  public onBookEditorLoadCodeBookDocument: Event<any>

  private readonly bookEditorDeleteCodeBookDocumentEmitter = new EventEmitter<any>();
  public onBookEditorDeleteCodeBookDocument: Event<any>;

  private readonly bookEditorShareCodeBookEmitter = new EventEmitter<any>();
  public onBookEditorShareCodeBook: Event<any>;

  private readonly bookEditorSaveTokensEmitter = new EventEmitter<any>();
  public onBookEditorSaveTokens: Event<any>;

  private readonly bookEditorDownloadCodeBook = new EventEmitter<any>();
  public onBookEditorDownloadCodeBook: Event<any>;

  private readonly bookEditorUploadCodeBook = new EventEmitter<any>();
  public onBookEditorUploadCodeBook: Event<any>;

  private readonly bookEditorRequestTokensEmitter = new EventEmitter<any>();
  public onBookEditorRequestTokens: Event<any>;

  private readonly bookEditorSetHoveredBookMarkIdEmitter = new EventEmitter<any>();
  public onBookEditorSetHoveredBookMarkId: Event<any>;

  private readonly bookEditorRunCommandEmitter = new EventEmitter<any>();
  public onBookEditorRunCommand: Event<any>;

  private readonly bookEditorCreateCodeBookEmitter = new EventEmitter<any>();
  public onBookEditorCreateCodeBook: Event<any>;

  private readonly bookEditorUnloadCodeBookEmitter = new EventEmitter<any>();
  public onBookEditorUnloadCodeBook: Event<any>;

  private readonly bookEditorListSavedCodeBooksEmitter = new EventEmitter<any>();
  public onBookEditoreListSavedCodeBooks: Event<any>;

  private readonly bookEditorGetWorkspaceNameEmitter = new EventEmitter<any>();
  public onBookEditorGetWorkspaceName: Event<any>;

  private readonly bookEditorLoadCodeBookEmitter = new EventEmitter<any>();
  public onBookEditorLoadCodeBook: Event<any>;

  private readonly bookEditorChangeCodeBookNameEmitter = new EventEmitter<any>();
  public onBookEditorChangeCodeBookName: Event<any>;

  private readonly bookEditorDeleteCodeBookEmitter = new EventEmitter<any>();
  public onBookEditorDeleteCodeBook: Event<any>;

  private readonly bookEditorDidCloseCodeBookEmitter = new EventEmitter<any>();
  public onBookEditorDidCloseCodeBook: Event<any>;

  private readonly bookEditorTrackEventEmitter = new EventEmitter<any>();
  public onBookEditorTrackEvent: Event<any>;

  public constructor() {
    // this.onBookEditorReady = this.bookEditorReadyEmitter.event;
    this.onBookEditorContentUpdated = this.bookEditorContentUpdatedEmitter.event;
    this.onBookEditorSaveContent = this.bookEditorSaveContentEmitter.event;
    this.onBookEditorCreateBookMark = this.bookEditorCreateBookMarkEmitter.event;
    this.onBookEditorGetBookMarkForOffset = this.bookEditorGetBookMarkForOffsetEmitter.event;
    // this.onBookEditorSetFocusedBookMarks = this.bookEditorSetFocusedBookMarksEmmiter.event;
    this.onBookEditorSetFocusedBookMarkId = this.bookEditorSetFocusedBookMarkIdEmitter.event;

    this.onBookEditorBookMarks = this.bookEditorBookMarksEmitter.event;
    this.onBookEditorDeleteBookMark = this.bookEditorDeleteBookMarkEmitter.event;
    this.onBookEditorShowCodeMarkForBookMark = this.bookEditorShowCodeMarkForBookMarkEmitter.event;
    this.onBookEditorCreateNewCodeBookDocument = this.bookEditorCreateNewCodeBookDocumentEmitter.event;
    this.onBookEditorLoadCodeBookDocument = this.bookEditorLoadCodeBookDocumentEmitter.event;
    this.onBookEditorDeleteCodeBookDocument = this.bookEditorDeleteCodeBookDocumentEmitter.event;
    this.onBookEditorShareCodeBook = this.bookEditorShareCodeBookEmitter.event;
    this.onBookEditorSaveTokens = this.bookEditorSaveTokensEmitter.event;
    this.onBookEditorDownloadCodeBook = this.bookEditorDownloadCodeBook.event;
    this.onBookEditorUploadCodeBook = this.bookEditorUploadCodeBook.event;
    this.onBookEditorSetHoveredBookMarkId = this.bookEditorSetHoveredBookMarkIdEmitter.event;
    this.onBookEditorRunCommand = this.bookEditorRunCommandEmitter.event;
    this.onBookEditorRequestTokens = this.bookEditorRequestTokensEmitter.event;

    this.onBookEditorCreateCodeBook = this.bookEditorCreateCodeBookEmitter.event;
    this.onBookEditorUnloadCodeBook = this.bookEditorUnloadCodeBookEmitter.event;
    this.onBookEditoreListSavedCodeBooks = this.bookEditorListSavedCodeBooksEmitter.event;
    this.onBookEditorGetWorkspaceName = this.bookEditorGetWorkspaceNameEmitter.event;
    this.onBookEditorLoadCodeBook = this.bookEditorLoadCodeBookEmitter.event;
    this.onBookEditorChangeCodeBookName = this.bookEditorChangeCodeBookNameEmitter.event;
    this.onBookEditorDeleteCodeBook = this.bookEditorDeleteCodeBookEmitter.event;
    this.onBookEditorDidCloseCodeBook = this.bookEditorDidCloseCodeBookEmitter.event;
    this.onBookEditorTrackEvent = this.bookEditorTrackEventEmitter.event;
  }

  public watchBookEditor(webview: Webview) {
    this.webview = webview;
    this.webview.onDidReceiveMessage(m => this.handleBookEditorMessage(m), null, this.disposables);
  }

  public dispose() {
    this.webview = undefined;
    while (this.disposables.length) {
      const x = this.disposables.pop();
      if (x) {
        x.dispose();
      }
    }
  }

  private async handleBookEditorMessage(message: any) {
    if (!this.webview) {
      console.error('BookEditorWatcher cannot handle messages from webview because webview is undefined');
    }
    console.log(`Webview message '${message.type}'`, message);

    switch (message.type) {
      case messageType.BOOK_EDITOR.CONTENT_UPDATED: {
        this.bookEditorContentUpdatedEmitter.fire(message);
        break;
      }
      case messageType.SAVE_EDITOR_CONTENT: {
        this.bookEditorSaveContentEmitter.fire(message);
        break;
      }
      case messageType.CREATE_BOOK_MARK: {
        this.bookEditorCreateBookMarkEmitter.fire(message);
        break;
      }
      case messageType.BOOK_EDITOR.GET_BOOK_MARK_FOR_OFFSET: {
        this.bookEditorGetBookMarkForOffsetEmitter.fire(message);
        break;
      }
      case messageType.SET_FOCUSED_BOOK_MARK: {
        // this.bookEditorSetFocusedBookMarksEmmiter.fire(message);
        this.bookEditorSetFocusedBookMarkIdEmitter.fire(message);
        break;
      }
      case messageType.BOOK_MARKS_IN_EDITOR: {
        this.bookEditorBookMarksEmitter.fire(message);
        break;
      }
      case messageType.DELETE_BOOK_MARK: {
        this.bookEditorDeleteBookMarkEmitter.fire(message);
        break;
      }
      case messageType.SHOW_CODE_MARK: {
        this.bookEditorShowCodeMarkForBookMarkEmitter.fire(message);
        break;
      }
      case messageType.CODE_BOOK.CREATE_NEW_CODE_BOOK_DOCUMENT: {
        this.bookEditorCreateNewCodeBookDocumentEmitter.fire(message);
        break;
      }
      case messageType.CODE_BOOK.LOAD_CODE_BOOK_DOCUMENT: {
        this.bookEditorLoadCodeBookDocumentEmitter.fire(message);
        break;
      }
      case messageType.CODE_BOOK.DELETE_CODE_BOOK_DOCUMENT: {
        this.bookEditorDeleteCodeBookDocumentEmitter.fire(message);
        break;
      }
      case messageType.CODE_BOOK.SHARE_CODE_BOOK: {
        this.bookEditorShareCodeBookEmitter.fire(message);
        break;
      }
      case messageType.SAVE_TOKENS: {
        this.bookEditorSaveTokensEmitter.fire(message);
        break;
      }
      case messageType.DOWNLOAD_CODEBOOK: {
        this.bookEditorDownloadCodeBook.fire(message);
        break;
      }
      case messageType.UPLOAD_CODEBOOK: {
        this.bookEditorUploadCodeBook.fire(message);
        break;
      }
      case messageType.REQUEST_TOKENS: {
        this.bookEditorRequestTokensEmitter.fire(message);
        break;
      }
      case messageType.SET_HOVERED_BOOK_MARK: {
        this.bookEditorSetHoveredBookMarkIdEmitter.fire(message);
        break;
      }
      case messageType.RUN_COMMAND: {
        this.bookEditorRunCommandEmitter.fire(message);
        break;
      }
      case messageType.CREATE_CODE_BOOK: {
        this.bookEditorCreateCodeBookEmitter.fire(message);
        break;
      }
      case messageType.GET_WORKSPACE_CODE_BOOKS: {
        this.bookEditorListSavedCodeBooksEmitter.fire(message);
        break;
      }
      case messageType.BOOK_EDITOR.UNLOAD_CODE_BOOK: {
        this.bookEditorUnloadCodeBookEmitter.fire(message);
        break;
      }
      case messageType.GET_WORKSPACE_NAME: {
        this.bookEditorGetWorkspaceNameEmitter.fire(message);
        break;
      }
      case messageType.LOAD_CODE_BOOK: {
        this.bookEditorLoadCodeBookEmitter.fire(message);
        break;
      }
      case messageType.CHANGE_CODE_BOOK_NAME: {
        this.bookEditorChangeCodeBookNameEmitter.fire(message);
        break;
      }
      case messageType.DELETE_CODE_BOOK: {
        this.bookEditorDeleteCodeBookEmitter.fire(message);
        break;
      }
      case messageType.DID_CLOSE_CODE_BOOK: {
        this.bookEditorDidCloseCodeBookEmitter.fire(message);
        break;
      }
      case messageType.MIXPANEL_TRACK_EVENT: {
        this.bookEditorTrackEventEmitter.fire(message);
        break;
      }
      default:
        console.log('Unknown webview message type', message.type);
        break;
    }
  }
}
