import {
  Disposable,
  WebviewPanel,
  window,
  Uri,
  ViewColumn,
  workspace,
  WebviewPanelOptions,
  WebviewOptions,
  Webview,
  commands,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import * as path from 'path';

import { Mark, MarkChange } from 'marker';
import { messageType } from '../shared/messageManager';
import { BookEditorWatcher } from '../BookEditorWatcher';
// import {
//   CodeBookSettings,
//   CodeBookDocument,
// } from '../CodeBookSettings';

import {
  CodeBook,
  CodeBookDocument,
} from '../CodelinesSettings';

import * as manifest from '../reformatted-asset-manifest.json';
import { getEditorColumnsCount } from '../CodeEditor/utils';

type EditorContent = any;
let panelColumn: ViewColumn | undefined;

class BookEditorMessager {
  private readonly webview: Webview

  public constructor(webview: Webview) {
    this.webview = webview;
  }

  public sendFocusBookEditor() {
    this.webview.postMessage({
      type: messageType.BOOK_EDITOR.FOCUS_BOOK_EDITOR,
    });
  }

  public sendUploadCodeBook(status: string | undefined) {
    this.webview.postMessage({
      type: messageType.UPLOAD_CODEBOOK,
      data: {
        status,
      },
    });
  }

  public sendDownloadCodeBook(status: string | undefined) {
    this.webview.postMessage({
      type: messageType.DOWNLOAD_CODEBOOK,
      data: {
        status,
      },
    });
  }

  public sendTokens(idToken: any, refreshToken: any) {
    this.webview.postMessage({
      type: messageType.REQUEST_TOKENS,
      data: {
        idToken,
        refreshToken,
      },
    });
  }

  public sendBookMarksDidChange(changes: MarkChange[], editorContent: EditorContent) {
    this.webview.postMessage({
      type: messageType.BOOK_EDITOR.BOOK_MARKS_DID_CHANGE,
      data: {
        changes,
        editorContent,
      },
    });
  }

  public sendBookMarksForOffset(bookMarks: Mark[], editorContent: EditorContent) {
    this.webview.postMessage({
      type: messageType.BOOK_EDITOR.GET_BOOK_MARK_FOR_OFFSET,
      data: {
        bookMarks,
        editorContent,
      },
    });
  }

  public sendCodeBookDocumentsUpdated(codeBookDocuments: CodeBookDocument[]) {
    this.webview.postMessage({
      type: messageType.CODE_BOOK.CODE_BOOK_DOCUMENTS_UPDATED,
      data: { codeBookDocuments },
    });
  }

  public sendBookMarksDeletedWithCodeMarks(changes: MarkChange[]) {
    this.webview.postMessage({
      type: messageType.CODE_BOOK.BOOK_MARKS_DELETED_WITH_CODE_MARKS,
      data: {
        changes,
      },
    });
  }

  public sendLoadedCodeBookDocument(editorContent: any, bookMarks: Mark[]) {
    this.webview.postMessage({
      type: messageType.CODE_BOOK.LOADED_CODE_BOOK_DOCUMENT,
      data: {
        editorContent,
        bookMarks,
      },
    });
  }

  public sendCodeBookUploaded(codeBookUrl: string) {
    this.webview.postMessage({
      type: messageType.CODE_BOOK.CODE_BOOK_UPLOADED,
      data: {
        codeBookUrl,
      },
    });
  }

  public sendSavedCodeBooks(codeBooks: CodeBook[]) {
    this.webview.postMessage({
      type: messageType.WORKSPACE_CODE_BOOKS,
      data: {
        codeBooks,
      },
    });
  }

  public sendWorkspaceName(workspaceName: string) {
    this.webview.postMessage({
      type: messageType.WORKSPACE_NAME,
      data: {
        workspaceName,
      },
    });
  }

  public sendLoadedCodeBook(codeBook: CodeBook) {
    this.webview.postMessage({
      type: messageType.LOADED_CODE_BOOK,
      data: {
        codeBook,
      },
    });
  }

  public sendCodeBookCreated(codeBook: CodeBook) {
    this.webview.postMessage({
      type: messageType.CODE_BOOK_CREATED,
      data: {
        codeBook,
      },
    });
  }

  public sendDidChangeCodeBookNameOk(workspaceCodeBooks: CodeBook[]) {
    this.webview.postMessage({
      type: messageType.DID_CHANGE_CODE_BOOK_NAME,
      data: {
        result: 'OK',
        workspaceCodeBooks,
      },
    });
  }

  public sendDidChangeCodeBookNameError(errorMessage: string) {
    this.webview.postMessage({
      type: messageType.DID_CHANGE_CODE_BOOK_NAME,
      data: {
        result: 'ERROR',
        error: { message: errorMessage },
      },
    });
  }

  public sendDidDeleteCodeBook(workspaceCodeBooks: CodeBook[]) {
    this.webview.postMessage({
      type: messageType.DID_DELETE_CODE_BOOK,
      data: {
        workspaceCodeBooks,
      },
    });
  }
}

export class CodelinesPanel {
  private readonly extensionPath: string;

  private readonly viewType = 'codelinesWebview';

  private readonly title = 'Codelines';

  private readonly workspaceName: string;

  private readonly workspaceRootPath: string;

  private disposables: Disposable[] = [];

  private didCloseOutsideHandler: any;

  private panel: WebviewPanel | undefined;

  // public readonly editorContentSavePath: string;

  public readonly bookEditorWatcher: BookEditorWatcher = new BookEditorWatcher();

  //@ts-ignore
  public readonly bookEditorMessager: BookEditorMessager;

  public get isVisible(): boolean {
    return this.panel ? this.panel.visible : false;
  }

  public constructor(extensionPath: string/*, editorContentSavePath: string*/) {
    const folders = workspace.workspaceFolders;
    if (!folders || !workspace.name) {
      // TODO: Handle more user friendly?
      const message = 'Cannot initialize extension without a workspace';
      window.showErrorMessage(message);
      throw new Error(message);
    }

    this.workspaceName = workspace.name;
    this.workspaceRootPath = folders[0].uri.fsPath;
    this.extensionPath = extensionPath;
    // this.editorContentSavePath = editorContentSavePath;
  }

  public getColumn() {
    if (this.panel) {
      return this.panel.viewColumn;
    }
    return undefined;
  }

  private createPanelAndRegisterEventHandlers() {
    this.panel = this.createPanel();
    this.registerPanelEventHandlers();

    this.bookEditorWatcher.watchBookEditor(this.panel.webview);
    //@ts-ignore
    this.bookEditorMessager = new BookEditorMessager(this.panel.webview);
    this.panel.webview.html = this.getWebviewContent();
  }

  private createPanel(): WebviewPanel {
    const panelOptions: (WebviewPanelOptions & WebviewOptions) = {
      retainContextWhenHidden: true,
      enableScripts: true,
      localResourceRoots: [
        Uri.file(path.join(this.extensionPath, 'build')),
      ],
    };

    // const editors = window.visibleTextEditors.map(e => e.viewColumn);
    // const sortedEditors = editors.sort((a: ViewColumn | undefined, b: ViewColumn | undefined) => {
    //   if (!a && !b) {
    //     return 0;
    //   }
    //   if (!a) {
    //     return 1;
    //   }
    //   if (!b) {
    //     return -1;
    //   }
    //   if (a < b) {
    //     return 1;
    //   }
    //   if (a > b) {
    //     return -1;
    //   }
    //   return 0;
    // });
    // const column = sortedEditors.pop();

    const panel = window.createWebviewPanel(
      this.viewType,
      this.title,
      panelColumn || 300,
      panelOptions,
    );
    if (getEditorColumnsCount() === 0) {
      commands.executeCommand('workbench.action.splitEditorLeft');
    }
    panelColumn = panel.viewColumn;
    return panel;
  }


  public onDidCloseWebviewOneHandler(handler: any) {
    this.didCloseOutsideHandler = handler;
  }

  private registerPanelEventHandlers() {
    if (!this.panel) { return; }
    // Listen for when the panel is disposed
    // This happens when the user closes the panel or when the panel is closed programatically

    this.panel.onDidDispose(() => {
      console.log('PANEL IS BEING DISPOSED');
      this.dispose();
    }, null, this.disposables);
    this.panel.onDidDispose(this.didCloseOutsideHandler, null, this.disposables);

    // this.panel.onDidChangeViewState(() => this.bookEditorMessager.sendFocusBookEditor());
  }

  private dispose() {
    if (!this.panel) { return; }

    this.bookEditorWatcher.dispose();
    this.panel.dispose();

    while (this.disposables.length) {
      const x = this.disposables.pop();
      if (x) {
        x.dispose();
      }
    }
    this.panel = undefined;
  }

  public async showPanel() {
    if (this.panel) {
      this.panel.reveal(panelColumn);
    } else {
      this.createPanelAndRegisterEventHandlers();

      // The `panel` variable actually IS assigned inside `createPanelAndRegisterEventHandlers`.
      // Typescript just can't detect it because it happens inside the method's scope.
      // @ts-ignore: strictNullChecks
      this.panel.reveal(panelColumn);
    }
  }

  public hidePanel() {
    if (this.panel) {
      panelColumn = this.panel.viewColumn;
      this.panel.dispose();
    }
  }

  private generateNonce() {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 32; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  private convertScriptPathsToHtml(scriptPaths: string[], nonce: string) {
    let html = '';
    scriptPaths.forEach((scriptPath) => {
      const diskPath = Uri.file(path.join(this.extensionPath, 'build', scriptPath));
      const uri = diskPath.with({ scheme: 'vscode-resource' });
      html += `<script nonce="${nonce}" src="${uri}"></script>`;
    });
    return html;
  }

  private converStylePathsToHtml(stylePaths: string[]) {
    let html = '';
    stylePaths.forEach((stylePath) => {
      const diskPath = Uri.file(path.join(this.extensionPath, 'build', stylePath));
      const uri = diskPath.with({ scheme: 'vscode-resource' });
      html += `<link rel="stylesheet" type="text/css" href="${uri}">`;
    });
    return html;
  }

  private getWebviewContent() {
    // eslint-disable-next-line
    // const manifest = require(path.join(this.extensionPath, 'build', 'reformatted-asset-manifest.json'));

    const nonce = this.generateNonce();

    const scriptPaths: string[] = manifest.js;
    const scriptTags = this.convertScriptPathsToHtml(scriptPaths, nonce);

    const stylePaths: string[] = manifest.css;
    const styleTags = this.converStylePathsToHtml(stylePaths);

    // const codeBookSettings = CodeBookSettings.getInstance();
    // let lastOpenedDoc: CodeBookDocument;
    // if (codeBookSettings.lastOpenedCodeBookDocument) {
    //   lastOpenedDoc = codeBookSettings.lastOpenedCodeBookDocument;
    // } else {
    //   // eslint-disable-next-line prefer-destructuring
    //   lastOpenedDoc = codeBookSettings.codeBookDocuments[0];
    // }

    const savedEditorContent = '{"blocks":[{"data":{},"depth":0,"entityRanges":[],"inlineStyleRanges":[],"key":"66rqv","text":"","type":"unstyled"}],"entityMap":{}}';
    // const docPath = path.join(this.workspaceRootPath, '.codelines/documents', lastOpenedDoc.contentPath);
    // if (fs.existsSync(docPath)) {
    //   savedEditorContent = fs.readFileSync(docPath).toString();
    // }

    return `<!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
        <meta name="theme-color" content="#000000">
        <base href="${Uri.file(path.join(this.extensionPath, 'build')).with({ scheme: 'vscode-resource' })}/">
        <meta http-equiv="Content-Security-Policy" content="
          default-src 'none';
          connect-src 'self' https://codelines.firebaseio.com https://firestore.googleapis.com/ https://www.googleapis.com https://securetoken.googleapis.com https://us-central1-codelines.cloudfunctions.net;
          img-src data: vscode-resource:;
          script-src 'nonce-${nonce}';
          font-src 'self' vscode-resource:;
          style-src 'self' https://fonts.googleapis.com vscode-resource: 'unsafe-inline' http: https: data:;
        ">

        ${styleTags}
      </head>

        <body>
        <div id="root"></div>

        <script nonce=${nonce}>
        window.savedEditorContent = ${savedEditorContent}
        if (!window.vscode) {
          window.vscode = acquireVsCodeApi();
        }
        </script>
        ${scriptTags}
      </body>
      </html>`;
  }
}
