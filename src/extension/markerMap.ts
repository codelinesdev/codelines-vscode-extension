import { CodeBookMarker } from 'marker';
import * as path from 'path';
import CodelinesSettings, { CodeBook } from './CodelinesSettings';

export interface MarkerMap {
  [codeBookId: string]: CodeBookMarker;
}

export async function initializeCodeBookMarker(codelinesPath: string, codeBook: CodeBook, markerMap: MarkerMap) {
  const marksPath = path.join(codelinesPath, codeBook.marksPath);
  const marker = new CodeBookMarker(marksPath);
  await marker.initialize();
  // eslint-disable-next-line no-param-reassign
  markerMap[codeBook.id] = marker;
}

export async function closeCodeBookMarker(codeBook: CodeBook, markerMap: MarkerMap) {
  const marker = markerMap[codeBook.id];
  await marker.close();
  // eslint-disable-next-line no-param-reassign
  delete markerMap[codeBook.id];
}

export async function initializeAllCodeBookMarkers(settings: CodelinesSettings, markerMap: MarkerMap) {
  try {
    const { codeBooks } = settings;
    const initializeAll = codeBooks.map(async c => initializeCodeBookMarker(settings.codelinesFolderPath, c, markerMap));
    return Promise.all(initializeAll);
  } catch (error) {
    console.error('cannot close codebooks', error);
  }
  return undefined;
}

export async function closeAllCodeBookMarkers(settings: CodelinesSettings, markerMap: MarkerMap) {
  try {
    const { codeBooks } = settings;
    const closeAll = codeBooks.map(async c => closeCodeBookMarker(c, markerMap));
    return Promise.all(closeAll);
  } catch (error) {
    console.error('cannot close codebooks', error);
  }
  return undefined;
}
