export declare namespace messageType {
  //// NEW
  const GET_WORKSPACE_NAME: string;
  const WORKSPACE_NAME: string;

  const GET_WORKSPACE_CODE_BOOKS: string;
  const WORKSPACE_CODE_BOOKS: string;

  const LOAD_CODE_BOOK: string;
  const LOADED_CODE_BOOK: string;

  const CREATE_CODE_BOOK: string;
  const CODE_BOOK_CREATED: string;

  const SAVE_EDITOR_CONTENT: string;

  const CREATE_BOOK_MARK: string;

  const DELETE_BOOK_MARK: string;

  const RUN_COMMAND: string;

  const SHOW_CODE_MARK: string;

  const BOOK_MARKS_IN_EDITOR: string;

  const SET_FOCUSED_BOOK_MARK: string;
  const SET_HOVERED_BOOK_MARK: string;

  const CHANGE_CODE_BOOK_NAME: string;
  const DID_CHANGE_CODE_BOOK_NAME: string;

  const DELETE_CODE_BOOK: string;
  const DID_DELETE_CODE_BOOK: string;

  const DID_CLOSE_CODE_BOOK: string;

  const MIXPANEL_TRACK_EVENT: string;
  //// NEW <end>

  const SAVE_TOKENS: string;
  const REQUEST_TOKENS: string;
  const DOWNLOAD_CODEBOOK: string;
  const UPLOAD_CODEBOOK: string;

  const CODE_SELECTION_ACTIVE: string;
  const CODE_SELECTION_INACTIVE: string;

  namespace BOOK_EDITOR {
    const UNLOAD_CODE_BOOK: string;
    const FOCUS_BOOK_EDITOR: string;
    const BOOK_MARKS_DID_CHANGE: string;
    const CONTENT_UPDATED: string;
    const SAVE_CONTENT: string;
    const GET_BOOK_MARK_FOR_OFFSET: string;
  }

  namespace CODE_BOOK {
    const BOOK_MARKS_DELETED_WITH_CODE_MARKS: string;
    const CODE_BOOK_DOCUMENTS_UPDATED: string;
    const LOAD_CODE_BOOK_DOCUMENT: string;
    const LOADED_CODE_BOOK_DOCUMENT: string;
    const CREATE_NEW_CODE_BOOK_DOCUMENT: string;
    const DELETE_CODE_BOOK_DOCUMENT: string;
    const SHARE_CODE_BOOK: string;
    const CODE_BOOK_UPLOADED: string;
  }
}

export declare class Message {
  public type: string;
  public data: any;

  public constructor(type: string, data: any);
}
