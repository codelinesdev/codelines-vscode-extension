import * as workspaceNameTypes from '@/App/components/withWorkspaceName/types';
import * as workspaceCodeBooksTypes from '@/App/components/withWorkspaceCodeBooks/types';
import * as loadCodeBookTypes from '@/App/components/withLoadCodeBook/types';
import * as createCodeBookTypes from '@/App/components/withCreateCodeBook/types';
import * as saveEditorContentTypes from '@/App/components/withSaveEditorContent/types';
import * as createBookMarkTypes from '@/App/components/withCreateBookMark/types';
import * as deleteBookMarkTypes from '@/App/components/withDeleteBookMark/types';
import * as runCommandTypes from '@/App/components/withRunCommand/types';
import * as showCodeMarkTypes from '@/App/components/withShowCodeMark/types';
import * as reportNookMarksInEditorTypes from '@/App/components/withReportBookMarksInEditor/types';
import * as setFocusedBookMarkTypes from '@/App/components/withSetFocusedBookMark/types';
import * as setHoveredBookMarkTypes from '@/App/components/withSetHoveredBookMark/types';
import * as changeCodeBookNameTypes from '@/App/components/withChangeCodeBookName/types';
import * as deleteCodeBookTypes from '@/App/components/withDeleteCodeBook/types';
import * as didCloseCodeBookTypes from '@/App/components/withDidCloseCodeBook/types';
import mixpanelTypes from '@/App/components/Mixpanel/types';

export const messageType = {
  ...workspaceNameTypes,
  ...workspaceCodeBooksTypes,
  ...loadCodeBookTypes,
  ...createCodeBookTypes,
  ...saveEditorContentTypes,
  ...createBookMarkTypes,
  ...deleteBookMarkTypes,
  ...runCommandTypes,
  ...showCodeMarkTypes,
  ...reportNookMarksInEditorTypes,
  ...setFocusedBookMarkTypes,
  ...setHoveredBookMarkTypes,
  ...changeCodeBookNameTypes,
  ...deleteCodeBookTypes,
  ...didCloseCodeBookTypes,
  ...mixpanelTypes,

  // TODO
  SAVE_TOKENS: 'SAVE_TOKENS',
  REQUEST_TOKENS: 'REQUEST_TOKENS',
  DOWNLOAD_CODEBOOK: 'DOWNLOAD_CODEBOOK',
  UPLOAD_CODEBOOK: 'UPLOAD_CODEBOOK',

  CODE_SELECTION_ACTIVE: 'CODE_SELECTION_ACTIVE',
  CODE_SELECTION_INACTIVE: 'CODE_SELECTION_INACTIVE',

  CODE_EDITOR: {
    IS_CODE_SELECTION_EMPTY: 'IS_CODE_SELECTION_EMPTY',
  },

  BOOK_EDITOR: {
    UNLOAD_CODE_BOOK: 'UNLOAD_CODE_BOOK',
    FOCUS_BOOK_EDITOR: 'FOCUS_BOOK_EDITOR', // Sent by extension to the webview

    BOOK_MARKS_DID_CHANGE: 'BOOK_MARKS_DID_CHANGE', // Sent by extension to the webview

    GET_BOOK_MARK_FOR_OFFSET: 'GET_BOOK_MARK_FOR_OFFSET', // Sent by the webview to extension
    BOOK_MARK_FOR_OFFSET: 'BOOK_MARK_FOR_OFFSET', // Sent by the extension to the webview as a response to 'GET_BOOK_MARK_FOR_OFFSET'
  },

  CODE_BOOK: {
    BOOK_MARKS_DELETED_WITH_CODE_MARKS: 'BOOK_MARKS_DELETED_WITH_CODE_MARKS',
    CODE_BOOK_DOCUMENTS_UPDATED: 'CODE_BOOK_DOCUMENTS_UPDATED', // Sent by extension
    LOAD_CODE_BOOK_DOCUMENT: 'LOAD_CODE_BOOK_DOCUMENT',
    LOADED_CODE_BOOK_DOCUMENT: 'LOADED_CODE_BOOK_DOCUMENT', // Sent by extension
    CREATE_NEW_CODE_BOOK_DOCUMENT: 'CREATE_NEW_CODE_BOOK_DOCUMENT',
    DELETE_CODE_BOOK_DOCUMENT: 'DELETE_CODE_BOOK_DOCUMENT',
    SHARE_CODE_BOOK: 'SHARE_CODE_BOOK',
    CODE_BOOK_UPLOADED: 'CODE_BOOK_UPLOADED',
  },
};

export class Message {
  constructor(type, data) {
    this.type = type;
    this.data = data;
  }
}
