import { v4 as ip } from 'public-ip';

let cachedIp: string | undefined;

export async function getIp() {
  if (cachedIp) {
    return cachedIp;
  }
  try {
    const newIp = await ip();
    cachedIp = newIp;
    return newIp;
  } catch (error) {
    console.error('cannot retrieve ip', error);
  }
  return undefined;
}
