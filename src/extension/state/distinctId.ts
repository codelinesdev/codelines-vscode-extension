// eslint-disable-next-line import/no-unresolved
import { ExtensionContext } from 'vscode';
import { randomBytes } from 'crypto';

let cachedDistinctId: string | undefined;

export async function getDistinctId(context: ExtensionContext) {
  if (cachedDistinctId) {
    return cachedDistinctId;
  }
  try {
    const globalDistinctId = await context.globalState.get('distinctId');
    if (globalDistinctId) {
      cachedDistinctId = globalDistinctId as string;
    } else {
      await context.globalState.update('distinctId', cachedDistinctId);
      cachedDistinctId = randomBytes(16).toString('hex');
    }
    return cachedDistinctId;
  } catch (error) {
    console.error('Distinct Id cannot be retrieved', error);
    throw error;
  }
}
