// eslint-disable-next-line import/no-unresolved
import { ExtensionContext } from 'vscode';

let cachedTokens: {idToken: string; refreshToken: string} | undefined;

export async function getTokens(context: ExtensionContext) {
  if (cachedTokens) {
    return cachedTokens;
  }
  try {
    const globalTokens = await Promise.all([
      context.globalState.get('idToken'),
      context.globalState.get('refreshToken')]);
    cachedTokens = { idToken: globalTokens[0] as unknown as string, refreshToken: globalTokens[1] as unknown as string };
    return cachedTokens;
  } catch {
    console.error('Tokens cannot be retrieved');
  }
  return { idToken: undefined, refreshToken: undefined };
}

export async function setTokens(idToken: string, refreshToken: string, context: ExtensionContext) {
  cachedTokens = { idToken, refreshToken };
  try {
    return Promise.all([
      context.globalState.update('idToken', idToken),
      context.globalState.update('refreshToken', refreshToken),
    ]);
  } catch (error) {
    console.error('Tokens cannot be saved');
  }
  return undefined;
}
