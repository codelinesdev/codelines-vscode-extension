/* eslint-disable @typescript-eslint/camelcase */
import { Block } from 'marker';
// eslint-disable-next-line import/no-unresolved
import { ExtensionContext } from 'vscode';
import * as os from 'os';
import * as Mixpanel from 'mixpanel';
import {
  getMarkLength,
  getMarkLineCount,
  getPathLanguage,
  getPathVersion,
  getPathLineCount,
  getEditorsCount,
  getEditorColumnsCount,
} from '../CodeEditor/utils';
import { getIp } from '../state/ip';
import { getDistinctId } from '../state/distinctId';

const mixpanel = Mixpanel.init('a03cd2bcbeface4188b7cc6c8a5b4b8b', {
  protocol: 'https',
});

const sessionStartTime = new Date();
let codeBookSessionStartTime: Date | undefined;
let webviewSessionStartTime: Date | undefined;
let currentUserId: string | undefined;
const platform = os.platform();

function getTimeSinceStart(startTime: Date) {
  return new Date(Date.now() - startTime.getMilliseconds()).getSeconds();
}

async function getCurrentId(context: ExtensionContext) {
  if (currentUserId) {
    return currentUserId;
  }
  return getDistinctId(context);
}

function getCodemarkInfo(mark: { blocks: Block[]; filePath: string }) {
  return {
    codeMarkLength: getMarkLength(mark),
    codeMarkLineCount: getMarkLineCount(mark),
    language: getPathLanguage(mark.filePath),
    fileVersion: getPathVersion(mark.filePath),
    fileLineCount: getPathLineCount(mark.filePath),
    openEditorsCount: getEditorsCount(),
    codeColumnsCount: getEditorColumnsCount(),
  };
}

export async function trackSignIn(userId: string, context: ExtensionContext) {
  const ip = await getIp();
  currentUserId = userId;
  const distinctId = await getCurrentId(context);
  mixpanel.track('Sign In', {
    distinct_id: distinctId,
    time: new Date(),
    ip,
    platform,
  });
}

export async function trackSignUp(userId: string, email: string, username: string, context: ExtensionContext) {
  const ip = await getIp();
  currentUserId = userId;
  const distinctId = await getCurrentId(context);
  mixpanel.people.set(
    distinctId, {
      signedUp: true,
      $email: email,
      $name: username,
      signUpTime: new Date(),
      signupTimeSinceSessionStart: getTimeSinceStart(sessionStartTime),
      ip,
    },
  );
  mixpanel.track('Sign up', {
    distinct_id: distinctId,
    time: new Date(),
    platform,
    ip,
  });
}

export async function trackSignOut(context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  currentUserId = undefined;
  mixpanel.track('Sign Out', {
    distinct_id: distinctId,
    time: new Date(),
    ip,
    platform,
  });
}

export async function trackCreateCommnad(commandString: string, bookEditorTextLength: number, context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'commandsCreated');
  mixpanel.track('Create Command', {
    distinct_id: distinctId,
    time: new Date(),
    commandString,
    bookEditorTextLength,
    ip,
    platform,
  });
}

export async function trackRunCommand(commandString: string, context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'commandsRun');
  mixpanel.track('Run Command', {
    distinct_id: distinctId,
    time: new Date(),
    commandString,
    ip,
    platform,
  });
}

export async function trackDeleteMark(context: ExtensionContext) {
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'marksDeleted');
}

export async function trackCreateMark(bookMarkInfo: any, codeMark: { blocks: Block[]; filePath: string }, context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'marksCreated');
  mixpanel.track('Create Mark', {
    distinct_id: distinctId,
    time: new Date(),
    ...getCodemarkInfo(codeMark),
    ...bookMarkInfo,
    ip,
    platform,
  });
}

export async function trackCloseWebview(context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.track('Close Webview', {
    distinct_id: distinctId,
    time: new Date(),
    webviewSessionLength: webviewSessionStartTime ? getTimeSinceStart(webviewSessionStartTime) : undefined,
    ip,
    platform,
  });
}

export async function trackOpenWebview(context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  webviewSessionStartTime = new Date();
  mixpanel.track('Open Webview', {
    distinct_id: distinctId,
    time: new Date(),
    ip,
    platform,
  });
}

export async function trackCreateCodeBook(codeBooksCount: number, context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'codeBooksCreated');
  mixpanel.track('Create CodeBook', {
    distinct_id: distinctId,
    time: new Date(),
    codeBooksCount,
    ip,
    platform,
  });
}

export async function trackDeleteCodeBook(codeBooksCount: number, context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'codeBooksDeleted');
  mixpanel.track('Delete CodeBook', {
    distinct_id: distinctId,
    time: new Date(),
    codeBooksCount,
    ip,
    platform,
  });
}

export async function trackLoadCodeBook(context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  codeBookSessionStartTime = new Date();
  mixpanel.track('Load CodeBook', {
    distinct_id: distinctId,
    time: new Date(),
    ip,
    platform,
  });
}

export async function trackCloseCodeBook(context: ExtensionContext) {
  const ip = await getIp();
  const distinctId = await getCurrentId(context);
  mixpanel.track('Close CodeBook', {
    distinct_id: distinctId,
    time: new Date(),
    codeBookSessionLength: codeBookSessionStartTime ? getTimeSinceStart(codeBookSessionStartTime) : undefined,
    ip,
    platform,
  });
}

export async function trackRevealCodeMark(context: ExtensionContext) {
  const distinctId = await getCurrentId(context);
  mixpanel.people.increment(distinctId, 'marksRevealed');
}
