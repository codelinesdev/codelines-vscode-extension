import {
  commands,
  TreeDataProvider,
  TreeItem,
  EventEmitter,
  Event,
  ProviderResult,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { showWebview, hideWebview } from './codelinesCommands';
import { CodelinesPanel } from './CodelinesPanel';


// This is a hack.
// We want to open the CodelinesPanel's webview when user
// click on a sidebar icon.
// The sidebar viewlet can't be controlled directly but
// only through registering a tree data provider.
// Tree data provider basically tells vscode what data
// should be rendered in the sidebar view.

// This is a dummy class implementing TreeDataProvider but
// not actually populating the sidebar in any way.

// SidebarDataProvider's refresh() method gets periodically
// called once the extension is activated.
// Every time refresh() is called and *!!!* the sidebar view
// is already visible *!!!* the onDidChangeTreeData even is
// fired and the sidebar view call getChildren()

export class SidebarDataProvider implements TreeDataProvider<TreeItem> {
  public onDidChangeTreeData: Event<TreeItem>;
  private treeChangeEventEmitter: EventEmitter<TreeItem>;

  private isHandlingWebview = false;
  private readonly panel: CodelinesPanel;

  public constructor(panel: CodelinesPanel) {
    this.panel = panel;
    this.treeChangeEventEmitter = new EventEmitter<TreeItem>();
    this.onDidChangeTreeData = this.treeChangeEventEmitter.event;
  }

  private toggleSidebarAfterDelay() {
    // If we clouse the sidebar too quickly the webview won't load properly
    setTimeout(() => {
      this.isHandlingWebview = false;
      commands.executeCommand('workbench.action.toggleSidebarVisibility');
    }, 100);
  }

  public getTreeItem(): TreeItem | Thenable<TreeItem> {
    return new TreeItem('');
  }

  public getChildren(): ProviderResult<TreeItem[]> {
    if (!this.isHandlingWebview) {
      this.isHandlingWebview = true;

      // if (CodelinesPanel.currentPanel && CodelinesPanel.currentPanel.isVisible) {
      if (this.panel.isVisible) {
        return hideWebview().then(() => {
          this.toggleSidebarAfterDelay();
          return [new TreeItem('')];
        });
      }
      return showWebview().then(() => {
        this.toggleSidebarAfterDelay();
        return [new TreeItem('')];
      });
    }
    return [];
  }

  public refresh() {
    this.treeChangeEventEmitter.fire();
  }
}
