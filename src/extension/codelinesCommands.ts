import {
  commands,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';

export const commandNames = {
  SHOW_WEBVIEW: 'codelines.showWebview',
  HIDE_WEBVIEW: 'codelines.hideWebview',
};

export function showWebview() {
  return commands.executeCommand(commandNames.SHOW_WEBVIEW);
}

export function hideWebview() {
  return commands.executeCommand(commandNames.HIDE_WEBVIEW);
}
