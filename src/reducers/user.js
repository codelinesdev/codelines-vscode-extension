import { types } from '@/actions';

const initialState = {
  currentUser: null,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case types.user.SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.currentUser,
      };
    default:
      return state;
  }
}
