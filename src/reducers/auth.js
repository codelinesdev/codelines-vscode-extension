import { types } from '@/actions';

const initialState = {
  didFinishReauth: false,
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case types.auth.SET_DID_FINISH_REAUTH:
      return {
        ...state,
        didFinishReauth: action.didFinishReauth,
      };
    default:
      return state;
  }
}
