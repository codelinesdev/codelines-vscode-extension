import { types } from '@/actions';

const initialState = {
  editorRef: undefined,
  editorState: undefined,
};

export default function editor(state = initialState, action) {
  switch (action.type) {
    case types.editor.SET_EDITOR_REF:
      return {
        ...state,
        editorRef: action.editorRef,
      };
    case types.editor.SET_EDITOR_STATE:
      return {
        ...state,
        editorState: action.editorState,
      };
    default:
      return state;
  }
}
