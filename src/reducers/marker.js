import { SET_IS_MARKER_READY } from '@/actions/types';

export default function marker(
  state = {
    isMarkerReady: false,
  },
  action,
) {
  switch (action.type) {
    case SET_IS_MARKER_READY: {
      return {
        ...state,
        isMarkerReady: action.isMarkerReady,
      };
    }
    default:
      return state;
  }
}
