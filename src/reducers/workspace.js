import { types } from '@/actions';

const initialState = {
  name: '',
  codeBooks: [],
};

export default function workspace(state = initialState, action) {
  switch (action.type) {
    case types.workspace.SET_WORKSPACE_NAME:
      return {
        ...state,
        name: action.name,
      };
    case types.workspace.SET_WORKSPACE_CODE_BOOKS:
      return {
        ...state,
        codeBooks: action.codeBooks,
      };
    default:
      return state;
  }
}
