import { types } from '@/actions';

const initialState = {
  activeCodeBook: undefined,
  activeDocument: '',
  codeBookDocuments: [],
};

export default function codeBook(state = initialState, action) {
  switch (action.type) {
    case types.codeBook.SET_ACTIVE_CODE_BOOK:
      return {
        ...state,
        activeCodeBook: action.activeCodeBook,
      };
    case types.codeBook.SET_ACTIVE_CODE_BOOK_DOCUMENT:
      return {
        ...state,
        activeDocument: action.activeDocument,
      };
    case types.codeBook.SET_CODE_BOOK_DOCUMENTS:
      return {
        ...state,
        codeBookDocuments: action.codeBookDocuments,
      };
    default:
      return state;
  }
}
