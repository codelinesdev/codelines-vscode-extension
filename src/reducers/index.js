import { combineReducers } from 'redux';
import bookMarks from './bookMarks';
import marker from './marker';
import editor from './editor';
import codeBook from './codeBook';
import user from './user';
import router from './router';
import workspace from './workspace';
import auth from './auth';

export default combineReducers({
  bookMarks,
  marker,
  editor,
  codeBook,
  user,
  router,
  workspace,
  auth,
});
