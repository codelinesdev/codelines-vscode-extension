import { types } from '@/actions';

const initialState = {
  hoveredBookMarkId: '',
  focusedBookMarkId: '',
};

export default function bookMarks(state = initialState, action) {
  switch (action.type) {
    case types.bookMarks.SET_HOVERED_BOOK_MARK_ID:
      return {
        ...state,
        hoveredBookMarkId: action.hoveredBookMarkId,
      };
    case types.bookMarks.SET_FOCUSED_BOOK_MARK_ID:
      return {
        ...state,
        focusedBookMarkId: action.focusedBookMarkId,
      };
    default:
      return state;
  }
}
