import { types } from '@/actions';

const initialState = {
  route: 'home',
};

export default function router(state = initialState, action) {
  switch (action.type) {
    case types.router.SET_ROUTE:
      return {
        ...state,
        route: action.route,
      };
    default:
      return state;
  }
}
