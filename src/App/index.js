import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import actions from '@/actions';
import compose from '@/utils/compose';

import withLoadCodeBook from './components/withLoadCodeBook';

import Header from './components/Header';
import HomeView from './views/HomeView';
import BookEditorView from './views/BookEditorView';

// TODO: Change theming based on the currently selected
// theme by user (light, dark, high-contrast)
// see https://code.visualstudio.com/api/extension-guides/webview

const AppDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

function App(props) {
  let content = null;

  switch (props.route) {
    case 'editor':
      content = <BookEditorView />;
      break;
    case 'home':
      content = (
        <HomeView
          loadCodeBook={props.loadCodeBook}
        />
      );
      break;
    default:
      break;
  }

  return (
    <AppDiv>
      <Header />
      {content}
    </AppDiv>
  );
}

function mapStateToProps(state) {
  return {
    codeBook: state.codeBook,
    route: state.router.route,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoute: route => dispatch(actions.setRoute(route)),
  };
}

export default compose(
  withLoadCodeBook,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(App);
