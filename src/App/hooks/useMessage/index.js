import { useEffect } from 'react';

export default function useMessage(messageType, handler) {
  useEffect(() => {
    function handleMessage(event) {
      const message = event.data;
      const { type } = message;
      switch (type) {
        case messageType:
          console.log(`[Extension Message ${type}]`, message);
          handler(message);
          break;
        default:
          break;
      }
    }
    window.addEventListener('message', handleMessage);
    return () => {
      window.removeEventListener('message', handleMessage);
    };
  }, [handler, messageType]);
}
