import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  DID_CLOSE_CODE_BOOK,
} from './types';

const withDidCloseCodeBook = WrappedComponent => (props) => {
  function didCloseCodeBook(codeBookId) {
    const m = new Message(DID_CLOSE_CODE_BOOK, {
      id: codeBookId,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      didCloseCodeBook={didCloseCodeBook}
      {...props}
    />
  );
};

export default withDidCloseCodeBook;
