import React, { useRef, useEffect } from 'react';

const withOutsideClick = WrappedComponent => (props) => {
  const wrappedRef = useRef(null);

  function handleMouseDown(event) {
    if (!wrappedRef.current.contains(event.target)) {
      event.stopPropagation();
      event.preventDefault();
      props.onOutsideClick();
    }
  }

  useEffect(() => {
    window.addEventListener('mousedown', handleMouseDown);
    return () => {
      window.removeEventListener('mousedown', handleMouseDown);
    };
  });

  return (
    <WrappedComponent
      innerRef={wrappedRef}
      {...props}
    />
  );
};

export default withOutsideClick;
