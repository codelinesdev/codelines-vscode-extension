import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  RUN_COMMAND,
} from './types';

const withRunCommand = WrappedComponent => (props) => {
  function runCommand(commandString) {
    const m = new Message(RUN_COMMAND, {
      commandString,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      runCommand={runCommand}
      {...props}
    />
  );
};

export default withRunCommand;
