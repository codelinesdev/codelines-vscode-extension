import React from 'react';
import { connect } from 'react-redux';
import actions from '@/actions';
import useMessage from '@/App/hooks/useMessage';
import { Message } from '@/extension/shared/messageManager';

import {
  GET_WORKSPACE_NAME,
  WORKSPACE_NAME,
} from './types';

const withWorkspaceName = (WrappedComponent) => {
  function getWorkspaceName() {
    const m = new Message(GET_WORKSPACE_NAME, {});
    window.vscode.postMessage(m);
  }

  function WorkspaceName(props) {
    function handler(message) {
      const { workspaceName } = message.data;
      props.setWorkspaceName(workspaceName);
    }
    useMessage(WORKSPACE_NAME, handler);

    return (
      <WrappedComponent
        getWorkspaceName={getWorkspaceName}
        {...props}
      />
    );
  }

  function mapDispatchToProps(dispatch) {
    return {
      setWorkspaceName: name => dispatch(actions.setWorkspaceName(name)),
    };
  }

  return connect(
    null,
    mapDispatchToProps,
  )(WorkspaceName);
};

export default withWorkspaceName;
