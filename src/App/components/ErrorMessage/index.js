import React from 'react';
import styled from 'styled-components';

const StyledP = styled.p`
  text-align: center;
  color: var(--error);
`;

export default function ErrorMessage(props) {
  return (
    <StyledP {...props} />
  );
}
