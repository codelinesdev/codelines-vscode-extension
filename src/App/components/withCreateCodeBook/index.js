import React from 'react';
import { connect } from 'react-redux';
import actions from '@/actions';
import useMessage from '@/App/hooks/useMessage';
import { Message } from '@/extension/shared/messageManager';

import {
  CREATE_CODE_BOOK,
  CODE_BOOK_CREATED,
} from './types';

const withCreateCodeBook = (WrappedComponent) => {
  function createCodeBook() {
    const m = new Message(CREATE_CODE_BOOK, {});
    window.vscode.postMessage(m);
  }

  function CreateCodeBook(props) {
    function handler(message) {
      const { codeBook } = message.data;
      const newCodeBooks = [...props.workspaceCodeBooks, codeBook];
      props.setWorkspaceCodeBooks(newCodeBooks);
    }
    useMessage(CODE_BOOK_CREATED, handler);

    return (
      <WrappedComponent
        createCodeBook={createCodeBook}
        {...props}
      />
    );
  }

  function mapStateToProps(state) {
    return {
      workspaceCodeBooks: state.workspace.codeBooks,
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      setWorkspaceCodeBooks: codeBooks => dispatch(actions.setWorkspaceCodeBooks(codeBooks)),
    };
  }

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CreateCodeBook);
};

export default withCreateCodeBook;
