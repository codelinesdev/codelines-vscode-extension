import React from 'react';
import { Message } from '@/extension/shared/messageManager';
import {
  promisifyEvent,
} from '@/utils/promisifyEvent';

import {
  CHANGE_CODE_BOOK_NAME,
  DID_CHANGE_CODE_BOOK_NAME,
} from './types';

const withChangeCodeBookName = WrappedComponent => (props) => {
  function sendChangeCodeBookName(id, name, description) {
    const m = new Message(CHANGE_CODE_BOOK_NAME, {
      id,
      name,
      description,
    });
    window.vscode.postMessage(m);
  }

  async function changeCodeBookName(codeBookId, name, description) {
    const event = await promisifyEvent(
      l => window.addEventListener('message', l),
      l => window.removeEventListener('message', l),
      () => sendChangeCodeBookName(codeBookId, name, description),
      e => e.data.type === DID_CHANGE_CODE_BOOK_NAME,
    );
    return event.data.data;
  }

  return (
    <WrappedComponent
      changeCodeBookName={changeCodeBookName}
      {...props}
    />
  );
};

export default withChangeCodeBookName;
