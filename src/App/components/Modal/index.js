import React from 'react';
import styled from 'styled-components';

import compose from '@/utils/compose';
import withOutsideClick from '@/App/components/withOutsideClick';

const ModalDiv = styled.div`
  padding: 5px 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;

  border-radius: 5px;

  ${(props) => {
    if (props.isLight) {
      return `
        /* background: white; */
        background: var(--background-secondary);
        border: none;
      `;
    }

    if (props.isDark) {
      return `
        /* border: 1px solid #1B1D26; */
        border: 1px solid var(--separator);
        /* background: #3C3F50; */
        background: var(--background-secondary);
      `;
    }

    return '';
  }}
`;

function Modal(props) {
  const { innerRef, ...rest } = props;
  return <ModalDiv ref={innerRef} {...rest} />;
}

export default compose(
  withOutsideClick,
)(Modal);
