import React from 'react';
import styled from 'styled-components';


const StyledTextArea = styled.textarea`
  padding: 10px 12px;

  /* color: white; */
  color: var(--text-light);
  font-size: 18px;

  /* background: #1F212A; */
  background: var(--background-primary);

  border: 1px solid var(--separator);
  border-radius: 5px;

  ::placeholder {
    /* color: #868A95; */
    color: var(--text-placeholder);
  }
`;

export default function TextArea(props) {
  return <StyledTextArea {...props} />;
}
