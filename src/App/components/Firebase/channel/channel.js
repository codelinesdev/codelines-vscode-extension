import {
  Message,
} from '@/extension/shared/messageManager';
import {
  promisifyEvent,
} from '@/utils/promisifyEvent';

export class Channel {
  // redistribute event types to more emitters in some central class
  // using channel with permanent event listening (for one kind of type)?
  addListener = l => window.addEventListener('message', l);
  removeListener = l => window.removeEventListener('message', l);
  constructor(type) {
    this.createMessage = data => new Message(type, data);
    this.dispatch = data => window.vscode.postMessage(data);
    this.predicate = event => event.data.type === type;
  }

  async send(data) {
    const result = await promisifyEvent(
      this.addListener,
      this.removeListener,
      this.dispatch(this.createMessage(data)),
      this.predicate,
    );
    return result.data.data;
  }
}
