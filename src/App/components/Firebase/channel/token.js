import {
  messageType,
  Message,
} from '@/extension/shared/messageManager';
import {
  promisifyEvent,
} from '@/utils/promisifyEvent';

export function sendSaveTokens(idToken, refreshToken, userId) {
  const t = messageType.SAVE_TOKENS;
  const m = new Message(t, {
    idToken,
    refreshToken,
    userId,
  });
  window.vscode.postMessage(m);
}

function sendRequestTokens() {
  const t = messageType.REQUEST_TOKENS;
  const m = new Message(t, {});
  window.vscode.postMessage(m);
}

export async function requestTokensFromExtension() {
  const event = await promisifyEvent(
    l => window.addEventListener('message', l),
    l => window.removeEventListener('message', l),
    sendRequestTokens,
    e => e.data.type === messageType.REQUEST_TOKENS,
  );
  return event.data.data;
}
