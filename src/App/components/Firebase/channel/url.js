import {
  messageType,
  Message,
} from '@/extension/shared/messageManager';
import {
  promisifyEvent,
} from '@/utils/promisifyEvent';


function sendDownloadFileFromUrl(url, codebookId) {
  const t = messageType.DOWNLOAD_CODEBOOK;
  const m = new Message(t, {
    url,
    codebookId,
  });
  window.vscode.postMessage(m);
}

export async function downloadFileFromUrl(url, codebookId) {
  const event = await promisifyEvent(
    l => window.addEventListener('message', l),
    l => window.removeEventListener('message', l),
    () => sendDownloadFileFromUrl(url, codebookId),
    e => e.data.type === messageType.DOWNLOAD_CODEBOOK,
  );
  return event.data.data;
}


function sendUploadFileToUrl(url, codebookId) {
  const t = messageType.UPLOAD_CODEBOOK;
  const m = new Message(t, {
    url,
    codebookId,
  });
  window.vscode.postMessage(m);
}


export async function uploadFileToUrl(url, codebookId) {
  const event = await promisifyEvent(
    l => window.addEventListener('message', l),
    l => window.removeEventListener('message', l),
    () => sendUploadFileToUrl(url, codebookId),
    e => e.data.type === messageType.UPLOAD_CODEBOOK,
  );
  return event.data.data;
}
