//import * as firebase from 'firebase/app';
import 'firebase/firestore';

const USERS_COLLECTION = 'users';
const CODEBOOKS_COLLECTION = 'codebooks';


export class DatabaseLayer {
  constructor(app) {
    this.firestore = app.firestore(app);
    this.users = this.firestore.collection(USERS_COLLECTION);
    this.codebooks = this.firestore.collection(CODEBOOKS_COLLECTION);
  }

  async getUser(userId) {
    try {
      const userRef = await this.users.doc(userId).get();
      if (userRef) {
        return userRef.data();
      }
    } catch (error) {
      console.error('cannot get user');
    }
    return undefined;
  }

  async getCodebook(codebookId) {
    try {
      const userRef = await this.codebooks.doc(codebookId).get();
      if (userRef) {
        return userRef.data();
      }
    } catch (error) {
      console.error('cannot get codebook');
    }
    return undefined;
  }

  async createCodebook(codebookId, userId) {
    try {
      const codebookDoc = this.codebooks.doc(codebookId);
      await codebookDoc.set({ userId });
    } catch (error) {
      console.error('cannot create codebook');
    }
  }

  async isUsernameUnique(username) {
    try {
      const query = await this.users.where('username', '==', username).get();
      return query.size === 0;
    } catch (error) {
      console.error('non unique name', error);
    }
    return false;
  }

  async createUser(username, userId) {
    try {
      await this.users.doc(userId).set({ username });
      return true;
    } catch (error) {
      console.error('error creating username', error);
    }
    return false;
  }

  async listUserCodebooks(userId) {
    try {
      const query = await this.codebooks.where('userId', '==', userId).get();
      return query.docs;
    } catch (error) {
      console.error('cannot update user');
    }
    return undefined;
  }
}
