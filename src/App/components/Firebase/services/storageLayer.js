//import * as firebase from 'firebase/app';
import 'firebase/storage';
import * as path from 'path';

export class StorageLayer {
  constructor(app) {
    this.storage = app.storage();
  }

  async generateCodebookDownloadUrl(userId, codebookId) {
    const storagePath = path.join(userId, codebookId);
    return this.storage.ref(storagePath).getDownloadUrl();
  }
}
