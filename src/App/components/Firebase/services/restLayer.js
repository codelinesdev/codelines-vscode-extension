import axios from 'axios';
import { config } from '../config';

export async function requestCustomToken(idToken) {
  const url = 'https://us-central1-codelines.cloudfunctions.net/getCustomToken';
  try {
    const response = await axios({
      method: 'POST',
      url,
      headers: {
      },
      data: {
        idToken,
      },
    });
    const { customToken } = response.data;
    return customToken;
  } catch (error) {
    console.error('custom token request error', error.response);
    return undefined;
  }
}

export async function requestRefreshedIdToken(refreshToken) {
  const url = `https://securetoken.googleapis.com/v1/token?key=${config.apiKey}`;
  const params = new URLSearchParams();
  params.append('grant_type', 'refresh_token');
  params.append('refresh_token', refreshToken);
  const response = await axios({
    method: 'POST',
    url,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: params,
  });
  return response.data.id_token;
}

export async function requestDownloadUrl(codebookId) {
  const url = 'https://us-central1-codelines.cloudfunctions.net/getDownloadUrl';
  try {
    const response = await axios({
      method: 'POST',
      url,
      headers: {
      },
      data: {
        codebookId,
      },
    });
    const { downloadUrl } = response.data;
    return downloadUrl;
  } catch (error) {
    console.error('cannot get download url', error.response);
    return undefined;
  }
}

export async function requestUploadUrl(codebookId, idToken) {
  const url = 'https://us-central1-codelines.cloudfunctions.net/getUploadUrl';
  try {
    const response = await axios({
      method: 'POST',
      url,
      headers: {
      },
      data: {
        codebookId,
        idToken,
      },
    });
    const { uploadUrl } = response.data;
    return uploadUrl;
  } catch (error) {
    console.error('cannot get upload url', error.response);
    return undefined;
  }
}
