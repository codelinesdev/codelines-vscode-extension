//import * as firebase from 'firebase/app';
import 'firebase/auth';

import { sendSaveTokens } from '../channel/token';
import { requestCustomToken, requestRefreshedIdToken } from './restLayer';

export class AuthLayer {
  constructor(app) {
    this.auth = app.auth(app);
  }

  onAuthStateChanged(handler) {
    this.auth.onAuthStateChanged(handler);
  }

  async getIdToken() {
    return this.auth.currentUser.getIdToken(true);
  }

  getSignedInUid() {
    const user = this.auth.currentUser;
    if (user) {
      return user.uid;
    }
    return undefined;
  }

  isAuthenticated() {
    return this.auth.currentUser !== null;
  }

  async resetPassword() {
    const { email } = this.auth.currentUser;
    return this.auth.sendPasswordResetEmail(email);
  }

  async reauthenticate(refreshToken) {
    try {
      console.log('reauthenticating...');
      const refreshedIdToken = await requestRefreshedIdToken(refreshToken);
      console.log('refreshedIdToken retrieved');
      const customToken = await requestCustomToken(refreshedIdToken);
      console.log('customToken retrieved');
      await this.auth.signInWithCustomToken(customToken);
    } catch (error) {
      console.log('cannot reauthenticate', error.response);
    }
  }

  async saveTokens() {
    if (this.isAuthenticated()) {
      const idToken = await this.auth.currentUser.getIdToken(true);
      const { refreshToken } = this.auth.currentUser;
      sendSaveTokens(idToken, refreshToken, this.auth.currentUser.uid);
    }
  }

  deleteTokens() {
    sendSaveTokens(undefined, undefined, undefined);
  }

  async createUser(email, password) {
    await this.auth.createUserWithEmailAndPassword(email, password);
    await this.saveTokens();
    return this.auth.currentUser.uid;
  }

  async signInUser(email, password) {
    await this.auth.signInWithEmailAndPassword(email, password);
    await this.saveTokens();
    return this.auth.currentUser.uid;
  }

  async signOutUser() {
    try {
      await this.auth.signOut();
      this.deleteTokens();
      console.log('User signed out');
    } catch (error) {
      throw error;
    }
  }
}
