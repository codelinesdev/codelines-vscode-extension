import React from 'react';

export const FirebaseContext = React.createContext(null);


export const withFirebase = WrappedComponent => props => (
  <FirebaseContext.Consumer>
    {firebase => <WrappedComponent {...props} firebase={firebase} />}
  </FirebaseContext.Consumer>
);
