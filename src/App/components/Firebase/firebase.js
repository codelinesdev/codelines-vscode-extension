import * as firebase from 'firebase/app';
import { config } from './config';

import { AuthLayer } from './services/authLayer';
import { DatabaseLayer } from './services/databaseLayer';
import { StorageLayer } from './services/storageLayer';

import { requestDownloadUrl, requestUploadUrl } from './services/restLayer';
import { requestTokensFromExtension } from './channel/token';
import { downloadFileFromUrl, uploadFileToUrl } from './channel/url';

import actions from '@/actions';
import store from '@/store';
import {
  trackSignIn,
  trackSignUp,
  trackSignOut,
} from '@/App/components/Mixpanel';


export default class Firebase {
  app = firebase.initializeApp(config);
  auth = new AuthLayer(this.app);
  databaseLayer = new DatabaseLayer(this.app);
  storageLayer = new StorageLayer(this.app);

  constructor() {
    this.reauthenticateFromSavedTokens();
  }

  // async test() {
  //   const uid = await this.signIn('iscaac1@newton.uk', 'qwertyu');
  //   console.log('uid', uid);
  //   const { username } = await this.getUser();
  //   console.log('username', username);
  //   const codebooks = await this.listSignedUserCodebooks();
  //   console.log('number of codebooks', codebooks.shift().data());
  //   const c = await this.getCodebook('codebook1');
  //   console.log('codebook', c);
  //   this.uploadCodebook('codebook2');
  // }

  onAuthStateChanged(callback) {
    this.auth.onAuthStateChanged(async (authUser) => {
      let mergedUser;
      if (authUser) {
        const dbUser = await this.databaseLayer.getUser(authUser.uid);
        mergedUser = {
          uid: authUser.uid,
          email: authUser.email,
          emailVerified: authUser.emailVerified,
          providerData: authUser.providerData,
          ...dbUser,
        };
      }
      callback(mergedUser);
    });
  }

  async reauthenticateFromSavedTokens() {
    store.dispatch(actions.setDidFinishReauth(false));
    if (this.auth.isAuthenticated()) {
      store.dispatch(actions.setDidFinishReauth(true));
      console.log('Already authenticated');
      return;
    }
    try {
      const tokens = await requestTokensFromExtension();
      if (!tokens) {
        store.dispatch(actions.setDidFinishReauth(true));
        return;
      }
      if (!tokens.idToken || !tokens.refreshToken) {
        store.dispatch(actions.setDidFinishReauth(true));
        return;
      }
      await this.auth.reauthenticate(tokens.refreshToken);
      store.dispatch(actions.setDidFinishReauth(true));
    } catch (error) {
      store.dispatch(actions.setDidFinishReauth(true));
      throw error;
    }
  }

  async getCodebook(codebookId) {
    return this.databaseLayer.getCodebook(codebookId);
  }

  async listSignedUserCodebooks() {
    const userId = this.auth.getSignedInUid();
    if (userId) {
      return this.databaseLayer.listUserCodebooks(userId);
    }
    return [];
  }

  async resetPassword() {
    return this.auth.resetPassword();
  }

  async signIn(email, password) {
    const userId = await this.auth.signInUser(email, password);
    trackSignIn(userId);
    return userId;
  }

  async signUp(email, password, username) {
    if (!username) {
      throw new Error('Please choose non-empty username');
    }
    const isUniqueName = await this.databaseLayer.isUsernameUnique(username);
    if (!isUniqueName) {
      throw new Error('Username is already registered');
    }
    const userId = await this.auth.createUser(email, password);
    trackSignUp(userId, email, username);
    return this.databaseLayer.createUser(username, userId);
  }

  async uploadCodebook(codebookId) {
    if (!await this.auth.isAuthenticated()) {
      return false;
    }
    try {
      const idToken = await this.auth.getIdToken();
      const uploadUrl = await requestUploadUrl(codebookId, idToken);
      const userId = await this.auth.getSignedInUid();
      const result = await uploadFileToUrl(uploadUrl, codebookId);
      if (result.status === 'OK') {
        await this.databaseLayer.createCodebook(codebookId, userId);
        return true;
      }
    } catch (error) {
      console.error('cannot upload codebook', error);
    }
    return undefined;
  }

  async downloadCodebook(codebookId) {
    try {
      const downloadUrl = await this.getCodebookDownloadUrl(codebookId);
      if (!downloadUrl) {
        console.log('cannot download');
        return false;
      }
      const result = await downloadFileFromUrl(downloadUrl, codebookId);
      return result.status === 'OK';
    } catch (error) {
      console.error(`cannot download public codebook ${codebookId}`, error);
    }
    return false;
  }

  // direct url, NOT our packaged that leads to codelines.dev
  // dont use - private
  async getCodebookDownloadUrl(codebookId) {
    try {
      const userId = this.auth.getSignedInUid();
      const downloadUrl = await this.storageLayer.generateCodebookDownloadUrl(userId, codebookId);
      return downloadUrl;
    } catch (error) {
      console.log(`codebook is not owned by user, searching public codebooks ${codebookId}`, error);
    }
    try {
      const downloadUrl = await requestDownloadUrl(codebookId);
      return downloadUrl;
    } catch (error) {
      console.log(`codebook is not public ${codebookId}`, error);
    }
    return undefined;
  }

  async generateCodeBookLink(username, codeBookId) {
    return `https://codelines.dev/${username}/${codeBookId}`;
  }

  async downloadCodeBookFromLink(link) {
    const splitted = link.split('/');
    const codeBookId = splitted.pop();
    const username = splitted.pop();
    if (codeBookId && username) {
      return this.downloadCodebook(codeBookId);
    }
    return undefined;
  }

  async signOut() {
    trackSignOut();
    return this.auth.signOutUser();
  }
}
