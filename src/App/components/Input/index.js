import React from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
  padding: 10px 12px;

  /* color: white; */
  color: var(--text-light);
  font-size: 18px;

  /* background: #1F212A; */
  background: var(--background-primary);

  border: 1px solid var(--separator);
  border-radius: 5px;

  ::-webkit-input-placeholder {
    /* color: #868A95; */
    color: var(--text-placeholder);
  }

  ${(props) => {
    if (props.isLight) {
      return `
        /* color: #1F212A; */
        color: var(--text-dark);
        /* background: white; */
        background: var(--background-secondary);
        border: 1px solid #868A95;

        ::-webkit-input-placeholder {
          /* color: #4d4f54; */
          color: var(--text-placeholder);
        }
      `;
    }

    return '';
  }}
`;

export default function Input(props) {
  return <StyledInput {...props} />;
}
