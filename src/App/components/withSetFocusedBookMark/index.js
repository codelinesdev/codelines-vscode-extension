import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  SET_FOCUSED_BOOK_MARK,
} from './types';

const withSetFocusedBookMark = WrappedComponent => (props) => {
  function setFocusedBookMark(bookMarkId) {
    const m = new Message(SET_FOCUSED_BOOK_MARK, {
      bookMarkId,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      setFocusedBookMark={setFocusedBookMark}
      {...props}
    />
  );
};

export default withSetFocusedBookMark;
