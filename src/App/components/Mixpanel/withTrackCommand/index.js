import React from 'react';
import trackEvent from '../trackEvent';

const withTrackCommand = WrappedComponent => (props) => {
  function trackCreateCommand(commandString, bookEditorTextLength) {
    trackEvent('COMMAND_CREATE', 'Command created', { commandString, bookEditorTextLength });
  }

  return (
    <WrappedComponent
      trackCreateCommand={trackCreateCommand}
      {...props}
    />
  );
};

export default withTrackCommand;
