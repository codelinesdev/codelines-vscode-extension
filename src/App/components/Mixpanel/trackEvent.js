import { Message } from '@/extension/shared/messageManager';

import types from './types';

export default function trackEvent(name, description, trackingData) {
  const m = new Message(types.MIXPANEL_TRACK_EVENT, {
    name,
    description,
    trackingData,
  });
  window.vscode.postMessage(m);
}
