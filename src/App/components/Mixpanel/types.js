// import * as trackEventTypes from './withTrackEvent/types';

// const types = {
//   ...trackEventTypes,
// };

const types = {
  MIXPANEL_TRACK_EVENT: 'MIXPANEL_TRACK_EVENT',
};

export default types;
