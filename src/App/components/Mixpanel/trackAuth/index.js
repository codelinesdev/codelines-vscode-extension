import trackEvent from '../trackEvent';

function trackSignIn(userId) {
  trackEvent('AUTH_SIGN_IN', 'User signed in', { userId });
}

function trackSignUp(userId, email, username) {
  trackEvent('AUTH_SIGN_UP', 'New user signed up', { userId, email, username });
}

function trackSignOut() {
  trackEvent('AUTH_SIGN_OUT', 'User signed out', {});
}

export {
  trackSignIn,
  trackSignUp,
  trackSignOut,
};
