import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  padding: 8px;

  font-weight: 600;
  font-size: 17px;
  color: white;

  background: ${(props) => {
    if (props.isGreen) {
      return '#1BAD84';
    }

    if (props.isBlue) {
      return '#1F212A';
    }

    return 'transparent';
  }};

  pointer-events: ${props => (props.isDisabled ? 'none' : 'auto')};

  border: none;
  border-radius: 5px;

  :hover {
    cursor: pointer;
    filter: brightness(110%);
  }
`;

export default function Button(props) {
  return <StyledButton {...props} />;
}
