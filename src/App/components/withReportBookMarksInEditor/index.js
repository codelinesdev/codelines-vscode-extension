import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  BOOK_MARKS_IN_EDITOR,
} from './types';

const withReportBookMarksInEditor = WrappedComponent => (props) => {
  function reportBookMarksInEditor(bookMarks, codeBookId, documentName) {
    const m = new Message(BOOK_MARKS_IN_EDITOR, {
      bookMarks,
      codeBookId,
      documentName,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      reportBookMarksInEditor={reportBookMarksInEditor}
      {...props}
    />
  );
};

export default withReportBookMarksInEditor;
