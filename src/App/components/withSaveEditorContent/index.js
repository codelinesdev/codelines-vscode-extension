import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  SAVE_EDITOR_CONTENT,
} from './types';

const withSaveEditorContent = WrappedComponent => (props) => {
  function saveEditorContent(editorContent, codeBookId, documentName) {
    const m = new Message(SAVE_EDITOR_CONTENT, {
      editorContent,
      codeBookId,
      documentName,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      saveEditorContent={saveEditorContent}
      {...props}
    />
  );
};

export default withSaveEditorContent;
