import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  DELETE_BOOK_MARK,
} from './types';

const withDeleteBookMark = WrappedComponent => (props) => {
  function deleteBookMark(editorContent, bookMarkId) {
    const m = new Message(DELETE_BOOK_MARK, {
      editorContent,
      bookMarkId,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      deleteBookMark={deleteBookMark}
      {...props}
    />
  );
};

export default withDeleteBookMark;
