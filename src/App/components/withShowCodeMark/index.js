import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  SHOW_CODE_MARK,
} from './types';

const withShowCodeMark = WrappedComponent => (props) => {
  function showCodeMark(bookMarkId) {
    const m = new Message(SHOW_CODE_MARK, {
      bookMarkId,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      showCodeMark={showCodeMark}
      {...props}
    />
  );
};

export default withShowCodeMark;
