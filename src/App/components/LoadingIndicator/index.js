import React from 'react';
import styled from 'styled-components';

import loaderIcon from '@/images/loader.png';

const LoaderImg = styled.img`
  width: 20px;
  height: 20px;
  animation:spin 2s linear infinite;

  @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }
`;

export default function LoadingIndicator(props) {
  return <LoaderImg src={loaderIcon} {...props} />;
}
