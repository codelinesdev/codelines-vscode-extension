import React from 'react';
import { connect } from 'react-redux';
import actions from '@/actions';
import useMessage from '@/App/hooks/useMessage';
import { Message } from '@/extension/shared/messageManager';

import {
  GET_WORKSPACE_CODE_BOOKS,
  WORKSPACE_CODE_BOOKS,
} from './types';

const withWorkspaceCodeBooks = (WrappedComponent) => {
  function getWorkspaceCodeBooks() {
    const m = new Message(GET_WORKSPACE_CODE_BOOKS, {});
    window.vscode.postMessage(m);
  }

  function WorkspaceCodeBooks(props) {
    function handler(message) {
      const { codeBooks } = message.data;
      props.setWorkspaceCodeBooks(codeBooks);
    }
    useMessage(WORKSPACE_CODE_BOOKS, handler);

    return (
      <WrappedComponent
        getWorkspaceCodeBooks={getWorkspaceCodeBooks}
        {...props}
      />
    );
  }

  function mapDispatchToProps(dispatch) {
    return {
      setWorkspaceCodeBooks: codeBooks => dispatch(actions.setWorkspaceCodeBooks(codeBooks)),
    };
  }

  return connect(
    null,
    mapDispatchToProps,
  )(WorkspaceCodeBooks);
};

export default withWorkspaceCodeBooks;
