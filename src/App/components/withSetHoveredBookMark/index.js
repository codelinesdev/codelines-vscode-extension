import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  SET_HOVERED_BOOK_MARK,
} from './types';

const withSetHoveredBookMark = WrappedComponent => (props) => {
  function setHoveredBookMark(bookMarkId) {
    const m = new Message(SET_HOVERED_BOOK_MARK, {
      bookMarkId,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      setHoveredBookMark={setHoveredBookMark}
      {...props}
    />
  );
};

export default withSetHoveredBookMark;
