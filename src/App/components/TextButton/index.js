import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  margin: 0;
  padding: 0;
  border: 0;
  background: transparent;

  font-size: 16px;

  color: ${(props) => {
    if (props.isGreen) {
      return '#1BAD84';
    }

    if (props.isWhite) {
      return 'white';
    }

    return '';
  }};

  :hover {
    cursor: pointer;
    filter: brightness(110%);
    background: ${props => (props.shouldShowBackgroundOnHover ? '#2D303D' : 'transparent')};
  }
`;

export default function TextButton(props) {
  return <StyledButton {...props} />;
}
