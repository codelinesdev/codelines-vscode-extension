import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import actions from '@/actions';
import { withFirebase } from '@/App/components/Firebase';
import compose from '@/utils/compose';

const withUser = (WrappedComponent) => {
  function WithUser(props) {
    useEffect(() => {
      props.firebase.onAuthStateChanged((user) => {
        props.setCurrentUser(user);
      });
    });

    return (
      <WrappedComponent
        {...props}
      />
    );
  }

  function mapDispatchToProps(dispatch) {
    return {
      setCurrentUser: user => dispatch(actions.setCurrentUser(user)),
    };
  }

  return compose(
    withFirebase,
    connect(
      null,
      mapDispatchToProps,
    ),
  )(WithUser);
};

export default withUser;
