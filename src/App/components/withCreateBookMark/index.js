import React from 'react';
import { Message } from '@/extension/shared/messageManager';

import {
  CREATE_BOOK_MARK,
} from './types';

const withCreateBookMark = WrappedComponent => (props) => {
  function createBookMark(editorContent, bookMarkBlocks, bookEditorTextLength, codeBookId, documentName) {
    const m = new Message(CREATE_BOOK_MARK, {
      editorContent,
      bookMarkBlocks,
      bookEditorTextLength,
      codeBookId,
      documentName,
    });
    window.vscode.postMessage(m);
  }

  return (
    <WrappedComponent
      createBookMark={createBookMark}
      {...props}
    />
  );
};

export default withCreateBookMark;
