import React from 'react';
import { connect } from 'react-redux';
import actions from '@/actions';
import useMessage from '@/App/hooks/useMessage';
import { Message } from '@/extension/shared/messageManager';

import {
  LOAD_CODE_BOOK,
  LOADED_CODE_BOOK,
} from './types';

const withLoadCodeBook = (WrappedComponent) => {
  function loadCodeBook(codeBookId) {
    const m = new Message(LOAD_CODE_BOOK, { id: codeBookId });
    window.vscode.postMessage(m);
  }

  function LoadCodeBook(props) {
    function handler(message) {
      const { codeBook } = message.data;
      props.setActiveCodeBook(codeBook);
    }
    useMessage(LOADED_CODE_BOOK, handler);

    return (
      <WrappedComponent
        loadCodeBook={loadCodeBook}
        {...props}
      />
    );
  }

  function mapDispatchToProps(dispatch) {
    return {
      setActiveCodeBook: codeBook => dispatch(actions.setActiveCodeBook(codeBook)),
    };
  }

  return connect(
    null,
    mapDispatchToProps,
  )(LoadCodeBook);
};

export default withLoadCodeBook;
