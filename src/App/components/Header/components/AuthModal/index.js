import React, { useState } from 'react';
import styled from 'styled-components';

import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import PasswordReset from './components/PasswordReset';

import compose from '@/utils/compose';
import { withFirebase } from '@/App/components/Firebase';
// import withOutsideClick from '@/App/components/withOutsideClick';

import Modal from '@/App/components/Modal';

const ModalBackgroundDiv = styled.div`
  z-index: 5;
  position: fixed;
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;

  background: rgba(0, 0, 0, 0.5);
`;

const StyledModal = styled(Modal)`
  z-index: 11;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  width: 435px;
  height: 350px;
`;

function AuthModal(props) {
  const [authContent, setAuthContent] = useState('sign-in');

  const [errorMessage, setErrorMessage] = useState('');

  const [isDisabledInteraction, setIsDisabledInteraction] = useState(false);

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function resetValues() {
    setErrorMessage('');
    setUsername('');
    setEmail('');
    setPassword('');
  }

  function changeToSignInClick() {
    resetValues();
    setAuthContent('sign-in');
  }

  function changeToSignUpClick() {
    resetValues();
    setAuthContent('sign-up');
  }

  function changeToPasswordResetClick() {
    resetValues();
    setAuthContent('password-reset');
  }

  async function signIn() {
    setErrorMessage('');
    setIsDisabledInteraction(true);
    try {
      await props.firebase.signIn(email, password);
      props.onClose();
    } catch (e) {
      setErrorMessage(e.message);
    } finally {
      setIsDisabledInteraction(false);
    }
  }

  async function signUp() {
    setErrorMessage('');
    setIsDisabledInteraction(true);
    try {
      await props.firebase.signUp(email, password, username);
      props.onClose();
    } catch (e) {
      setErrorMessage(e.message);
    } finally {
      setIsDisabledInteraction(false);
    }
  }

  function resetPassword() {
    setErrorMessage('');
    console.log('TODO Reset password');
  }

  return (
    <React.Fragment>
      <ModalBackgroundDiv />
      <StyledModal
        isLight
        onOutsideClick={props.onClose}
      >
        {authContent === 'sign-up' && (
          <SignUp
            errorMessage={errorMessage}
            isDisabledInteraction={isDisabledInteraction}
            usernameValue={username}
            onUsernameChange={e => setUsername(e.target.value)}
            emailValue={email}
            onEmailChange={e => setEmail(e.target.value)}
            passwordValue={password}
            onPasswordChange={e => setPassword(e.target.value)}
            onChangeToSignInClick={changeToSignInClick}
            onSignUpClick={signUp}
          />
        )}

        {authContent === 'sign-in' && (
          <SignIn
            errorMessage={errorMessage}
            isDisabledInteraction={isDisabledInteraction}
            emailValue={email}
            onEmailChange={e => setEmail(e.target.value)}
            passwordValue={password}
            onPasswordChange={e => setPassword(e.target.value)}
            onChangeToSignUpClick={changeToSignUpClick}
            onChangeToPasswordResetClick={changeToPasswordResetClick}
            onSignInClick={signIn}
          />
        )}

        {authContent === 'password-reset' && (
          <PasswordReset
            errorMessage={errorMessage}
            isDisabledInteraction={isDisabledInteraction}
            emailValue={email}
            onEmailChange={e => setEmail(e.target.value)}
            onPasswordResetClick={resetPassword}
          />
        )}
      </StyledModal>
    </React.Fragment>
  );
}

export default compose(
  withFirebase,
)(AuthModal);
