import React from 'react';
import styled from 'styled-components';

import Button from '@/App/components/Button';
import TextButton from '@/App/components/TextButton';
import ErrorMessage from '@/App/components/ErrorMessage';
import AuthInput from '../AuthInput';

const ContentDiv = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const InputsDiv = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const TextDiv = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;

  font-size: 16px;
  color: var(--text-dark);
`;

const StyledButton = styled(Button)`
  margin: 50px 10px 20px;
  width: 300px;
`;

export default function SignUp(props) {
  function handleKeyDown(event) {
    if (event.key === 'Enter') {
      props.onSignUpClick();
    }
  }

  return (
    <ContentDiv
      onKeyDown={handleKeyDown}
    >
      <InputsDiv>
        <AuthInput
          placeholder="username"
          value={props.usernameValue}
          onChange={props.onUsernameChange}
        />
        <AuthInput
          type="email"
          placeholder="email"
          value={props.emailValue}
          onChange={props.onEmailChange}
        />
        <AuthInput
          type="password"
          placeholder="password"
          value={props.passwordValue}
          onChange={props.onPasswordChange}
        />
        <TextDiv>
          <TextButton
            isGreen
            onClick={props.onChangeToSignInClick}
          >sign in
          </TextButton>
        </TextDiv>
      </InputsDiv>

      <ErrorMessage>{props.errorMessage}</ErrorMessage>

      <StyledButton
        isGreen
        isDisabled={props.isDisabledInteraction}
        onClick={props.onSignUpClick}
      >Sign Up
      </StyledButton>
    </ContentDiv>
  );
}
