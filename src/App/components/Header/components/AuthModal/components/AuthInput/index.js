import React from 'react';
import styled from 'styled-components';
import Input from '@/App/components/Input';

const StyledInput = styled(Input)`
  width: 300px;
  margin: 5px auto;
`;

export default function AuthInput(props) {
  return <StyledInput {...props} />;
}
