import React from 'react';
import styled from 'styled-components';

import Button from '@/App/components/Button';
import ErrorMessage from '@/App/components/ErrorMessage';
import AuthInput from '../AuthInput';

const ContentDiv = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const InputsDiv = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Text = styled.p`
  margin: 10px auto;
  width: 300px;
  font-size: 16px;
  color: #1F212A;
`;

const StyledButton = styled(Button)`
  margin: 50px 10px 20px;
  width: 300px;
`;

export default function PasswordReset(props) {
  function handleKeyDown(event) {
    if (event.key === 'Enter') {
      props.onPasswordResetClick();
    }
  }

  return (
    <ContentDiv
      onKeyDown={handleKeyDown}
    >
      <InputsDiv>
        <Text>
          Enter your email and we&apos;ll send you a link to reset your password
        </Text>
        <AuthInput
          placeholder="email"
          value={props.emailValue}
          onChange={props.onEmailChange}
        />
      </InputsDiv>

      <ErrorMessage>{props.errorMessage}</ErrorMessage>

      <StyledButton
        isBlue
        isDisabled={props.isDisabledInteraction}
        onClick={props.onPasswordResetClick}
      >Reset password
      </StyledButton>
    </ContentDiv>
  );
}
