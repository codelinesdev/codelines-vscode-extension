import React from 'react';
import styled from 'styled-components';

import TextButton from '@/App/components/TextButton';
import Modal from '@/App/components/Modal';

const StyledTextButton = styled(TextButton)`
  width: 100%;
  padding: 8px;

  font-weight: 600;
`;

export default function UserMenuModal(props) {
  return (
    <Modal
      isDark
      className={props.className}
      onOutsideClick={props.onOutsideClick}
    >
      <StyledTextButton
        isWhite
        shouldShowBackgroundOnHover
        onClick={props.onSignOutClick}
      >Sign Out
      </StyledTextButton>
    </Modal>
  );
}
