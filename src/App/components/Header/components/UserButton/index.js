import React, { useState } from 'react';
import styled from 'styled-components';

import compose from '@/utils/compose';
import { withFirebase } from '@/App/components/Firebase';

import TextButton from '@/App/components/TextButton';
import chevronDown from '@/images/chevron-down.png';

import UserMenuModal from './components/UserMenuModal';

const StyledUserMenuModal = styled(UserMenuModal)`
  z-index: 5;
  min-height: 100px;
  min-width: 110px;
  position: absolute;
  top: 50px;
  right: 10px;
`;

const UsernameButton = styled(TextButton)`
  display: flex;
  align-items: center;
  font-weight: 600;
`;

const ChevronImg = styled.img`
  width: 12px;
  height: 6px;
  margin-left: 5px;
`;

function UserButton(props) {
  const [isModalHidden, setIsModalHidden] = useState(true);

  function handleButtonClick() {
    if (isModalHidden) {
      setIsModalHidden(false);
    }
    // else - if a modal is visible it's same as clicking outside of the modal
  }

  function handleModalOutsideClick() {
    setIsModalHidden(true);
  }

  function handleSignOutClick() {
    props.firebase.auth.signOutUser();
  }

  return (
    <React.Fragment>
      {!isModalHidden && (
        <StyledUserMenuModal
          onSignOutClick={handleSignOutClick}
          onOutsideClick={handleModalOutsideClick}
        />
      )}
      <UsernameButton
        isWhite
        onClick={handleButtonClick}
      >
        {props.username}
        <ChevronImg src={chevronDown} alt="chevron-down" />
      </UsernameButton>
    </React.Fragment>
  );
}


export default compose(
  withFirebase,
)(UserButton);
