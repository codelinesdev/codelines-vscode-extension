import React, { useEffect } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import actions from '@/actions';
import compose from '@/utils/compose';

import withUser from '@/App/components/withUser';
import withWorkspaceName from '@/App/components/withWorkspaceName';

import Button from '@/App/components/Button';
import TextButton from '@/App/components/TextButton';
import LoadingIndicator from '@/App/components/LoadingIndicator';
import homeIcon from '@/images/home.png';
import shareCodeBookIcon from '@/images/share-code-book-enabled.png';

import UserButton from '../UserButton';

const SignInButton = styled(Button)`
  margin: 0 10px;
`;

const SectionDiv = styled.div`
  display: flex;
  align-items: center;
`;

const ShareButton = styled(Button)`
  margin: 0 10px;
  align-items: center;
  display: flex;

  color: #1BAD84;
  font-size: 13px;
`;

const ShareImg = styled.img`
  margin: 0 5px;
  width: 18px;
  height: 21px;
`;

const HomeButton = styled(TextButton)`
  margin: 0 10px;
`;

const HomeIcon = styled.img`
  height: 20px;
  width: 18px;
`;

const NameDiv = styled.div`
  margin: 0 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const WorkspaceTitle = styled.span`
  color: #868A95;
  font-family: 'RobotoMono';
  font-weight: 400;
  font-size: 14px;
`;

const CodeBookTitle = styled.span`
  margin-left: 2px;
  color: #1BAD84;
  font-family: 'RobotoMono';
  font-weight: 600;
  font-size: 16px;
`;

function BookEditorHeader(props) {
  const {
    getWorkspaceName,
  } = props;
  useEffect(() => {
    getWorkspaceName();
  }, [getWorkspaceName]);

  return (
    <React.Fragment>
      <SectionDiv>
        {props.activeCodeBook && (
          <NameDiv>
            <WorkspaceTitle>({props.workspace.name})/</WorkspaceTitle>
            <CodeBookTitle>{props.activeCodeBook.name}</CodeBookTitle>
          </NameDiv>
        )}
      </SectionDiv>

      <SectionDiv>
        {/* <ShareButton
          isBlue
        >
          <ShareImg src={shareCodeBookIcon} alt="share-code-book-image" />
          Share Code Book
        </ShareButton> */}

        {!props.auth.didFinishReauth && <LoadingIndicator />}

        {props.auth.didFinishReauth && (
          <React.Fragment>
            {props.currentUser && (
              <React.Fragment>
                <UserButton
                  username={props.currentUser.username}
                />
              </React.Fragment>
            )}

            {!props.currentUser && (
              <SignInButton
                isBlue
                onClick={props.onSignIn}
              >Sign In
              </SignInButton>
            )}
          </React.Fragment>
        )}

        <HomeButton
          onClick={props.onGoHome}
        >
          <HomeIcon src={homeIcon} alt="go home" />
        </HomeButton>
      </SectionDiv>
    </React.Fragment>
  );
}

function mapStateToProps(state) {
  return {
    workspace: state.workspace,
    activeCodeBook: state.codeBook.activeCodeBook,
    auth: state.auth,
    currentUser: state.user.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoute: route => dispatch(actions.setRoute(route)),
  };
}

export default compose(
  withUser,
  withWorkspaceName,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(BookEditorHeader);
