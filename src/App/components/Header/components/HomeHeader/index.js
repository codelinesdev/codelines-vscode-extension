import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import compose from '@/utils/compose';
import withUser from '@/App/components/withUser';

import Button from '@/App/components/Button';
import LoadingIndicator from '@/App/components/LoadingIndicator';

import UserButton from '../UserButton';

const StyledButton = styled(Button)`
  margin: 0 10px;
`;

function HomeHeader(props) {
  return (
    <React.Fragment>
      <StyledButton
        isGreen
        onClick={props.onOpenEditor}
      >New Code Book
      </StyledButton>

      {!props.auth.didFinishReauth && <LoadingIndicator />}

      {props.auth.didFinishReauth && (
        <React.Fragment>
          {props.currentUser && (
            <React.Fragment>
              <UserButton
                username={props.currentUser.username}
              />
            </React.Fragment>
          )}

          {!props.currentUser && (
            <StyledButton
              isBlue
              onClick={props.onSignIn}
            >Sign In
            </StyledButton>
          )}
        </React.Fragment>
      )}


    </React.Fragment>
  );
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    currentUser: state.user.currentUser,
  };
}

export default compose(
  withUser,
  connect(
    mapStateToProps,
    null,
  ),
)(HomeHeader);
