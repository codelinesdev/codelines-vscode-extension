import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import actions from '@/actions';
import compose from '@/utils/compose';

import withCreateCodeBook from '@/App/components/withCreateCodeBook';
import withDidCloseCodeBook from '@/App/components/withDidCloseCodeBook';

import AuthModal from './components/AuthModal';
import HomeHeader from './components/HomeHeader';
import BookEditorHeader from './components/BookEditorHeader';

const HeaderDiv = styled.div`
  min-height: 50px;
  width: 100%;
  padding: 15px 20px;
  display: flex;
  align-items: center;

  /* background: #3C3F50; */
  background: var(--background-primary);

  border-bottom: 2px solid var(--separator);

  ${(props) => {
    switch (props.route) {
      case 'home':
        return `
          justify-content: flex-end;
        `;
      case 'editor':
        return `
          justify-content: space-between;
          /* background: #2D303D; */
          background: var(--background-primary);
          /* border-bottom: 2px solid #3C3F50; */
        `;
      default:
        return '';
    }
  }}
`;

function Header(props) {
  const [isAuthModalOpened, setIsAuthModelOpened] = useState(false);

  function openAuthModal() {
    setIsAuthModelOpened(true);
  }

  function closeAuthModal() {
    setIsAuthModelOpened(false);
  }

  function openEditor() {
    props.createCodeBook();
  }

  function shareCodeBook() {
    console.log('Share code book');
  }

  function goHome() {
    const { id } = props.activeCodeBook;
    props.didCloseCodeBook(id);
    props.setRoute('home');
  }

  return (
    <React.Fragment>
      {isAuthModalOpened && (
        <AuthModal
          onClose={closeAuthModal}
        />
      )}
      <HeaderDiv
        route={props.route}
      >
        {props.route === 'home' && (
          <HomeHeader
            onOpenEditor={openEditor}
            onSignIn={openAuthModal}
          />
        )}

        {props.route === 'editor' && (
          <BookEditorHeader
            onSignIn={openAuthModal}
            onShareCodeBook={shareCodeBook}
            onGoHome={goHome}
          />
        )}
      </HeaderDiv>
    </React.Fragment>
  );
}

function mapStateToProps(state) {
  return {
    route: state.router.route,
    activeCodeBook: state.codeBook.activeCodeBook,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoute: route => dispatch(actions.setRoute(route)),
  };
}

export default compose(
  withCreateCodeBook,
  withDidCloseCodeBook,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Header);
