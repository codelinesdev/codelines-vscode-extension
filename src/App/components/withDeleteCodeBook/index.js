import React from 'react';
import { Message } from '@/extension/shared/messageManager';
import {
  promisifyEvent,
} from '@/utils/promisifyEvent';

import {
  DELETE_CODE_BOOK,
  DID_DELETE_CODE_BOOK,
} from './types';

const withDeleteCodeBook = WrappedComponent => (props) => {
  function sendDeleteCodeBook(id) {
    const m = new Message(DELETE_CODE_BOOK, { id });
    window.vscode.postMessage(m);
  }

  async function deleteCodeBook(codeBookId) {
    const event = await promisifyEvent(
      l => window.addEventListener('message', l),
      l => window.removeEventListener('message', l),
      () => sendDeleteCodeBook(codeBookId),
      e => e.data.type === DID_DELETE_CODE_BOOK,
    );
    return event.data.data;
  }

  return (
    <WrappedComponent
      deleteCodeBook={deleteCodeBook}
      {...props}
    />
  );
};

export default withDeleteCodeBook;
