import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import actions from '@/actions';
import {
  messageType,
  Message,
} from '@/extension/shared/messageManager';
import BookEditor from './components/BookEditor';
// import Header from './components/Header';
// import NewDocumentModal from './components/NewDocumentModal';
// import DeleteDocumentModal from './components/DeleteDocumentModal';
// import ShareCodeBookModal from './components/ShareCodeBookModal';
// import CodeBookUrlModal from './components/CodeBookUrlModal';

import compose from '@/utils/compose';
import { withFirebase } from '@/App/components/Firebase';

// TODO: Change theming based on the currently selected
// theme by user (light, dark, high-contrast)
// see https://code.visualstudio.com/api/extension-guides/webview

const WrapperDiv = styled.div`
  max-width: 100%;
  width: 100%;
`;

const BookEditorWrapper = styled.div`
  max-width: 100%;
  width: 100%;
  height: 100%;
  margin: 40px auto 0;
  padding: 10px;
`;

const Button = styled.button``;

function BookEditorView(props) {
  const [isNewDocumentModalHidden, setIsNewDocumentModalHidden] = useState(true);
  const [isDeleteModalHidden, setIsDeleteModalHidden] = useState(true);
  const [isShareModalHidden, setIsShareModalHidden] = useState(true);
  const [newDocumentName, setNewDocumentName] = useState('');
  const [codeBookShareName, setCodeBookShareName] = useState('');
  const [isUploadingCodeBook, setIsUploadingCodeBook] = useState(false);
  const [isCodeBookUrlModalHidden, setIsCodeBookUrlModalHidden] = useState(true);
  const [codeBookUrl, setCodeBookUrl] = useState('');


  ////////////// window 'message'


  function handleCodeBookUploaded(message) {
    setCodeBookUrl(message.data.codeBookUrl);
    setIsUploadingCodeBook(false);
    setIsShareModalHidden(true);
    setIsCodeBookUrlModalHidden(false);
  }


  ////////////// window 'message'

  function shareCodeBook() {
    setIsUploadingCodeBook(true);
    const t = messageType.CODE_BOOK.SHARE_CODE_BOOK;
    const m = new Message(t, { codeBookShareName });
    window.vscode.postMessage(m);
  }
  function handleCodeBookDocumentSelection(event) {
    const docContentPath = event.target.value;
    const newActiveDoc = props.codeBookDocuments.filter(doc => doc.contentPath === docContentPath)[0];

    props.setActiveCodeBookDocument(newActiveDoc);

    // TODO: Show something like "loading" while we wait for the extension to load the document's content

    const t = messageType.CODE_BOOK.LOAD_CODE_BOOK_DOCUMENT;
    const m = new Message(t, { codeBookDocument: newActiveDoc });
    window.vscode.postMessage(m);
  }

  function handleCreateNewDocumentModalButtonClick() {
    if (newDocumentName) {
      const t = messageType.CODE_BOOK.CREATE_NEW_CODE_BOOK_DOCUMENT;
      const m = new Message(t, { newDocumentName });
      window.vscode.postMessage(m);
      setNewDocumentName('');
      setIsNewDocumentModalHidden(true);
    }
  }

  function deleteActiveCodeBookDocument() {
    if (props.codeBookDocuments.length > 1) {
      const t = messageType.CODE_BOOK.DELETE_CODE_BOOK_DOCUMENT;
      const m = new Message(t, {
        codeBookDocument: props.activeCodeBookDocument,
      });
      window.vscode.postMessage(m);

      // Remove delete code book doc from all available code book docs
      const newCodeBookDocs = props.codeBookDocuments.filter(doc => doc.contentPath !== props.activeCodeBookDocument.contentPath);
      props.setCodeBookDocuments(newCodeBookDocs);
      // Switch to some other code book document
      const newActiveDoc = newCodeBookDocs[0];
      props.setActiveCodeBookDocument(newActiveDoc);

      const t2 = messageType.CODE_BOOK.LOAD_CODE_BOOK_DOCUMENT;
      const m2 = new Message(t2, { codeBookDocument: newActiveDoc });
      window.vscode.postMessage(m2);

      setIsDeleteModalHidden(true);
    }
  }

  // useEffect(() => {
  //   function handleMarkerReady(message) {
  //     const {
  //       codeBookDocuments,
  //       loadedDocument,
  //     } = message.data;

  //     props.setActiveCodeBookDocument(loadedDocument);
  //     props.setCodeBookDocuments(codeBookDocuments);

  //     props.setIsMarkerReady(true);
  //   }

  //   function handleCodeBookDocumentsUpdated(message) {
  //     const { codeBookDocuments } = message.data;
  //     props.setCodeBookDocuments(codeBookDocuments);
  //   }

  //   function handleExtensionMessage(event) {
  //     console.log('<App extension message>', event);

  //     const message = event.data;
  //     const { type } = message;

  //     switch (type) {
  //       // case messageType.MARKER_READY: {
  //       //   handleMarkerReady(message);
  //       //   break;
  //       // }
  //       // case messageType.CODE_BOOK.CODE_BOOK_DOCUMENTS_UPDATED: {
  //       //   handleCodeBookDocumentsUpdated(message);
  //       //   break;
  //       // }
  //       // case messageType.CODE_BOOK.CODE_BOOK_UPLOADED: {
  //       //   handleCodeBookUploaded(message);
  //       //   break;
  //       // }
  //       default:
  //         break;
  //     }
  //   }

  //   // window.addEventListener('message', handleExtensionMessage);

  //   // TODO: Init firebase here

  //   // const t = messageType.BOOK_EDITOR.BOOK_EDITOR_READY;
  //   // const m = new Message(t, {});
  //   // window.vscode.postMessage(m);

  //   return () => {
  //     // window.removeEventListener('message', handleExtensionMessage);
  //   };
  // }, [props]);

  return (
    <WrapperDiv>
      {/* {!isNewDocumentModalHidden
        && (
          <NewDocumentModal
            onCreateDocumentClick={handleCreateNewDocumentModalButtonClick}
            onInputChange={event => setNewDocumentName(event.target.value)}
            onCloseButtonClick={() => setIsNewDocumentModalHidden(true)}
          />
        )
      }
      {!isDeleteModalHidden
        && (
          <DeleteDocumentModal
            onCancelClick={() => setIsDeleteModalHidden(true)}
            onDeleteClick={deleteActiveCodeBookDocument}
          />
        )
      }
      {!isShareModalHidden
        && (
          <ShareCodeBookModal
            onShareClick={shareCodeBook}
            onInputChange={event => setCodeBookShareName(event.target.value)}
            onCloseClick={() => setIsShareModalHidden(true)}
            isUploading={isUploadingCodeBook}
          />
        )
      }
      {!isCodeBookUrlModalHidden && (
        <CodeBookUrlModal
          codeBookUrl={codeBookUrl}
          onCloseClick={() => setIsCodeBookUrlModalHidden(true)}
        />
      )} */}
      {/* <Header
        activeDocument={props.activeCodeBookDocument}
        codeBookDocuments={props.codeBookDocuments}
        onCodeBookDocumentSelection={handleCodeBookDocumentSelection}
        onNewCodeBookDocumentButtonClick={() => setIsNewDocumentModalHidden(false)}
        isDeleteButtonHidden={props.codeBookDocuments.length <= 1}
        onDeleteCodeBookDocumentButtonClick={() => setIsDeleteModalHidden(false)}
        onShareButtonClick={() => setIsShareModalHidden(false)}
      /> */}

      <BookEditorWrapper>
        <BookEditor />
      </BookEditorWrapper>
    </WrapperDiv>
  );
}

function mapStateToProps(state) {
  return {
    // authUser: state.authUser.authUser,
    isMarkerReady: state.marker.isMarkerReady,
    activeCodeBookDocument: state.codeBook.activeCodeBookDocument,
    codeBookDocuments: state.codeBook.codeBookDocuments,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setIsMarkerReady: isReady => dispatch(actions.setIsMarkerReady(isReady)),
    setActiveCodeBookDocument: doc => dispatch(actions.setCodeBookActiveDocument(doc)),
    setCodeBookDocuments: docs => dispatch(actions.setCodeBookDocuments(docs)),
  };
}

export default compose(
  withFirebase,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(BookEditorView);
