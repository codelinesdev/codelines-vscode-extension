import React from 'react';
import styled from 'styled-components';

const ModalDiv = styled.div`
  height: 200px;
  width: 360px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  margin: auto;
  padding: 20px 0;


  background: #191a21;
  border-radius: 8px;
  box-shadow: 0 2px 15px 10px rgba(33, 34, 40, 0.5);
`;

const Title = styled.h3`
  font-size: 20px;
  margin: 5px 0;
`;

const Input = styled.input`
  width: 250px;
  margin: 5px 0;
  padding: 10px 8px;
  font-size: 20px;
  border-radius: 3px;
  border: none;
`;

const ButtonsDiv = styled.div`
  margin: 5px 10px;
  display: flex;
  justify-content: space-between;
`;

const Button = styled.button`
  margin: 0 5px;
  padding: 8px 12px;
  border: none;
  border-radius: 5px;
  background-color: #3C3F50;

  font-size: 15px;
  font-weight: bold;
  color: white;

  :hover {
    cursor: pointer;
  }
`;

const UploadingText = styled.p``;

export default function ShareCodeBookModal(props) {
  return (
    <ModalDiv className={props.className}>
      <Title>Share Code Book</Title>

      {props.isUploading && (
        <UploadingText>Uploading Code Book...</UploadingText>
      )}

      {!props.isUploading && (
        <React.Fragment>
          <Input
            type="text"
            placeholder="Code Book Name"
            value={props.codeBookName}
            onChange={props.onInputChange}
          />

          <ButtonsDiv>
            <Button
              onClick={props.onCloseClick}
            >Close
            </Button>

            <Button
              onClick={props.onShareClick}
            >Share Code Book
            </Button>
          </ButtonsDiv>
        </React.Fragment>
      )}
    </ModalDiv>
  );
}
