import React from 'react';
import styled from 'styled-components';
import selectArrowIcon from './select-arrow.png';
import addDocumentIcon from './add-document.png';
import deleteDocumentIcon from './delete-document.png';
import backToStartMenuIcon from './back-start-menu.png';

const HeaderDiv = styled.div`
  padding: 5px 10px 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const DocumentSelect = styled.select`
  width: 200px;
  margin: 10px 10px 10px 0;
  padding: 10px 20px;

  font-size: 20px;
  color: white;

  -webkit-appearance: none;

  background: url(${selectArrowIcon}) no-repeat right #2B2B2B;
  background-repeat: no-repeat, repeat;
  background-position: right .7em top 50%, 0 0;
  background-size: .65em auto, 100%;

  border: 1px solid #1D202A;
  border-radius: 5px;

  :hover {
    cursor: pointer;
    filter: brightness(150%);
  }
`;

const NewDocumentButton = styled.button`
  margin-right: 10px;
  padding: 10px;
  background: url(${addDocumentIcon}) no-repeat;
  background-position: center;
  background-size: 40px auto, 100%;
  border: none;
  :hover {
    cursor: pointer;
    filter: brightness(150%);
  }
`;

const DeleteDocumentButton = styled.button`
  visibility: ${props => (props.isHidden ? 'hidden' : 'visible')};
  padding: 10px;
  background: url(${deleteDocumentIcon}) no-repeat;
  background-position: center;
  background-size: 20px auto, 100%;
  border: none;
  :hover {
    cursor: pointer;
    filter: brightness(150%);
  }
`;

const BackToStartMenuButton = styled.button`
  margin-right: 10px;
  margin-left: auto;
  padding: 5px;
  width: 30px;
  height: 30px;
  background: url(${backToStartMenuIcon}) no-repeat;
  background-position: center;
  background-size: 100%;
  border: none;
  :hover {
    cursor: pointer;
    filter: brightness(150%);
  }
`;

const LeftDiv = styled.div`
  display: flex;
`;

const RightDiv = styled.div`
  display: flex;
`;

const ShareButton = styled.button`
  padding: 10px 8px;

  font-size: 18px;
  color: #1BAD84;

  background: transparent;
  border-radius: 5px;
  border: 1px solid #1BAD84;

  :hover {
    cursor: pointer;
  }
`;

export default function Header(props) {
  return (
    <HeaderDiv className={props.className}>
      <LeftDiv>
        <DocumentSelect value={props.activeDocument.contentPath} onChange={props.onCodeBookDocumentSelection}>
          {props.codeBookDocuments.map(doc => <option value={doc.contentPath} key={doc.contentPath}>{doc.name}</option>)}
        </DocumentSelect>

        <NewDocumentButton
          onClick={props.onNewCodeBookDocumentButtonClick}
        />
        <DeleteDocumentButton
          isHidden={props.isDeleteButtonHidden}
          onClick={props.onDeleteCodeBookDocumentButtonClick}
        />
      </LeftDiv>

      <RightDiv>
        <ShareButton
          onClick={props.onShareButtonClick}
        >Share Code Book
        </ShareButton>
        <BackToStartMenuButton
          onClick={props.onBackToStartMenuButtonClick}
        />
      </RightDiv>
    </HeaderDiv>
  );
}
