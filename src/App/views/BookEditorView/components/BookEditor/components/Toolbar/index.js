import React, { useEffect } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import createMarkDisabledIcon from '@/images/create-mark-disabled.png';
import createMarkEnabledIcon from '@/images/create-mark-enabled.png';

import showCodeDisabledIcon from '@/images/show-code-disabled.png';
import showCodeEnabledIcon from '@/images/show-code-enabled.png';

import deleteMarkDisabledIcon from '@/images/delete-mark-disabled.png';
import deleteMarkEnabledIcon from '@/images/delete-mark-enabled.png';

import addCommandDisabledIcon from '@/images/add-command-disabled.png';
import addCommandEnabledIcon from '@/images/add-command-enabled.png';

import shareCodeBookDisabledIcon from '@/images/share-code-book-disabled.png';
import shareCodeBookEnabledIcon from '@/images/share-code-book-enabled.png';

const ToolbarWrapper = styled.div`
  min-width: 750px;
  display: flex;
  justify-content: space-between;

  padding: 8px 25px;

  box-shadow: 0 0 30px 10px rgba(23, 22, 22, 0.35);
  background: #1B1D26;

  border-radius: 5px;
`;

const Section = styled.div`
  display: flex;
`;

const Button = styled.button`
  display: flex;
  flex-direction: column;
  align-items: center;

  border: none;
  background: transparent;

  &:not(:last-child) {
    margin-right: 5px;
  }

  :hover {
    cursor: pointer;
    ${props => props.isEnabled && 'filter: brightness(150%);'}
  }
`;

const ButtonImg = styled.img`
  margin: 10px 0;
  width: 25px;
  height: 25px;
`;

const ButtonText = styled.span`
  color: ${props => (props.isDisabled ? '#868A95' : '#1BAD84')};
  font-size: 16px;
`;

// TODO: Remove for production

const WrapperDiv = styled.div`
  display: flex;
  flex-direction: column;

  box-shadow: 0 0 30px 10px rgba(23, 22, 22, 0.35);
  background: #1B1D26;
`;

const DebugInfo = styled.span`
  font-size: 14px;
`;

function Toolbar(props) {
  return (
    <ToolbarWrapper className={props.className}>
      <Section>
        <Button
          onClick={props.onCreateMarkClick}
          isEnabled={props.canCreateMark}
        >
          {props.canCreateMark && (
            <ButtonImg
              src={createMarkEnabledIcon}
              alt="create-mark"
            />
          )}
          {!props.canCreateMark && (
            <ButtonImg
              src={createMarkDisabledIcon}
              alt="create-mark"
            />
          )}
          <ButtonText
            isDisabled={!props.canCreateMark}
          >Create Mark
          </ButtonText>
        </Button>

        <Button
          onClick={props.onDeleteMarkClick}
          isEnabled={props.isMarkInteractionEnabled}
        >
          {props.isMarkInteractionEnabled && (
            <ButtonImg
              src={deleteMarkEnabledIcon}
              alt="delete-mark"
            />
          )}
          {!props.isMarkInteractionEnabled && (
            <ButtonImg
              src={deleteMarkDisabledIcon}
              alt="delete-mark"
            />
          )}
          <ButtonText
            isDisabled={!props.isMarkInteractionEnabled}
          >Delete Mark
          </ButtonText>
        </Button>

        <Button
          onClick={props.onShowCodeClick}
          isEnabled={props.isMarkInteractionEnabled}
        >
          {props.isMarkInteractionEnabled && (
            <ButtonImg
              src={showCodeEnabledIcon}
              alt="show-code"
            />
          )}
          {!props.isMarkInteractionEnabled && (
            <ButtonImg
              src={showCodeDisabledIcon}
              alt="show-code"
            />
          )}
          <ButtonText
            isDisabled={!props.isMarkInteractionEnabled}
          >Show Code
          </ButtonText>
        </Button>
      </Section>

      <Section>
        <Button
          onClick={props.onAddCommandClick}
          isEnabled
        >
          <ButtonImg
            src={addCommandEnabledIcon}
            alt="add-command"
          />
          <ButtonText>Add Command</ButtonText>
        </Button>

        {/* <Button
            onClick={props.onShareCodeBookClick}
            isEnabled
          >
            <ButtonImg
              src={shareCodeBookEnabledIcon}
              alt="share-code-book"
            />
            <ButtonText>Share Code Book</ButtonText>
          </Button> */}
      </Section>
    </ToolbarWrapper>
  );
}


function mapStateToProps(state) {
  return {
    focusedBookMarkId: state.bookMarks.focusedBookMarkId,
  };
}

export default connect(
  mapStateToProps,
  null,
)(Toolbar);
