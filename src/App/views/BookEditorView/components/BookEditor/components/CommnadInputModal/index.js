import React, { useState } from 'react';
import styled from 'styled-components';

const ModalDiv = styled.div`
  height: 200px;
  width: 360px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  margin: auto;
  padding: 20px 0;


  background: #191a21;
  border-radius: 8px;
  box-shadow: 0 2px 15px 10px rgba(33, 34, 40, 0.5);
`;

const Input = styled.input`
  width: 250px;
  margin: 5px 0;
  padding: 10px 8px;
  font-size: 20px;
  border-radius: 3px;
  border: none;
`;

const Buttons = styled.div`
  margin: 5px 10px;
  display: flex;
  justify-content: space-between;
`;

const Button = styled.button`
  margin: 0 5px;
  padding: 8px 12px;
  border: none;
  border-radius: 5px;
  background-color: #3C3F50;

  font-size: 15px;
  font-weight: bold;
  color: ${props => (props.isEnabled ? '#1BAD84' : '#868A95')};

  :hover {
    cursor: pointer;
  }
`;

export default function CommnadInputModal(props) {
  const [commandString, setCommandString] = useState('');

  function handleSetButtonClick() {
    if (commandString) {
      props.onSetButtonClick(commandString);
    }
  }

  return (
    <ModalDiv>
      Command
      <Input
        type="text"
        placeholder="e.g.: echo 'Hello World'"
        value={commandString}
        onChange={e => setCommandString(e.target.value)}
      />

      <Buttons>
        <Button
          onClick={props.onCloseButtonClick}
          isEnabled
        >Close
        </Button>
        <Button
          onClick={handleSetButtonClick}
          isEnabled={!!commandString}
        >Set
        </Button>
      </Buttons>
    </ModalDiv>
  );
}
