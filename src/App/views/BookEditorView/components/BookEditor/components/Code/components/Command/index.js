import React from 'react';
import styled from 'styled-components';

import compose from '@/utils/compose';
import withRunCommand from '@/App/components/withRunCommand';

import {
  messageType,
  Message,
} from '@/extension/shared/messageManager';

const CommandDiv = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "RobotoMono";
  user-select: none;
`;

const CommandText = styled.div`
  /* background: #1D1E1F; */
  background: var(--background-primary);

  padding: 10px 8px;
  border-radius: 5px;
  border: 1px solid var(--separator);
  /* color: white; */
  color: var(--text-light);
`;

const Buttons = styled.div`
  margin-top: 15px;
  display: flex;
  justify-content: flex-end;
`;

const Button = styled.button`
  padding: 5px 10px;
  font-size: 20px;
  border-radius: 5px;
  border: none;

  :hover {
    cursor: pointer;
    filter: brightness(120%);
  }
`;

// const DeleteButton = styled(Button)`
// color: #1D1E1F;
// background: #868A95;
// `;

const RunButton = styled(Button)`
  color: white;
  background: #1BAD84;
`;

function Command(props) {
  function onRunButtonClick() {
    props.runCommand(props.commandString);
  }

  return (
    <CommandDiv
      contentEditable={false}
      readOnly
    >
      <CommandText>$ {props.commandString}</CommandText>
      <Buttons>
        {/* <DeleteButton>Delete
        </DeleteButton> */}
        <RunButton
          onClick={onRunButtonClick}
        >Run
        </RunButton>
      </Buttons>
    </CommandDiv>
  );
}

export default compose(
  withRunCommand,
)(Command);
