import React from 'react';
import Command from './components/Command';

export default function Code(props) {
  const entityKey = props.block.getEntityAt(0);
  if (entityKey) {
    const entity = props.contentState.getEntity(
      props.block.getEntityAt(0),
    );
    const data = entity.getData();
    const type = entity.getType();

    if (type === 'CODE_COMMAND') {
      return <Command commandString={data.commandString} blockKey={props.block.getKey()} />;
    }
    return null;
  }
  return null;
}
