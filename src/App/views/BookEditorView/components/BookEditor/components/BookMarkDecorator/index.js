import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import {
  messageType,
  Message,
} from '@/extension/shared/messageManager';
import actions from '@/actions';

import compose from '@/utils/compose';
import withSetFocusedBookMark from '@/App/components/withSetFocusedBookMark';
import withSetHoveredBookMark from '@/App/components/withSetHoveredBookMark';

const Wrapper = styled.div`
  display: inline;

  background: ${props => (props.isHighlighted ? 'rgba(27, 173, 132, 0.9)' : 'transparent')};

  :hover {
    cursor: pointer;
  }
`;

const BookMarkSpan = styled.span`
  /* background-image: ${props => (props.isHighlighted ? 'linear-gradient(to right, rgba(27, 173, 132, 0.9) 0%, rgba(27, 173, 132, 0.9) 100%)' : 'linear-gradient(to right, rgba(27, 173, 132, 0.7) 0%, rgba(27, 173, 132, 0.7) 100%)')}; */
  background-image: linear-gradient(to right, rgba(27, 173, 132, 0.7) 0%, rgba(27, 173, 132, 0.7) 100%);
  /* Make it horizontal */
  background-repeat: repeat-x;
   /* Change the second value to adjust how far from the top the underline should be */
  background-position: 0 100%;
  /* Change the second value to the desired height of the underline */
  background-size: 100% 4px;
`;

class BookMarkDecorator extends React.Component {
  constructor(props) {
    super(props);
    this.bookMarkRef = React.createRef();

    this.state = {
      bookMarkId: this.getBookMarkId(),
    };
  }

  componentDidMount() {
    this.mousedownListener = document.addEventListener('mousedown', (e) => {
      if (this.props.editorRef && this.props.editorRef.editor && this.bookMarkRef) {
        const isInEditorEl = this.props.editorRef.editor.contains(e.target);
        const isThisEl = this.bookMarkRef.contains(e.target);

        if (isInEditorEl && !isThisEl) {
          this.props.setFocusedBookMarkId('');
          this.props.setFocusedBookMark('');

          this.props.setHoveredBookMarkId('');
          this.props.setHoveredBookMark('');
        }
      }
    }, false);
  }

  componentDidUpdate(prevProps) {
    // Different decorator if true
    if (this.props.entityKey !== prevProps.entityKey) {
      // Draft.js constructs just one BookMarkDecorator but keeps feeding it with different props
      // Therefore the constructor is called just once.

      this.setState({
        bookMarkId: this.getBookMarkId(),
      });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.mousedownListener);
  }

  getBookMarkId() {
    const {
      entityKey,
      contentState,
    } = this.props;

    const entity = contentState.getEntity(entityKey);
    const { bookMarkId } = entity.getData();
    return bookMarkId;
  }


  handleWrapperClick() {
    const { bookMarkId } = this.state;
    this.props.setFocusedBookMarkId(bookMarkId);
    // This is from 'withSetFocusedBookMark' HOC
    this.props.setFocusedBookMark(bookMarkId);
  }

  handleWrapperOnMouseOver() {
    if (this.props.focusedBookMarkId !== this.state.bookMarkId) {
      const { bookMarkId } = this.state;
      this.props.setHoveredBookMarkId(bookMarkId);
      // This is from 'withSetFocusedBookMark' HOC
      this.props.setHoveredBookMark(bookMarkId);
    }
  }

  handleWrapperOnMouseLeave() {
    if (this.props.focusedBookMarkId !== this.state.bookMarkId) {
      this.props.setHoveredBookMarkId('');
      this.props.setHoveredBookMark('');
    }
  }

  render() {
    const { bookMarkId } = this.state;
    const {
      hoveredBookMarkId,
      focusedBookMarkId,
    } = this.props;

    const isHighlighted = (hoveredBookMarkId === bookMarkId) || (focusedBookMarkId === bookMarkId);

    return (
      <Wrapper
        onClick={() => this.handleWrapperClick()}
        isHighlighted={isHighlighted}
        onMouseOver={() => this.handleWrapperOnMouseOver()}
        onMouseLeave={() => this.handleWrapperOnMouseLeave()}
      >
        <BookMarkSpan
          ref={(el) => {
            this.bookMarkRef = el;
          }}
          isHighlighted={isHighlighted}
        >
          {this.props.children}
        </BookMarkSpan>
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    hoveredBookMarkId: state.bookMarks.hoveredBookMarkId,
    focusedBookMarkId: state.bookMarks.focusedBookMarkId,
    editorRef: state.editor.editorRef,
  };
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    setFocusedBookMarkId: id => dispatch(actions.setFocusedBookMarkId(id)),
    setHoveredBookMarkId: id => dispatch(actions.setHoveredBookMarkId(id)),
  };
}

// Connected Component
export default compose(
  withSetFocusedBookMark,
  withSetHoveredBookMark,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(BookMarkDecorator);
