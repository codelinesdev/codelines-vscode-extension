import React from 'react';
import {
  connect,
} from 'react-redux';
import styled from 'styled-components';
import {
  AtomicBlockUtils,
  convertToRaw,
  convertFromRaw,
  CompositeDecorator,
  Editor,
  EditorState,
  getDefaultKeyBinding,
} from 'draft-js';
import {
  messageType,
  Message,
} from '@/extension/shared/messageManager';
import {
  extractTextFromContentState,
  getStartingGlobalOffsetForSelection,
  getSelectedTextForSelectionState,
  removeBookMarkEntitiesFromState,
  findBookMarksInContent,
} from '@/utils/editor';
import actions from '@/actions';
import BookMarkStrategy from '@/utils/editor/strategies/BookMark';
import BookMarkChangeHandler from '@/utils/bookMark/BookMarkChangeHandler';

import compose from '@/utils/compose';
import withSaveEditorContent from '@/App/components/withSaveEditorContent';
import withCreateBookMark from '@/App/components/withCreateBookMark';
import withDeleteBookMark from '@/App/components/withDeleteBookMark';
import withShowCodeMark from '@/App/components/withShowCodeMark';
import withReportBookMarksInEditor from '@/App/components/withReportBookMarksInEditor';
import { withTrackCommand } from '@/App/components/Mixpanel';

import BookMarkDecorator from './components/BookMarkDecorator';
import Toolbar from './components/Toolbar';
import Code from './components/Code';
import CommnadInputModal from './components/CommnadInputModal';


const WrapperDiv = styled.div`
  width: 100%;
  max-width: 100%;
`;

const StyledToolbar = styled(Toolbar)`
  position: fixed;
  bottom: 15px;
  left: 50%;
  transform: translateX(-50%);
  max-width: 800px;
  width: 100%;
  margin: 10px 0;
`;

const EditorWrapper = styled.div`
  padding: 0 5px 120px;
  font-size: 18px;
  line-height: 1.8em;
  height: 100%;

  /* This CSS fixes a bug when caret is pushed onto the
  new line by editor's placeholder */
  .public-DraftEditorPlaceholder-root {
    color: gray;
    position: absolute;
    z-index: 0;
  }

  .public-DraftEditorPlaceholder-hasFocus {
    color: var(fbui-desktop-text-placeholder-focused);
  }

  .DraftEditorPlaceholder-hidden {
    display: none;
  }
`;

class BookEditor extends React.Component {
  constructor(props) {
    super(props);

    this.decorators = new CompositeDecorator([{
      strategy: BookMarkStrategy.strategy,
      component: BookMarkDecorator,
    }]);

    this.editorRef = React.createRef();

    this.bookMarkChangeHandler = new BookMarkChangeHandler();

    let savedContent;
    if (window.savedEditorContent) {
      savedContent = convertFromRaw(window.savedEditorContent);
    }

    this.state = {
      isLoading: true,
      editorState: savedContent ? EditorState.createWithContent(savedContent, this.decorators) : EditorState.createEmpty(this.decorators),
      isBookEditorSelectionEmpty: true,
      isCommandInputModalHidden: true,
    };
  }

  componentDidMount() {
    window.addEventListener('message', this.handleExtensionMessageEvent);
  }

  componentDidUpdate(prevProps) {
    const { activeCodeBook } = this.props;
    const { activeCodeBook: prevActiveCodeBook } = prevProps;

    if (!activeCodeBook) {
      return;
    }

    if (!prevActiveCodeBook || prevActiveCodeBook.id !== activeCodeBook.id) {
      // new code book
      const { lastOpenedDocument } = activeCodeBook;
      const docName = lastOpenedDocument || Object.keys(activeCodeBook.documents)[0];
      this.props.setActiveCodeBookDocument(docName);
      this.sendLoadCodeBookDocumentContent(activeCodeBook.id, docName);
    }
  }

  componentWillUnmount() {
    this.props.setActiveCodeBook(undefined);
    this.props.setActiveCodeBookDocument('');
    window.removeEventListener('message', this.handleExtensionMessageEvent);
  }

  setNewEditorStateAndSave(editorState) {
    this.setState({
      editorState,
    });
    // Remove book mark entities from the saved content because
    // the source of the truth is what was saved by the marker
    const strippedState = removeBookMarkEntitiesFromState(editorState);
    const currentContent = strippedState.getCurrentContent();

    const rawContent = convertToRaw(currentContent);
    const codeBookId = this.props.activeCodeBook.id;
    const documentName = this.props.activeDocument;
    this.props.saveEditorContent(rawContent, codeBookId, documentName);
  }

  focusEditor = () => {
    this.editorRef.focus();
  }

  handleEditorChange = (newEditorState) => {
    const bookMarks = findBookMarksInContent(newEditorState);

    const codeBookId = this.props.activeCodeBook.id;
    const documentName = this.props.activeDocument;
    this.props.reportBookMarksInEditor(bookMarks, codeBookId, documentName);

    const previousEditorState = this.state.editorState;
    this.setNewEditorStateAndSave(newEditorState);

    if (!this.state.isLoading) {
      this.calculateStateChangesAndNotifyExtension(previousEditorState, newEditorState);
    }
  }

  handleExtensionMessageEvent = (event) => {
    const message = event.data;
    const {
      type,
    } = message;

    switch (type) {
      case messageType.CODE_BOOK.LOADED_CODE_BOOK_DOCUMENT:
        this.handleLoadedCodeBookDocument(message);
        break;

      case messageType.CODE_BOOK.BOOK_MARKS_DELETED_WITH_CODE_MARKS:
        this.handleBookMarksDeletedWithCodeMarks(message);
        break;

      case messageType.BOOK_EDITOR.BOOK_MARKS_DID_CHANGE:
        this.handleBookMarksDidChange(message);
        break;

      default:
        break;
    }
  }

  handleBookMarksDeletedWithCodeMarks(message) {
    const {
      changes,
    } = message.data;
    if (changes && changes.length && changes.length > 0) {
      const rawEditorContent = convertToRaw(this.state.editorState.getCurrentContent());
      const newEditorState = this.bookMarkChangeHandler.recalculateBookMarks({
        changes,
        rawEditorContent,
      }, this.state.editorState);
      this.handleEditorChange(newEditorState);
    }
  }

  handleBookMarksDidChange(message) {
    const {
      changes,
      editorContent: rawEditorContent,
    } = message.data;

    if (changes && changes.length && changes.length > 0) {
      const newEditorState = this.bookMarkChangeHandler.recalculateBookMarks({
        changes,
        rawEditorContent,
      }, this.state.editorState);
      this.handleEditorChange(newEditorState);
    }
  }

  handleLoadedCodeBookDocument(message) {
    const { editorContent, bookMarks } = message.data;
    const docEditorContent = convertFromRaw(editorContent);
    const savedEditorState = EditorState.createWithContent(docEditorContent, this.decorators);

    // For now convert bookMarks to bookMarkChanges and call recalculateBookMarks
    const changes = bookMarks.map(bm => ({
      changeType: 'CREATE',
      mark: bm,
    }));

    const rawEditorContent = convertToRaw(savedEditorState.getCurrentContent());
    const newEditorState = this.bookMarkChangeHandler.recalculateBookMarks({
      changes,
      rawEditorContent,
    }, savedEditorState);
    this.handleEditorChange(newEditorState);

    this.setState({ isLoading: false }, () => {
      this.focusEditor();
    });
  }

  sendLoadCodeBookDocumentContent(codeBookId, documentName) {
    const t = messageType.CODE_BOOK.LOAD_CODE_BOOK_DOCUMENT;
    const m = new Message(t, {
      codeBookId,
      documentName,
    });
    window.vscode.postMessage(m);
  }

  calculateStateChangesAndNotifyExtension(previousEditorState, newEditorState) {
    // const previousContentText = extractTextFromContentState(previousEditorState.getCurrentContent());
    // const previousLength = previousContentText.length;

    // const newContentText = extractTextFromContentState(newEditorState.getCurrentContent());
    // const newLength = newContentText.length;

    // const previousSelectionState = previousEditorState.getSelection();
    // const previousSelectedText = getSelectedTextForSelectionState(previousEditorState);
    // const previousSelectionIsEmpty = previousSelectionState.isCollapsed();

    const newSelectionState = newEditorState.getSelection();
    // const newSelectedText = getSelectedTextForSelectionState(newEditorState);
    const newSelectionIsEmpty = newSelectionState.isCollapsed();
    this.setState({
      isBookEditorSelectionEmpty: newSelectionIsEmpty,
    });
  }

  handleCreateMarkButtonClick(e) {
    // We don't want to propagate the event down to the editor
    // because editor will think that something changed and will
    // trigger onChange event.
    // This will trigger recalculation of existing book marks in
    // the editor before new book mark is created.
    e.stopPropagation();

    const {
      editorState,
      isBookEditorSelectionEmpty,
    } = this.state;

    // Convert current user's selection to book mark blocks
    // User can only select a continuous block of text so
    // there will be only single book mark block for the
    // current selection.
    // Therefore we just need to convert current seleciton
    // to the global offset and selected textlength

    if (!isBookEditorSelectionEmpty) {
      const offset = getStartingGlobalOffsetForSelection(editorState);
      const { length } = getSelectedTextForSelectionState(editorState);
      const { length: bookEditorTextLength } = extractTextFromContentState(editorState.getCurrentContent());

      const editorContent = convertToRaw(editorState.getCurrentContent());
      this.props.createBookMark(
        editorContent,
        [{
          offset,
          length,
        }],
        bookEditorTextLength,
        this.props.activeCodeBook.id,
        this.props.activeDocument,
      );
    } else {
      console.warn('Cannot create book mark from empty selection');
    }
  }

  handleShowCodeButtonClick() {
    const { focusedBookMarkId } = this.props;
    this.props.showCodeMark(focusedBookMarkId);
  }

  handleDeleteMarkButtonClick() {
    const { editorState } = this.state;
    const { focusedBookMarkId } = this.props;
    const editorContent = convertToRaw(editorState.getCurrentContent());
    this.props.deleteBookMark(editorContent, focusedBookMarkId);
    this.props.setFocusedBookMarkId('');
  }

  blockRendererFn(contentBlock) {
    const type = contentBlock.getType();
    if (type === 'atomic') {
      return {
        component: Code,
        editable: false,
      };
    }
    return null;
  }

  addCommand(commandString) {
    const { editorState } = this.state;

    let contentState = editorState.getCurrentContent();
    contentState = contentState.createEntity('CODE_COMMAND', 'IMMUTABLE', { commandString });
    const entityKey = contentState.getLastCreatedEntityKey();
    let newEditorState = EditorState.set(editorState, {
      currentContent: contentState,
    }, 'create-entity');
    newEditorState = AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ');

    this.handleEditorChange(newEditorState);
  }

  keyBindingFn(event) {
    const { editorState } = this.state;
    const selection = editorState.getSelection();

    if (
      !selection.isCollapsed()
      || selection.getAnchorOffset()
      || selection.getFocusOffset()
    ) {
      return getDefaultKeyBinding(event);
    }

    const content = editorState.getCurrentContent();
    const startKey = selection.getStartKey();

    if (event.keyCode === 8) {
      // Backspace
      const blockBefore = content.getBlockBefore(startKey);
      if (!blockBefore) {
        return getDefaultKeyBinding(event);
      }
      if (blockBefore.getType() !== 'atomic') {
        return getDefaultKeyBinding(event);
      }

      return 'delete-block-before';
    }
    // } if (event.keyCode === 46) {
    //   // Delete
    //   const blockAfter = content.getBlockAfter(startKey);
    //   if (!blockAfter) {
    //     return getDefaultKeyBinding(event);
    //   }
    //   if (blockAfter.getType() !== 'atomic') {
    //     return getDefaultKeyBinding(event);
    //   }

    //   return 'delete-block-after';
    // }
    return getDefaultKeyBinding(event);
  }

  handleKeyCommand(command) {
    const { editorState } = this.state;
    const selection = editorState.getSelection();
    const content = editorState.getCurrentContent();

    const startKey = selection.getStartKey();

    if (command === 'delete-block-before') {
      const blockBefore = content.getBlockBefore(startKey);
      if (blockBefore) {
        const blockMap = content.getBlockMap().delete(blockBefore.getKey());
        const withoutAtomicBlock = content.merge({
          blockMap,
          selectionAfter: selection,
        });
        if (withoutAtomicBlock !== content) {
          const newEditorState = EditorState.push(
            editorState,
            withoutAtomicBlock,
            'remove-range',
          );
          this.handleEditorChange(newEditorState);
          return 'handled';
        }
        return 'not-handled';
      }

      return 'not-handled';
    }
    // } if (command === 'delete-block-after') {
    //   const blockAfter
    //   return 'not-handled';
    // }
    return 'no-handled';
  }

  render() {
    return (
      <WrapperDiv>
        {this.state.isLoading && (
          <p>loading...</p>
        )}

        {!this.state.isLoading && (
          <React.Fragment>
            <EditorWrapper
              onClick={() => {
                this.focusEditor();
              }}
            >
              <Editor
                ref={
                  (el) => {
                    this.editorRef = el;
                    this.props.setEditorRef(this.editorRef);
                  }
                }
                keyBindingFn={e => this.keyBindingFn(e)}
                handleKeyCommand={c => this.handleKeyCommand(c)}
                placeholder="Let's start this awesome tutorial..."
                stripPastedStyles="true"
                blockRendererFn={contentBlock => this.blockRendererFn(contentBlock)}
                editorState={this.state.editorState}
                onChange={this.handleEditorChange}
              />
            </EditorWrapper>
            <StyledToolbar
              canCreateMark={!this.state.isBookEditorSelectionEmpty}
              isMarkInteractionEnabled={!!this.props.focusedBookMarkId}
              onCreateMarkClick={e => this.handleCreateMarkButtonClick(e)}
              onShowCodeClick={() => this.handleShowCodeButtonClick()}
              onDeleteMarkClick={() => this.handleDeleteMarkButtonClick()}
              onAddCommandClick={() => this.setState({ isCommandInputModalHidden: false })}
            />
            {!this.state.isCommandInputModalHidden && (
              <CommnadInputModal
                onCloseButtonClick={() => this.setState({ isCommandInputModalHidden: true })}
                onSetButtonClick={(commandString) => {
                  // Mixpanel track create command
                  const { editorState } = this.state;
                  const { length: bookEditorTextLength } = extractTextFromContentState(editorState.getCurrentContent());
                  this.props.trackCreateCommand(commandString, bookEditorTextLength);

                  this.setState({ isCommandInputModalHidden: true });
                  this.addCommand(commandString);
                }}
              />
            )}
          </React.Fragment>
        )}
      </WrapperDiv>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeCodeBook: state.codeBook.activeCodeBook,

    focusedBookMarkId: state.bookMarks.focusedBookMarkId,
    isMarkerReady: state.marker.isMarkerReady,
    activeDocument: state.codeBook.activeDocument,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setActiveCodeBook: codeBook => dispatch(actions.setActiveCodeBook(codeBook)),
    setActiveCodeBookDocument: documentName => dispatch(actions.setActiveCodeBookDocument(documentName)),

    setFocusedBookMarkId: id => dispatch(actions.setFocusedBookMarkId(id)),
    setEditorRef: ref => dispatch(actions.setEditorRef(ref)),
  };
}

export default compose(
  withSaveEditorContent,
  withCreateBookMark,
  withDeleteBookMark,
  withShowCodeMark,
  withReportBookMarksInEditor,
  withTrackCommand,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(BookEditor);
