import React, { useEffect } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import actions from '@/actions';
import compose from '@/utils/compose';

import withWorkspaceName from '@/App/components/withWorkspaceName';
import withWorkspaceCodeBooks from '@/App/components/withWorkspaceCodeBooks';

import CodeBook from './components/CodeBook';

const HomeDiv = styled.div`
  height: 100%;
  width: 100%;
  margin-top: 10px;
  padding: 0 30px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  @media screen and (max-width: 768px) {
    padding: 0 10px;
  }
`;

const Title = styled.h1`
  margin: 5px 0;

  color: #868A95;
  font-family: 'RobotoMono';
  font-weight: 400;
  font-size: 20px;
`;

const WorkspaceCodeBooks = styled.div`
  margin-top: 10px;
  display: flex;
  flex-direction: column;
`;

function HomeView(props) {
  const {
    getWorkspaceName,
    getWorkspaceCodeBooks,
  } = props;
  useEffect(() => {
    getWorkspaceName();
    getWorkspaceCodeBooks();
  }, [getWorkspaceCodeBooks, getWorkspaceName]);

  function handleCodeBookClick(id) {
    props.loadCodeBook(id);
    props.setRoute('editor');
  }

  return (
    <HomeDiv>
      <Title>({props.workspace.name})/</Title>
      <WorkspaceCodeBooks>
        {props.workspace.codeBooks.map(cb => (
          <CodeBook
            key={cb.id}
            id={cb.id}
            name={cb.name}
            description={cb.description}
            onNameButtonClick={() => handleCodeBookClick(cb.id)}
          />
        ))}
      </WorkspaceCodeBooks>
    </HomeDiv>
  );
}

function mapStateToProps(state) {
  return {
    workspace: state.workspace,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoute: route => dispatch(actions.setRoute(route)),
  };
}

export default compose(
  withWorkspaceName,
  withWorkspaceCodeBooks,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(HomeView);
