import React, { useState } from 'react';
import styled from 'styled-components';

import TextButton from '@/App/components/TextButton';

import EditButton from './components/EditButton';
import EditMenu from './components/EditMenu';

const CodeBookDiv = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const StyledEditMenu = styled(EditMenu)`
  width: 350px;
  z-index: 10;
  position: absolute;
  left: 0;
  top: 34px;
`;

const NameDiv = styled.div`
  display: flex;
  align-items: center;
`;

const StyledEditButton = styled(EditButton)`
  margin-right: 8px;
`;

const NameButton = styled(TextButton)`
  color: #1BAD84;
  font-family: 'RobotoMono';
  font-weight: 600;
  font-size: 25px;
`;

const Description = styled.p`
  margin-top: 5px;
  color: var(--text-light);
  font-size: 15px;
`;

export default function CodeBook(props) {
  const [isEditMenuHidden, setIsEditMenuHidden] = useState(true);
  const description = props.description ? props.description : 'No description';

  function handleEditButtonClick() {
    if (isEditMenuHidden) {
      setIsEditMenuHidden(false);
    }
  }

  function handleCloseEditMenu() {
    setIsEditMenuHidden(true);
  }

  return (
    <CodeBookDiv>
      {!isEditMenuHidden && (
        <StyledEditMenu
          codeBookId={props.id}
          codeBookName={props.name}
          codeBookDescription={props.description}
          onClose={handleCloseEditMenu}
        />
      )}
      <NameDiv>
        <StyledEditButton onClick={handleEditButtonClick} />
        <NameButton onClick={props.onNameButtonClick}>{props.name}</NameButton>
      </NameDiv>
      <Description>{description}</Description>
    </CodeBookDiv>
  );
}
