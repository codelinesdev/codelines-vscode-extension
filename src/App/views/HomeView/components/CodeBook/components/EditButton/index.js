import React from 'react';
import styled from 'styled-components';

import Button from '@/App/components/Button';

import dotIcon from '@/images/dot.png';

const StyledButton = styled(Button)`
  padding: 0 2px;
  display: flex;
  flex-direction: column;
`;

const StyledImg = styled.img`
  width: 4px;
  height: 4px;

  &:not(:first-child):not(:last-child) {
    margin: 3px 0;
  }
`;

export default function EditButton(props) {
  return (
    <StyledButton {...props}>
      <StyledImg src={dotIcon} alt="dot" />
      <StyledImg src={dotIcon} alt="dot" />
      <StyledImg src={dotIcon} alt="dot" />
    </StyledButton>
  );
}
