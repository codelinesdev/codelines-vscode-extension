import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import actions from '@/actions';
import compose from '@/utils/compose';
import withChangeCodeBookName from '@/App/components/withChangeCodeBookName';
import withDeleteCodeBook from '@/App/components/withDeleteCodeBook';

import Modal from '@/App/components/Modal';
import Input from '@/App/components/Input';
import TextArea from '@/App/components/TextArea';
import TextButton from '@/App/components/TextButton';
import Button from '@/App/components/Button';
import ErrorMessage from '@/App/components/ErrorMessage';

const StyledModal = styled(Modal)`
  padding: 8px;
  justify-content: space-between;
`;

const SectionDiv = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InputDiv = styled.div`
  width: 100%;
  margin: 0 0 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledInput = styled(Input)`
  width: 100%;
  border-radius: 3px;
`;

const InputInfo = styled.span`
  width: 100%;
  margin: 5px 0 0;
  font-weight: 300;
  font-size: 15px;
  text-align: right;
`;

const StyledTextArea = styled(TextArea)`
  height: 100px;
  width: 100%;
  border-radius: 3px;
  resize: none;
`;

const StyledTextButton = styled(TextButton)`
  width: 100%;
  padding: 8px 0;

  font-weight: 600;
  text-align: left;

  color: ${props => (props.isDeleteButton ? '#ED3702' : 'inherit')};
`;

const SaveButton = styled(Button)`
  margin-top: 10px;
  width: 100px;
`;

function EditMenu(props) {
  const [codeBookName, setCodeBookName] = useState(props.codeBookName);
  const [codeBookDescription, setCodeBookDescription] = useState(props.codeBookDescription);
  const [errorMessage, setErrorMessage] = useState('');

  const maxCodeBookNameLength = 60;
  const maxCodeBookDescriptionLength = 150;

  function handleNameInputChange(event) {
    const { value } = event.target;
    if (value.length <= maxCodeBookNameLength) {
      setCodeBookName(value);
    }
  }

  function handleDescriptionInputChange(event) {
    const { value } = event.target;
    if (value.length <= maxCodeBookDescriptionLength) {
      setCodeBookDescription(value);
    }
  }

  async function handleSaveButtonClick() {
    setErrorMessage('');
    if (codeBookName.length > 0) {
      const response = await props.changeCodeBookName(props.codeBookId, codeBookName, codeBookDescription);
      if (response.result === 'OK') {
        // Workspace code books with the code book for which we have just updated the name
        const { workspaceCodeBooks } = response;
        props.setWorkspaceCodeBooks(workspaceCodeBooks);
        props.onClose();
      } else if (response.result === 'ERROR') {
        console.error('Error while changing code book metadata', response);
        setErrorMessage(response.error.message);
      }
    }
  }

  async function handleDeleteButtonClick() {
    setErrorMessage('');
    const { workspaceCodeBooks } = await props.deleteCodeBook(props.codeBookId);
    props.setWorkspaceCodeBooks(workspaceCodeBooks);
    props.onClose();
  }

  return (
    <StyledModal
      isDark
      className={props.className}
      onOutsideClick={props.onClose}
    >
      <SectionDiv>
        <InputDiv>
          <StyledInput
            value={codeBookName}
            onChange={handleNameInputChange}
            placeholder="name your code book"
          />
          <InputInfo>{codeBookName.length}/{maxCodeBookNameLength} characters</InputInfo>
        </InputDiv>


        <InputDiv>
          <StyledTextArea
            value={codeBookDescription}
            onChange={handleDescriptionInputChange}
            placeholder="add a brief description for this code book"
          />
          <InputInfo>{codeBookDescription.length}/{maxCodeBookDescriptionLength} characters</InputInfo>
        </InputDiv>

        <StyledTextButton
          isWhite
          isDeleteButton
          shouldShowBackgroundOnHover
          onClick={handleDeleteButtonClick}
        >Delete Code Book
        </StyledTextButton>
      </SectionDiv>

      <SectionDiv>
        <SaveButton
          isBlue
          onClick={handleSaveButtonClick}
        >Save
        </SaveButton>
        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
      </SectionDiv>
    </StyledModal>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    setWorkspaceCodeBooks: codeBooks => dispatch(actions.setWorkspaceCodeBooks(codeBooks)),
  };
}

export default compose(
  withChangeCodeBookName,
  withDeleteCodeBook,
  connect(
    null,
    mapDispatchToProps,
  ),
)(EditMenu);
