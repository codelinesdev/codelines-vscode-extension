export async function promisifyEvent(addListener, removeListener, dispatch, predicate) {
  const result = new Promise((resolve) => {
    function listener(e) {
      // using keys to sign messages?
      if (predicate(e)) {
        console.log(`[Extension Message ${e.data.type}]`, e);
        removeListener(listener);
        resolve(e);
      }
    }
    addListener(listener);
  });
  dispatch();
  return result;
}
