import {
  CharacterMetadata,
  EditorState,
  SelectionState,
} from 'draft-js';

export function extractTextFromContentState(contentState) {
  const blockMap = contentState.getBlockMap();
  const [...keys] = blockMap.keys();

  let text = '';
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const block = blockMap.get(key);
    text += block.getText();
    // if (i < keys.length - 1) {
    //   text += '\n';
    // }
  }
  return text;
}

export function getStartingGlobalOffsetForSelection(editorState) {
  // Returns a global offset (number) for the starting position of a selection
  const selectionState = editorState.getSelection();

  const startingBlockKey = selectionState.getStartKey();
  const startOffset = selectionState.getStartOffset();

  const currentContent = editorState.getCurrentContent();
  const blockMap = currentContent.getBlockMap();

  const blocksBeforeStartingBlock = blockMap.takeUntil((block, key) => key === startingBlockKey);

  return blocksBeforeStartingBlock.reduce((reducer, block) => {
    // eslint-disable-next-line no-param-reassign
    reducer += block.getLength();
    return reducer;
  }, startOffset);
}

export function getSelectedTextForSelectionState(editorState) {
  const selectionState = editorState.getSelection();
  const currentContent = editorState.getCurrentContent();

  const startOffset = selectionState.getStartOffset();
  const endOffset = selectionState.getEndOffset();

  // Selection might start and end in different blocks
  const startingBlockKey = selectionState.getStartKey();
  const endingBlockKey = selectionState.getEndKey();

  if (startingBlockKey !== endingBlockKey) {
    const blockMap = currentContent.getBlockMap();
    const blocksInSelection = blockMap
      .skipWhile((block, key) => key !== startingBlockKey)
      .reverse()
      .skipUntil((block, key) => key === endingBlockKey)
      .reverse();

    const [...keys] = blocksInSelection.keys();
    // console.log('keys', keys);

    let selectedText = '';
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const block = currentContent.getBlockForKey(key);

      if (i === 0) {
        // First block
        selectedText += block.getText().slice(startOffset);
      } else if (i === keys.length - 1) {
        // Last block
        selectedText += block.getText().slice(0, endOffset);
      } else {
        selectedText += block.getText();
      }
    }
    return selectedText;
  }
  const anchorKey = selectionState.getAnchorKey();
  const currentContentBlock = currentContent.getBlockForKey(anchorKey);
  if (currentContentBlock) {
    return currentContentBlock.getText().slice(startOffset, endOffset);
  }
  return '';
}

export function converMarkBlockToSelectionState(currentContent, markBlock) {
  const {
    offset: markBlockStartOffset,
    length,
  } = markBlock;
  const markBlockEndOffset = markBlockStartOffset + length;

  let lengthSoFar = 0;

  const blockMap = currentContent.getBlockMap();
  const [...keys] = blockMap.keys();

  // We need anchor key, anchor offset, focus key, and focus offset
  let anchorKey = keys[0];
  let anchorOffset = -1;
  let focusKey = anchorKey;
  let focusOffset = anchorOffset;

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const block = blockMap.get(key);

    const blockLength = block.getLength();
    // if (i < keys.length - 1) {
    // +1 for a new line character
    // blockLength += 1;
    // }

    // const newLineAdd = i > 0 ? 1 : 0


    if (anchorOffset === -1) {
      if ((lengthSoFar + blockLength) >= markBlockStartOffset) {
        // Offset starts in this block
        anchorKey = key;
        anchorOffset = markBlockStartOffset - lengthSoFar;
      }
    }

    if (focusOffset === -1) {
      if ((lengthSoFar + blockLength) >= markBlockEndOffset) {
        focusKey = key;
        focusOffset = markBlockEndOffset - lengthSoFar;
      }
    }

    lengthSoFar += blockLength;
  }

  console.log('converMarkBlockToSelectionState', {
    focusKey,
    focusOffset,
    anchorKey,
    anchorOffset,
    hasFocus: false,
    isBackward: false,
  });

  const selection = SelectionState.createEmpty(anchorKey);
  return selection.merge({
    focusKey,
    focusOffset,
    anchorKey,
    anchorOffset,
    hasFocus: false,
    isBackward: false,
    // startKey: anchorKey,
    // startOffset: anchorOffset,
    // endKey: focusKey,
    // endOffset: focusOffset,
  });
}

// export function removeEntitiesFromState(entityKeys, editorState) {
//   if (entityKeys.length === 0) { return editorState; }

//   const editorContent = editorState.getCurrentContent();
//   const blockMap = editorContent.getBlockMap();

//   const blocks = blockMap.map((block) => {
//     let didDeleteEntity = false;

//     const chars = block.getCharacterList().map((char) => {
//       const charEntityKey = char.getEntity();
//       if (entityKeys.includes(charEntityKey)) {
//         didDeleteEntity = true;
//         return CharacterMetadata.applyEntity(char, null);
//       }
//       return char;
//     });

//     return didDeleteEntity ? block.set('characterList', chars) : block;
//   });

//   const newEditorContent = editorContent.merge({
//     blockMap: blockMap.merge(blocks),
//   });

//   const newEditorState = EditorState.push(
//     editorState,
//     newEditorContent,
//     'apply-entity',
//   );

//   return newEditorState;
// }

export function removeBookMarkEntitiesFromState(editorState) {
  const editorContent = editorState.getCurrentContent();
  const blockMap = editorContent.getBlockMap();

  const blocks = blockMap.map((block) => {
    let didDeleteEntity = false;

    const chars = block.getCharacterList().map((char) => {
      const charEntityKey = char.getEntity();
      if (charEntityKey) {
        const entity = editorContent.getEntity(charEntityKey);
        const entityType = entity.getType();
        if (entityType === 'BOOK_MARK') {
          didDeleteEntity = true;
          return CharacterMetadata.applyEntity(char, null);
        }
      }
      return char;
    });

    return didDeleteEntity ? block.set('characterList', chars) : block;
  });

  const newEditorContent = editorContent.merge({
    blockMap: blockMap.merge(blocks),
  });

  const newEditorState = EditorState.push(
    editorState,
    newEditorContent,
    'apply-entity',
  );

  return newEditorState;
}

export function findBookMarksInContent(editorState) {
  const editorContent = editorState.getCurrentContent();
  const blockMap = editorContent.getBlockMap();

  const bookMarks = {};
  let globalOffset = -1;
  let lastBookMarkId = '';
  blockMap.forEach((block) => {
    block.getCharacterList().forEach((char) => {
      globalOffset += 1;
      const charEntityKey = char.getEntity();
      if (charEntityKey) {
        const entity = editorContent.getEntity(charEntityKey);
        const type = entity.getType();

        if (type === 'BOOK_MARK') {
          const { bookMarkId } = entity.getData();

          const bm = bookMarks[bookMarkId];
          if (bm) {
            // Already encountered this book mark entity
            if (lastBookMarkId === bookMarkId) {
              // Still in a single block
              let lastBlock = bm.blocks.pop();
              lastBlock = {
                offset: lastBlock.offset,
                length: lastBlock.length + 1,
              };
              bm.blocks.push(lastBlock);
              bookMarks[bookMarkId] = bm;
            } else {
              // Create new block for this book mark
              const newBlock = {
                offset: globalOffset,
                length: 1,
              };
              bookMarks[bookMarkId].blocks.push(newBlock);
            }
          } else {
            // Starting new book mark
            bookMarks[bookMarkId] = {
              blocks: [{
                offset: globalOffset,
                length: 1,
              }],
            };
          }

          lastBookMarkId = bookMarkId;
        }
      } else {
        lastBookMarkId = '';
      }
    });
  });
  return bookMarks;
}
