const bookMarkEntity = {
  type: 'BOOK_MARK',
  mutability: 'MUTABLE',
};

function strategy(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges((character) => {
    const entityKey = character.getEntity();
    if (entityKey === null) return false;
    return contentState.getEntity(entityKey).getType() === bookMarkEntity.type;
  }, callback);
}

export default {
  bookMarkEntity,
  strategy,
};
