import {
  convertToRaw,
  CharacterMetadata,
  EditorState,
  Modifier,
  SelectionState,
} from 'draft-js';
import equal from 'fast-deep-equal';
import BookMarkStrategy from '@/utils/editor/strategies/BookMark';
import BookMarkChangeCollector from './BookMarkChangeCollector';

export default class BookMarkChangeHandler {
  constructor() {
    this.collector = new BookMarkChangeCollector();
    this.editorState = undefined;

    this.entityKeysForBookMarks = {}; // { '<bookMarkId>': ['<entityKey>'] }

    this.bookMarkEntity = BookMarkStrategy.bookMarkEntity;
  }

  recalculateBookMarks({ changes: bmChanges, rawEditorContent }, currentEditorState) {
    // `rawEditorContent` is a raw content to which the changes were applied

    this.editorState = currentEditorState;

    const currentRawContent = convertToRaw(currentEditorState.getCurrentContent());
    if (!equal(currentRawContent, rawEditorContent)) {
      // console.log('CONTENTS NOT EQUAL <START>');
      // console.log('currentRawContent', currentRawContent);
      // console.log('rawEditorContent', rawEditorContent);


      // User managed to change the content before we got response from the marker
      // Because a state for which the book mark changes were computer isn't same
      // as the current state, we can't apply changes.
      // Wait for next message from the marker to re-render book marks.

      // Even though the marker has a previous state from the last N steps
      // it'll eventually catch up to the current state.
      // Therefore, we must save the changes we received now (for the wrong state).
      // Once we receive changes for the correct state, we'll render all the changes
      // that were saved from before.

      // console.log('Pushing non-synced book mark changes to the collector', bmChanges);
      this.collector.pushChanges(bmChanges);
      // console.log('CONTENTS NOT EQUAL <END>');
    } else {
      // console.log('CONTENTS ARE EQUAL');
    }

    let totalChanges = bmChanges;
    if (!this.collector.isEmpty) {
      // Even though the editor's contents are now matching
      // the current `bmChanges` may affected previusly unrendered changes
      this.collector.pushChanges(bmChanges);
      const previousChanges = this.collector.getChanges();
      totalChanges = previousChanges.concat(bmChanges);
    }

    // const previousChanges = this.collector.getChanges();
    // if (previousChanges.length > 0) {
    //   console.log('FIRST RENDERING PREVIOUSLY UNRENDERED BOOK MARK CHANGES', previousChanges);
    //   // TODO
    //   // newEditorState = this.applyBookMarkChanges(previousChanges, currentEditorState);

    //   // We have to render previous changes first
    //   totalChanges = previousChanges.concat(bmChanges);
    // }

    // TODO
    // newEditorState = this.applyBookMarkChanges(changes, currentEditorState);

    this.applyBookMarkChanges(totalChanges);
    return this.editorState;
  }

  applyBookMarkChanges(changes) {
    changes.forEach((change) => {
      const {
        changeType,
        mark: bookMark,
      } = change;

      switch (changeType) {
        case 'CREATE':
          console.log('Will apply book mark change \'CREATE\'...');
          this.createBookMark(bookMark);
          break;
        case 'CHANGE':
          // Disabled for now
          // this._changeBookMark(bookMark);
          break;
        case 'DELETE':
          console.log('Will apply book mark change \'DELETE\'...');
          this.deleteBookMark(bookMark);
          break;
        default:
          console.error('Unknown book mark change type', changeType);
      }
    });
  }

  // _changeBookMark(bookMark) {
  // console.log('Changing book mark (delete + create)', bookMark);

  // TODO: Remove entity key for book mark from `_registerEntityKeyForBookMark`

  // Save the current caret position
  // const selectionBefore = this.editorState.getSelection();

  // this._deleteBookMark(bookMark);
  // this._createBookMark(bookMark);

  // Apply the saved caret position
  // this.editorState = EditorState.forceSelection(this.editorState, selectionBefore);
  // this.editorState = EditorState.acceptSelection(this.editorState, selectionBefore);
  // }

  deleteBookMark(bookMark) {
    console.log('Deleting book mark', bookMark);

    const entityKeysForBookMark = this.entityKeysForBookMarks[bookMark.id];
    if (entityKeysForBookMark && entityKeysForBookMark.length && entityKeysForBookMark.length > 0) {
      this.removeEntitiesFromState(entityKeysForBookMark);
      delete this.entityKeysForBookMarks[bookMark.id];
    }
  }

  createBookMark(bookMark) {
    console.log('Creating new book mark', bookMark);
    const {
      blocks: bmBlocks,
      references: cmReferences,
    } = bookMark;

    const entityData = {
      bookMarkId: bookMark.id,
      bindedCodeMarkIds: cmReferences.map(r => r.id),
    };

    const editorContent = this.editorState.getCurrentContent();
    bmBlocks.forEach((bmBlock) => {
      let newContent = editorContent.createEntity(this.bookMarkEntity.type, this.bookMarkEntity.mutability, entityData);
      const entityKey = newContent.getLastCreatedEntityKey();
      this.registerEntityKeyForBookMark(bookMark.id, entityKey);

      const bmBlockSelectionState = this.convertBlockToSelectionState(bmBlock);

      newContent = Modifier.applyEntity(
        newContent,
        bmBlockSelectionState,
        entityKey,
      );

      this.editorState = EditorState.push(
        this.editorState,
        newContent,
        'apply-entity',
      );
    });
  }

  registerEntityKeyForBookMark(bookMarkId, entityKey) {
    const keys = this.entityKeysForBookMarks[bookMarkId];
    if (keys && keys.length && keys.length > 0) {
      this.entityKeysForBookMarks[bookMarkId] = [...keys, entityKey];
    }
    this.entityKeysForBookMarks[bookMarkId] = [entityKey];
  }

  convertBlockToSelectionState(bookMarkBlock) {
    const {
      offset: bmBlockStartOffset,
      length,
    } = bookMarkBlock;
    const bmBlockEndOffset = bmBlockStartOffset + length;

    const editorContent = this.editorState.getCurrentContent();

    let lengthSoFar = 0;

    let anchorKey = '';
    let anchorOffset = -1;
    let focusKey = '';
    let focusOffset = -1;

    const blockMap = editorContent.getBlockMap();
    blockMap.forEach((block, key) => {
      const blockLength = block.getLength();

      if (anchorOffset === -1) {
        if ((lengthSoFar + blockLength) >= bmBlockStartOffset) {
          // Offset starts in this block
          anchorKey = key;
          anchorOffset = bmBlockStartOffset - lengthSoFar;
        }
      }

      if (focusOffset === -1) {
        if ((lengthSoFar + blockLength) >= bmBlockEndOffset) {
          focusKey = key;
          focusOffset = bmBlockEndOffset - lengthSoFar;
        }
      }
      lengthSoFar += blockLength;
    });

    // console.log('converMarkBlockToSelectionState', {
    //   focusKey,
    //   focusOffset,
    //   anchorKey,
    //   anchorOffset,
    //   hasFocus: false,
    //   isBackward: false,
    // });

    // return SelectionState.createEmpty(anchorKey);

    return SelectionState.createEmpty(anchorKey).merge({
      focusKey,
      focusOffset,
      anchorKey,
      anchorOffset,
      hasFocus: false,
      isBackward: false,
      // startKey: anchorKey,
      // startOffset: anchorOffset,
      // endKey: focusKey,
      // endOffset: focusOffset,
    });
  }

  removeEntitiesFromState(entityKeys) {
    if (entityKeys.length === 0) { return; }

    const editorContent = this.editorState.getCurrentContent();
    const blockMap = editorContent.getBlockMap();

    const blocks = blockMap.map((block) => {
      let didDeleteEntity = false;

      const chars = block.getCharacterList().map((char) => {
        const charEntityKey = char.getEntity();
        if (entityKeys.includes(charEntityKey)) {
          didDeleteEntity = true;
          return CharacterMetadata.applyEntity(char, null);
        }
        return char;
      });

      return didDeleteEntity ? block.set('characterList', chars) : block;
    });

    const newEditorContent = editorContent.merge({
      blockMap: blockMap.merge(blocks),
    });

    this.editorState = EditorState.push(
      this.editorState,
      newEditorContent,
      'apply-entity',
    );
  }
}
