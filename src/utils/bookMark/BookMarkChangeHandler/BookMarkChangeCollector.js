export default class BookMarkChangeCollector {
  constructor() {
    this.isEmpty = true;
    this.batch = {};
    this.changeType = {
      delete: 'DELETE',
      create: 'CREATE',
      change: 'CHANGE',
    };
  }

  pushChanges(changes) {
    changes.forEach(change => this.pushChange(change));
  }

  pushChange(change) {
    this.isEmpty = false;
    const current = this.batch[change.mark.id];
    if (!current && change) {
      this.batch[change.mark.id] = change;
    }
    if (current && change) {
      const adjustedChange = this.adjustChange(current, change);
      if (adjustedChange) {
        this.batch[change.mark.id] = adjustedChange;
      } else {
        delete this.batch[change.mark.id];
      }
    }
  }

  // filter invalid change sequences
  // SEQUENCES MUST BE IN CORRECT ORDER AND SEQUENCES MUST BE COMPLETE
  // eslint-disable-next-line
  adjustChange(current, change) {
    switch (current.changeType) {
      case this.changeType.delete:
        if (change.changeType === this.changeType.create) {
          return change;
        }
        return undefined;
      case this.changeType.change:
        if (change.changeType === this.changeType.create) {
          return undefined;
        }
        return change;
      case this.changeType.create:
        if (change.changeType === this.changeType.delete) {
          return undefined;
        } if (change.changeType === this.changeType.change) {
          // maybe create new instance?
          // eslint-disable-next-line
          change.changeType = this.changeType.create;
        }
        return change;
      default:
        break;
    }
  }

  getChanges() {
    const popBatch = this.batch;
    this.batch = {};
    this.isEmpty = true;
    return Object.values(popBatch);
  }
}
